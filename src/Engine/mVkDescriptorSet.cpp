/**
 * @file Engine/mVkDescriptorSet.cpp
 * @author Tomas Polasek
 * @brief Abstraction for Vulkan descriptor set.
 */

#include "Engine/mVkDescriptorSet.h"

namespace mVkH
{
    void HVkDescriptorSet::addBinding(uint32_t binding, VkDescriptorType type,
                                      uint32_t count, VkShaderStageFlags stages,
                                      VkSampler const *immutableSamplers)
    {
        VkDescriptorSetLayoutBinding lb{};
        lb.binding = binding;
        lb.descriptorType = type;
        lb.descriptorCount = count;
        lb.stageFlags = stages;
        lb.pImmutableSamplers = immutableSamplers;
        mBindings.push_back(lb);
    }

    void HVkDescriptorSet::createSetLayout(VkDevice device)
    {
        // Compile one set of bindings into a descriptor set.
        mDevice = device;

        VkDescriptorSetLayoutCreateInfo layoutCI{};
        layoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutCI.bindingCount = static_cast<uint32_t>(mBindings.size());
        layoutCI.pBindings = mBindings.data();

        mLayouts.push_back(VK_NULL_HANDLE);
        if (vkCreateDescriptorSetLayout(mDevice, &layoutCI, nullptr, &mLayouts.back()) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create descriptor set layout!");
        }
    }

    void HVkDescriptorSet::createDescriptorSet(VkDescriptorPool pool)
    {
        ASSERT_FATAL(mDevice != VK_NULL_HANDLE);

        VkDescriptorSetAllocateInfo descriptorAI{};
        descriptorAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        descriptorAI.descriptorPool = pool;
        descriptorAI.descriptorSetCount = static_cast<uint32_t>(mLayouts.size());
        descriptorAI.pSetLayouts = mLayouts.data();

        if (vkAllocateDescriptorSets(mDevice, &descriptorAI, &mDescriptorSet) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate descriptor set!");
        }
    }

    void HVkDescriptorSet::writeBufferInfo(VkBuffer buffer, VkDeviceSize offset,
                                           VkDeviceSize size,
                                           uint32_t binding, VkDescriptorType type)
    {
        VkDescriptorBufferInfo bi{};
        bi.buffer = buffer;
        bi.offset = offset;
        bi.range = size;
        VkWriteDescriptorSet wds{};
        wds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        wds.dstSet = mDescriptorSet;
        wds.dstBinding = binding;
        wds.dstArrayElement = 0;
        wds.descriptorCount = 1;
        wds.descriptorType = type;
        wds.pImageInfo = nullptr;
        wds.pBufferInfo = &bi;
        wds.pTexelBufferView = nullptr;

        auto writers = {
                wds
        };

        vkUpdateDescriptorSets(mDevice, writers.size(), writers.begin(), 0, nullptr);
    }

    void HVkDescriptorSet::writeSamplerInfo(VkSampler sampler, VkImageView view,
                                            VkImageLayout layout,
                                            uint32_t binding, VkDescriptorType type)
    {
        VkDescriptorImageInfo ii{};
        ii.sampler = sampler;
        ii.imageView = view;
        ii.imageLayout = layout;
        VkWriteDescriptorSet wds{};
        wds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        wds.dstSet = mDescriptorSet;
        wds.dstBinding = binding;
        wds.dstArrayElement = 0;
        wds.descriptorCount = 1;
        wds.descriptorType = type;
        wds.pImageInfo = &ii;
        wds.pBufferInfo = nullptr;
        wds.pTexelBufferView = nullptr;

        auto writers = {
                wds
        };

        vkUpdateDescriptorSets(mDevice, writers.size(), writers.begin(), 0, nullptr);
    }

    void HVkDescriptorSet::destroy()
    {
        for (auto &layout : mLayouts)
        {
            vkDestroyDescriptorSetLayout(mDevice, layout, nullptr);
        }
        mLayouts.clear();
        /**
         * No need to free descriptor set (for now), since it will be deleted
         * with the descriptor pool.
         */
    }

    HVkDescriptorSet::~HVkDescriptorSet()
    {
        destroy();
    }

    HVkDescriptorPool::HVkDescriptorPool()
    {
        std::memset(mRequirements, 0u, sizeof(mRequirements));
    }

    void HVkDescriptorPool::requestSize(VkDescriptorType type, uint32_t count)
    {
        mRequirements[type] += count;
    }

    void HVkDescriptorPool::create(VkDevice device, uint32_t maxSets)
    {
        mDevice = device;

        std::vector<VkDescriptorPoolSize> sizes{};
        for (uint32_t iii = 0; iii < VK_DESCRIPTOR_TYPE_RANGE_SIZE; ++iii)
        { // Find all non-zero requirements.
            if (mRequirements[iii] > 0)
            {
                sizes.emplace_back(VkDescriptorPoolSize{ static_cast<VkDescriptorType>(iii),
                                                         mRequirements[iii] });
            }
        }

        VkDescriptorPoolCreateInfo poolCI{};
        poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        // Allow creating and freeing descriptor sets.
        poolCI.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        poolCI.maxSets = maxSets;
        poolCI.poolSizeCount = static_cast<uint32_t>(sizes.size());
        poolCI.pPoolSizes = sizes.data();

        if (vkCreateDescriptorPool(mDevice, &poolCI, nullptr, &mPool) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create descriptor pool!");
        }
    }

    void HVkDescriptorPool::destroy()
    {
        if (mPool != VK_NULL_HANDLE)
        {
            vkDestroyDescriptorPool(mDevice, mPool, nullptr);
        }

        mPool = VK_NULL_HANDLE;
    }

    HVkDescriptorPool::~HVkDescriptorPool()
    {
        destroy();
    }
}

