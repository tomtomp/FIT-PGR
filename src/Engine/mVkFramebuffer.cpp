/**
 * @file Engine/mVkFramebuffer.cpp
 * @author Tomas Polasek
 * @brief Abstraction for Vulkan framebuffers.
 */

#include "Engine/mVkFramebuffer.h"

namespace mVkH
{
    void HVkFramebuffer::prepare(VkDevice device)
    {
        destroy();
        mDevice = device;
    }

    void HVkFramebuffer::addFramebuffer(VkRenderPass renderPass, VkExtent2D swapChainExtent,
                                        std::initializer_list<VkImageView> attachments)
    {
        VkFramebufferCreateInfo fbCI{};
        fbCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        fbCI.renderPass = renderPass;
        fbCI.attachmentCount = static_cast<uint32_t>(attachments.size());
        fbCI.pAttachments = attachments.begin();
        fbCI.width = swapChainExtent.width;
        fbCI.height = swapChainExtent.height;
        fbCI.layers = 1;

        mFramebuffers.push_back(VK_NULL_HANDLE);
        if (vkCreateFramebuffer(mDevice, &fbCI, nullptr, &mFramebuffers.back()) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create framebuffer");
        }
    }

    HVkFramebuffer::~HVkFramebuffer()
    {
        destroy();
    }

    void HVkFramebuffer::destroy()
    {
        if (mDevice != VK_NULL_HANDLE)
        {
            for (auto &fb : mFramebuffers)
            {
                vkDestroyFramebuffer(mDevice, fb, nullptr);
            }
        }
        mFramebuffers.clear();
    }
}
