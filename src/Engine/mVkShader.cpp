/**
 * @file Engine/mVkShader.cpp
 * @author Tomas Polasek
 * @brief Vulkan shader abstraction.
 */

#include "Engine/mVkShader.h"

namespace mVkH
{
    HVkShader::HVkShader(const std::string basePath, VkShaderStageFlagBits shaderType,
                         VkDevice device) :
        mDevice{device}, mShaderType{shaderType}
    {
        std::string filePath{basePath + mVkU::shaderExtension(shaderType) + SHADER_SPIRV_EXT};
        mShaderModule = createShaderModule(device, Util::loadFileB(filePath));
    }

    HVkShader::~HVkShader()
    {
        if (mShaderModule != VK_NULL_HANDLE)
        {
            vkDestroyShaderModule(mDevice, mShaderModule, nullptr);
        }
    }

    VkPipelineShaderStageCreateInfo HVkShader::pipelineShaderStageCreateInfo() const
    {
        VkPipelineShaderStageCreateInfo ci{};
        ci.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        ci.stage = mShaderType;
        ci.module = mShaderModule;
        ci.pName = "main";
        ci.pSpecializationInfo = nullptr;

        return ci;
    }

    VkShaderModule HVkShader::createShaderModule(VkDevice device, const std::vector<char> &shaderSpirV)
    {
        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = shaderSpirV.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderSpirV.data());

        VkShaderModule shaderModule;
        if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create shader module!");
        }

        return shaderModule;
    }
}
