/**
 * @file Engine/VkUtil.cpp
 * @author Tomas Polasek
 * @brief Utilities for the Vulkan engine.
 */

#include "Engine/mVkUtil.h"

namespace mVkU
{
    bool checkValidationLayers(const std::vector<const char*> &layers)
    {
        // Get number of layers.
        uint32_t layerCount{0};
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        // Get the actual layers.
        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        // Check that all of the validation layers are available.
        uint32_t supportedValidLayers{0};
        for (const auto validationLayer : layers)
        {
            for (const auto &layer : availableLayers)
            {
                if (std::strcmp(validationLayer, layer.layerName) == 0)
                {
                    supportedValidLayers++;
                    break;
                }
            }
        }

        // Check, if the number of supported layers is the same as the number of requested layers.
        return supportedValidLayers == layers.size();
    }

    void uvkPrintExtensions()
    {
        uint32_t extensionCount{0};
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
        std::vector<VkExtensionProperties> supportedExtensions(extensionCount);
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, supportedExtensions.data());

        std::cout << "Supported extensions : " << extensionCount << std::endl;
        for (const auto &extension : supportedExtensions)
        {
            std::cout << "\t" << extension.extensionName << " (" << extension.specVersion << ")\n";
        }
        std::cout << "End of extensions list." << std::endl;
    }

    VKAPI_ATTR VkBool32 VKAPI_CALL uvkDebugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objectType,
        uint64_t object,
        size_t location,
        int32_t messageCode,
        const char* pLayerPrefix,
        const char* pMessage,
        void* pUserData)
    {
        std::cerr << pLayerPrefix << " : " << pMessage << std::endl;

        return VK_FALSE;
    }

    VkResult createDebugReportCallbackEXT(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT *createInfo,
        const VkAllocationCallbacks *allocator,
        VkDebugReportCallbackEXT *callback)
    {
        auto createFun = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(
            vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT")
        );
        if (createFun)
        {
            return createFun(instance, createInfo, allocator, callback);
        }

        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

    void destroyDebugReportCallbackEXT(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks *allocator)
    {
        auto destroyFun = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(
            vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT")
        );
        if (destroyFun)
        {
            destroyFun(instance, callback, allocator);
        }
    }

    bool checkDeviceExtensionSupport(VkPhysicalDevice device,
                                     const std::vector<const char*> &reqExtensions)
    {
        // Get the number of extensions.
        uint32_t extensionCount{0};
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

        // Get the actual extensions.
        std::vector<VkExtensionProperties> extensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, extensions.data());

        // Make sure all of the required extensions are supported.
        for (const auto &reqExtension : reqExtensions)
        {
            bool supported{false};

            for (const auto &extension : extensions)
            {
                if (std::strcmp(reqExtension, extension.extensionName) == 0)
                {
                    supported = true;
                }
            }

            if (!supported)
            {
                return false;
            }
        }

        return true;
    }

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
    {
        if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
        { // We are free to use any format we want.
            return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
        }

        for (const auto &format : availableFormats)
        {
            if (format.format == VK_FORMAT_B8G8R8A8_UNORM &&
                format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            { // We found our preferred format.
                return format;
            }
        }

        // Preferred format not found, return the first one.
        return availableFormats[0];
    }

    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &presentModes,
                                           bool vSync)
    {
        // VK_PRESENT_MODE_FIFO_KGR should be supported, but some drivers don't support it.
        VkPresentModeKHR fallbackMode{VK_PRESENT_MODE_FIFO_KHR};

        for (const auto &mode : presentModes)
        {
            if (mode == VK_PRESENT_MODE_MAILBOX_KHR && !vSync)
            { // Our preferred one.
                return mode;
            }
            else if (mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
            {
                fallbackMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
            }
        }

        return fallbackMode;
    }

    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities,
                                uint32_t width, uint32_t height)
    {
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
        { // We can use the specified resolution.
            return capabilities.currentExtent;
        }
        else
        { // Choose resolution supported by window manager.
            VkExtent2D preferredExtent{width, height};

            // Select resolution acceptable by window manager.
            preferredExtent.width = std::max(capabilities.minImageExtent.width,
                                             std::min(capabilities.maxImageExtent.width,
                                                      preferredExtent.width));
            preferredExtent.height = std::max(capabilities.minImageExtent.height,
                                              std::min(capabilities.maxImageExtent.height,
                                                       preferredExtent.height));

            return preferredExtent;
        }
    }

    const char *shaderExtension(VkShaderStageFlags shaderType)
    {
        switch (shaderType)
        {
            case VK_SHADER_STAGE_VERTEX_BIT:
                return ".vert";
            case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
                return ".tesc";
            case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
                return ".tese";
            case VK_SHADER_STAGE_GEOMETRY_BIT:
                return ".geom";
            case VK_SHADER_STAGE_FRAGMENT_BIT:
                return ".frag";
            case VK_SHADER_STAGE_COMPUTE_BIT:
                return ".comp";
            default:
                return "";
        }
    }

    uint32_t findMemoryType(VkPhysicalDevice physDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
    {
        VkPhysicalDeviceMemoryProperties memProps{};
        vkGetPhysicalDeviceMemoryProperties(physDevice, &memProps);

        for (uint32_t iii = 0; iii < memProps.memoryTypeCount; ++iii)
        {
            // Check the type of the memory and supported properties are all available.
            if ((typeFilter & (1 << iii)) &&
                (memProps.memoryTypes[iii].propertyFlags & properties) == properties)
            {
                return iii;
            }
        }

        throw std::runtime_error("Unable to find memory with required type/properties.");
    }

    VkFormat findSupportedFormat(VkPhysicalDevice physDevice,
                                 const std::initializer_list<VkFormat>& candidates,
                                 VkImageTiling tiling, VkFormatFeatureFlags features)
    {
        for (auto &format : candidates)
        {
            VkFormatProperties properties;
            vkGetPhysicalDeviceFormatProperties(physDevice, format, &properties);
            if (tiling == VK_IMAGE_TILING_OPTIMAL &&
                (properties.optimalTilingFeatures & features) == features)
            {
                return format;
            }
            else if (tiling == VK_IMAGE_TILING_LINEAR &&
                     (properties.linearTilingFeatures & features) == features)
            {
                return format;
            }
        }

        throw std::runtime_error("Unable to find supported format!");
    }

    VkFormat findDepthFormat(VkPhysicalDevice physDevice)
    {
        return findSupportedFormat(
                physDevice,
                {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT},
                VK_IMAGE_TILING_OPTIMAL,
                VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
    }

    bool hasStencilComponent(VkFormat format)
    {
        return format == VK_FORMAT_D32_SFLOAT_S8_UINT ||
                format == VK_FORMAT_D24_UNORM_S8_UINT;
    }

    VkImageAspectFlags imageAspects(VkFormat format)
    {
        if (format == VK_FORMAT_D16_UNORM ||
            format == VK_FORMAT_X8_D24_UNORM_PACK32 ||
            format == VK_FORMAT_D32_SFLOAT)
        {
            return VK_IMAGE_ASPECT_DEPTH_BIT;
        }
        else if (format == VK_FORMAT_D16_UNORM_S8_UINT ||
            format == VK_FORMAT_D24_UNORM_S8_UINT ||
            format == VK_FORMAT_D32_SFLOAT_S8_UINT)
        {
            return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
        }
        else
        {
            return VK_IMAGE_ASPECT_COLOR_BIT;
        }
    }

    void createImageView(VkDevice device,
                         VkImage image, VkFormat format,
                         VkImageAspectFlags aspectFlags,
                         VkImageView &imageView)
    {
        // Create the image view.
        VkImageViewCreateInfo imageViewCI{};
        imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCI.image = image;
        imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCI.format = format;
        imageViewCI.components.r = VK_COMPONENT_SWIZZLE_R;
        imageViewCI.components.g = VK_COMPONENT_SWIZZLE_G;
        imageViewCI.components.b = VK_COMPONENT_SWIZZLE_B;
        imageViewCI.components.a = VK_COMPONENT_SWIZZLE_A;
        imageViewCI.subresourceRange.aspectMask = aspectFlags;
        imageViewCI.subresourceRange.baseMipLevel = 0;
        imageViewCI.subresourceRange.levelCount = 1;
        imageViewCI.subresourceRange.baseArrayLayer = 0;
        imageViewCI.subresourceRange.layerCount = 1;
        if (vkCreateImageView(device, &imageViewCI, nullptr, &imageView) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create image view!");
        }
    }
}

