/**
 * @file Engine/mVkBuffer.cpp
 * @author Tomas Polasek
 * @brief Vulkan buffer abstraction.
 */

#include "Engine/mVkBuffer.h"

namespace mVkH
{

    HVkBuffer::HVkBuffer(VkDevice device, VkPhysicalDevice physDevice,
                         VkDeviceSize size, VkBufferUsageFlags usage,
                         VkMemoryPropertyFlags properties)
    {
        create(device, physDevice, size, usage, properties);
    }

    HVkBuffer::HVkBuffer() :
        mDevice{VK_NULL_HANDLE}
    { }

    void HVkBuffer::create(VkDevice device, VkPhysicalDevice physDevice,
                           VkDeviceSize size, VkBufferUsageFlags usage,
                           VkMemoryPropertyFlags properties)
    {
        mDevice = device;
        mSize = size;

        // Create the buffer.
        VkBufferCreateInfo bufferCI{};
        bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        // Specify size of the buffer.
        bufferCI.size = size;
        // Presumed usage of the buffer.
        bufferCI.usage = usage;
        // Buffer will be used only from a single queue.
        bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        if (vkCreateBuffer(mDevice, &bufferCI, nullptr, &mBuffer) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create buffer!");
        }

        // Check how much memory will the buffer need.
        VkMemoryRequirements memReq{};
        vkGetBufferMemoryRequirements(mDevice, mBuffer, &memReq);

        // Allocate memory for the buffer.
        VkMemoryAllocateInfo memAI{};
        memAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        // Pass the required size.
        memAI.allocationSize = memReq.size;
        // Find suitable memory.
        memAI.memoryTypeIndex = mVkU::findMemoryType(physDevice, memReq.memoryTypeBits, properties);

        if (vkAllocateMemory(mDevice, &memAI, nullptr, &mMemory) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate vertex memory!");
        }

        // Connect the allocated memory with the buffer.
        vkBindBufferMemory(mDevice, mBuffer, mMemory, 0);
    }

    void HVkBuffer::destroy()
    {
        if (mMemory != VK_NULL_HANDLE)
        {
            vkFreeMemory(mDevice, mMemory, nullptr);
        }
        mMemory = VK_NULL_HANDLE;

        if (mBuffer != VK_NULL_HANDLE)
        {
            vkDestroyBuffer(mDevice, mBuffer, nullptr);
        }
        mBuffer = VK_NULL_HANDLE;
    }

    HVkBuffer::~HVkBuffer()
    {
        destroy();
    }

    void HVkBuffer::copyFromBuffer(VkCommandBuffer cmdBuffer, VkBuffer srcBuffer,
                                   VkDeviceSize srcOffset, VkDeviceSize dstOffset,
                                   VkDeviceSize size)
    {
        VkBufferCopy bufferCopy{};
        bufferCopy.srcOffset = srcOffset;
        bufferCopy.dstOffset = dstOffset;
        bufferCopy.size = size;
        vkCmdCopyBuffer(cmdBuffer, srcBuffer, mBuffer, 1, &bufferCopy);
    }
}
