/**
 * @file Engine/mVkSampler.cpp
 * @author Tomas Polasek
 * @brief Vulkan sampler abstraction.
 */

#include "Engine/mVkSampler.h"

namespace mVkH
{

    void HVkSampler::create(VkDevice device, VkFilter mag, VkFilter min,
                            VkSamplerAddressMode mode, VkBool32 anisotropy,
                            VkBorderColor borderColor)
    {
        mDevice = device;

        VkSamplerCreateInfo ci{};
        ci.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        ci.flags = 0;

        ci.magFilter = mag;
        ci.minFilter = min;

        ci.addressModeU = mode;
        ci.addressModeV = mode;
        ci.addressModeW = mode;

        ci.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        ci.mipLodBias = 0.0f;
        ci.minLod = 0.0f;
        ci.maxLod = 0.0f;

        ci.compareEnable = VK_FALSE;
        ci.compareOp = VK_COMPARE_OP_ALWAYS;

        ci.anisotropyEnable = anisotropy;
        ci.maxAnisotropy = 16.0f;
        ci.borderColor = borderColor;
        ci.unnormalizedCoordinates = VK_FALSE;

        if (vkCreateSampler(mDevice, &ci, nullptr, &mSampler) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create texture sampler!");
        }
    }

    void HVkSampler::destroy()
    {
        if (mSampler != VK_NULL_HANDLE)
        {
            vkDestroySampler(mDevice, mSampler, nullptr);
        }
        mSampler = VK_NULL_HANDLE;
    }

    HVkSampler::~HVkSampler()
    {
        destroy();
    }
}
