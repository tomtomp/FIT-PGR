/**
 * @file Engine/mVkCommandBuffer.cpp
 * @author Tomas Polasek
 * @brief Vulkan command buffer abstraction.
 */

#include "Engine/mVkCommandBuffer.h"

namespace mVkH
{
    HVkCommandBuffer::HVkCommandBuffer(VkDevice device, VkCommandPool cmdPool) :
        mDevice{device}, mCmdPool{cmdPool}
    {
        // TODO - Create a permanent command buffer for temporary commands. (VK_COMMAND_POOL_CREATE_TRANSIENT_BIT)

        // Create a temporary command buffer for the commands.
        VkCommandBufferAllocateInfo commandBufferCI{};
        commandBufferCI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferCI.commandPool = cmdPool;
        commandBufferCI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferCI.commandBufferCount = 1;

        if (vkAllocateCommandBuffers(device, &commandBufferCI, &mCmdBuffer) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate copy command buffer!");
        }
    }

    HVkCommandBuffer::~HVkCommandBuffer()
    {
        if (mCmdBuffer != VK_NULL_HANDLE)
        {
            vkFreeCommandBuffers(mDevice, mCmdPool, 1, &mCmdBuffer);
        }
    }

    VkCommandBuffer HVkCommandBuffer::beginCommands()
    {
        // Start recording commands.
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(mCmdBuffer, &beginInfo);

        return mCmdBuffer;
    }

    void HVkCommandBuffer::endCommands(VkQueue submitQueue)
    {
        // Finalize the command buffer.
        vkEndCommandBuffer(mCmdBuffer);

        // Submit recorded commands for processing.
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = 0;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &mCmdBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        // Submit the copy command and wait for it to finish.
        vkQueueSubmit(submitQueue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(submitQueue);
    }
}
