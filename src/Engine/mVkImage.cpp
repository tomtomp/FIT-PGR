/**
 * @file Engine/mVkImage.cpp
 * @author Tomas Polasek
 * @brief Vulkan image abstraction.
 */

#include "Engine/mVkImage.h"

namespace mVkH
{
    HVkImageView::HVkImageView(VkDevice device, VkImage image,
                               VkFormat format, VkImageAspectFlags aspectFlags)
    {
        create(device, image, format, aspectFlags);
    }

    void HVkImageView::destroy()
    {
        if (mImageView != VK_NULL_HANDLE)
        {
            vkDestroyImageView(mDevice, mImageView, nullptr);
        }
        mImageView = VK_NULL_HANDLE;
    }

    HVkImageView::~HVkImageView()
    {
        destroy();
    }

    HVkImageView::HVkImageView() :
        mDevice{VK_NULL_HANDLE}
    { }

    void HVkImageView::create(VkDevice device, VkImage image,
                              VkFormat format, VkImageAspectFlags aspectFlags)
    {
        mDevice = device;
        mVkU::createImageView(mDevice, image, format, aspectFlags, mImageView);
    }

    HVkImage::HVkImage(VkDevice device, VkPhysicalDevice physDevice,
                       uint32_t width, uint32_t height, VkFormat format,
                       VkSampleCountFlagBits samples, VkImageTiling tiling,
                       VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags)
    {
        create(device, physDevice, width, height, format,
               samples, tiling, usageFlags, memFlags);
    }

    HVkImage::HVkImage() :
        mDevice{VK_NULL_HANDLE}
    { }

    void HVkImage::create(VkDevice device, VkPhysicalDevice physDevice,
                          uint32_t width, uint32_t height, VkFormat format,
                          VkSampleCountFlagBits samples, VkImageTiling tiling,
                          VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags)
    {
        mDevice = device;
        mWidth = width;
        mHeight = height;
        mFormat = format;
        mSamples = samples;

        // Create the image.
        VkImageCreateInfo imageCI{};
        imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCI.flags = 0;
        imageCI.imageType = VK_IMAGE_TYPE_2D;
        imageCI.format = format;
        imageCI.extent.width = width;
        imageCI.extent.height = height;
        imageCI.extent.depth = 1;
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = 1;
        imageCI.samples = samples;
        imageCI.tiling = tiling;
        imageCI.usage = usageFlags;
        imageCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        imageCI.queueFamilyIndexCount = 0;
        imageCI.pQueueFamilyIndices = nullptr;
        imageCI.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        if (vkCreateImage(mDevice, &imageCI, nullptr, &mImage) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create image!");
        }

        // Allocate memory for the image.
        VkMemoryRequirements memoryReqs{};
        vkGetImageMemoryRequirements(mDevice, mImage, &memoryReqs);
        VkMemoryAllocateInfo memoryAI{};
        memoryAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        memoryAI.allocationSize = memoryReqs.size;
        memoryAI.memoryTypeIndex = mVkU::findMemoryType(physDevice, memoryReqs.memoryTypeBits, memFlags);
        if (vkAllocateMemory(mDevice, &memoryAI, nullptr, &mImageMemory) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate memory for image!");
        }

        // Bind the allocated memory to the MSAA image.
        vkBindImageMemory(mDevice, mImage, mImageMemory, 0);
    }

    VkImageView HVkImage::createView(VkFormat format, VkImageAspectFlags aspectFlags)
    {
        mImageView.create(mDevice, mImage, format, aspectFlags);
        return mImageView.view();
    }

    void HVkImage::copyFromBuffer(VkCommandBuffer cmdBuffer, VkBuffer buffer)
    {
        VkBufferImageCopy bufferImageCopy{};
        // We are copying into the buffer, no need to specify.
        bufferImageCopy.bufferOffset = 0;
        bufferImageCopy.bufferRowLength = 0;
        bufferImageCopy.bufferImageHeight = 0;
        // No mip-mapping or layers.
        bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferImageCopy.imageSubresource.mipLevel = 0;
        bufferImageCopy.imageSubresource.baseArrayLayer = 0;
        bufferImageCopy.imageSubresource.layerCount = 1;
        // Copy whole image.
        bufferImageCopy.imageOffset = { 0, 0, 0};
        bufferImageCopy.imageExtent.width = mWidth;
        bufferImageCopy.imageExtent.height = mHeight;
        bufferImageCopy.imageExtent.depth = 1;

        // Specify the copy command.
        vkCmdCopyBufferToImage(
                cmdBuffer,
                buffer, image(),
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1, &bufferImageCopy);
    }

    void HVkImage::transitionLayout(VkCommandBuffer cmdBuffer,
                                    VkImageLayout oldLayout, VkImageLayout newLayout,
                                    VkAccessFlags srcAccess, VkPipelineStageFlags srcStage,
                                    VkAccessFlags dstAccess, VkPipelineStageFlags dstStage)
    {
        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        // Specify the requested layout transition.
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = mImage;
        barrier.subresourceRange.aspectMask = mVkU::imageAspects(mFormat);
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        // TODO - Move to the usage of this method.
        /*
        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
            newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        {
            // Preparing image for receiving data.
            // Can start at any time.
            barrier.srcAccessMask = 0;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            // Operations which require write access should wait.
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        }
        else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
                 newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        {
            // Prepare depth/stencil buffer for usage in the pipeline.
            // Can start at any time.
            barrier.srcAccessMask = 0;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            // Should be completed before access for early tests.
            barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            dstStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        }
        else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
                 newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            // Prepare loaded image for sampling.
            // Wait for the copy operation to complete.
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            // Sampled from fragment shader.
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        }
        */

        // Add command to perform the transition.
        vkCmdPipelineBarrier(
                cmdBuffer,
                srcStage, dstStage,
                0,
                0, nullptr,
                0, nullptr,
                1, &barrier
        );
    }

    VkAttachmentDescription HVkImage::attachmentDescription(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp,
                                                            VkImageLayout initial, VkImageLayout final)
    {
        VkAttachmentDescription desc{};
        desc.format = mFormat;
        desc.samples = mSamples;
        desc.loadOp = loadOp;
        desc.storeOp = storeOp;
        desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        desc.initialLayout = initial;
        desc.finalLayout = final;

        return desc;
    }

    void HVkImage::destroy()
    {
        mImageView.destroy();

        if (mImageMemory != VK_NULL_HANDLE)
        {
            vkFreeMemory(mDevice, mImageMemory, nullptr);
        }
        if (mImage != VK_NULL_HANDLE)
        {
            vkDestroyImage(mDevice, mImage, nullptr);
        }

        mImageMemory = VK_NULL_HANDLE;
        mImage = VK_NULL_HANDLE;
    }

    HVkImage::~HVkImage()
    {
        destroy();
    }
}
