/**
 * @file Engine/VulkanEngine.cpp
 * @author Tomas Polasek
 * @brief Connection between application and Vulkan API.
 */

#include <Engine/mVkCommandBuffer.h>
#include "Engine/VulkanEngine.h"

namespace mVkEngineConf
{
    VkPhysicalDeviceFeatures reqDeviceFeatures()
    {
        VkPhysicalDeviceFeatures deviceFeatures{};
        // MSAA
        deviceFeatures.samplerAnisotropy = VK_TRUE;
        // Tesselation for LOD
        deviceFeatures.tessellationShader = VK_TRUE;
        // Wireframe drawing
        deviceFeatures.fillModeNonSolid = VK_TRUE;

        return deviceFeatures;
    }
}

namespace mVkE
{

    void VulkanEngine::initialize(mVkU::SurfaceCreateCb surfaceCb,
                                  void *cData,
                                  uint32_t width, uint32_t height,
                                  std::vector<const char*> extensions)
    {
        mVkU::uvkPrintExtensions();

        createInstance(extensions);
        setupDebugCallback();
        surfaceCb(cData, mVk.base.instance, nullptr, &mVk.base.surface);
        ASSERT_FATAL(mVk.base.surface != VK_NULL_HANDLE);

        // Prepare device.
        pickPhysicalDevice();
        createLogicalDevice();
        getDeviceQueues();

        // Prepare common Vulkan objects.
        createSwapChain(width, height);
        createDepthBuffer();
        createMultisampleBuffers();
        createCommandPool();
        createDescriptorPool();

        // Prepare resources used in the application.
        createAppBuffers();
        createAppSamplers();
        createAppSync();

        // Prepare framebuffer and presentation specific to this application.
        createAppRenderPass();
        createAppFramebuffers();
    }

    void VulkanEngine::finalize()
    {
        // Prepare height-map rendering objects.
        prepareHeightMap();

        // Prepare descriptors for communication with shaders.
        createAppDescriptorSetLayout();
        createAppDescriptorSet();

        // Assemble the rendering pipeline and create rendering commands.
        createAppGraphicsPipeline();
        hmCreateCommand();
        createAppCommandBuffers(mVk.pipelines.terrain, mVk.mRendPass.commands);
        createAppCommandBuffers(mVk.pipelines.terrainWf, mVk.mRendPass.commandsWf);
    }

    void VulkanEngine::resize(uint32_t width, uint32_t height)
    {
        vkDeviceWaitIdle(mVk.base.device);

        cleanupSwapChain();

        createSwapChain(width, height);
        createMultisampleBuffers();
        createDepthBuffer();
        createAppRenderPass();
        createAppGraphicsPipeline();
        createAppFramebuffers();
        createAppCommandBuffers(mVk.pipelines.terrain, mVk.mRendPass.commands);
        createAppCommandBuffers(mVk.pipelines.terrainWf, mVk.mRendPass.commandsWf);
    }

    void VulkanEngine::cleanup()
    {
        cleanupSwapChain();
        mVk.swapChain.swapChain.destroy(true);

        cleanupHeightMap();

        vkDestroySemaphore(mVk.base.device, mVk.sync.heightMapAvailable, nullptr);
        vkDestroySemaphore(mVk.base.device, mVk.sync.renderFinished, nullptr);
        vkDestroySemaphore(mVk.base.device, mVk.sync.imageAvailable, nullptr);

        mVk.samplers.terrain.destroy();
        mVk.samplers.terrainImage.destroy();

        mVk.buffers.index.destroy();
        mVk.buffers.vertex.destroy();

        mVk.buffers.uniform.destroy();

        mVk.descriptors.mainPool.destroy();
        vkDestroyCommandPool(mVk.base.device, mVk.base.cmdPool, nullptr);

        mVk.descriptors.terrainDesc.destroy();

        vkDestroyDevice(mVk.base.device, nullptr);

        vkDestroySurfaceKHR(mVk.base.instance, mVk.base.surface, nullptr);
        mVkU::destroyDebugReportCallbackEXT(mVk.base.instance, mVk.base.debugCallback, nullptr);
        vkDestroyInstance(mVk.base.instance, nullptr);
    }

    void VulkanEngine::updateUBO(mVkEngineConf::HHVkDescriptors::UniformBufferObject &uniforms,
                                 mVkEngineConf::HHVkDescriptors::HeightMapUBO &hmUniforms)
    {
        void *data{nullptr};
        {
            mVkH::ScopeMemoryMapper mapper(mVk.base.device, mVk.buffers.uniform.memory(),
                                           0, mVk.buffers.uniform.size(), 0, &data);
            memcpy(data, &uniforms, sizeof(mVkEngineConf::HHVkDescriptors::UniformBufferObject));
            // Implicit memory unmap.
        }
        {
            mVkH::ScopeMemoryMapper mapper(mVk.base.device, mVk.buffers.hmUniform.memory(),
                                           0, mVk.buffers.hmUniform.size(), 0, &data);
            memcpy(data, &hmUniforms, sizeof(mVkEngineConf::HHVkDescriptors::HeightMapUBO));
            // Implicit memory unmap.
        }
    }

    void VulkanEngine::prepareTerrain(const std::vector<mVkI::Vertex3D> &vertices,
                                      const std::vector<uint32_t> &indices,
                                      uint32_t heightMapRes, uint32_t terrainIndices)
    {
        mVk.buffers.numTerrainIndices = terrainIndices;
        fillVertIndBuffers(vertices, indices);

        uint32_t realRes{heightMapRes};
        mVk.hmRendPass.resolution = realRes;
        mVk.hmRendPass.ext.width = realRes;
        mVk.hmRendPass.ext.height = realRes;
    }

    void VulkanEngine::submitDrawCommands(bool wireframe)
    {
        // Explicitly synchronize with GPU, to prevent memory leak with validation layers.
        if (mVkEngineConf::VALIDATION_LAYERS_ENABLE)
        {
            vkQueueWaitIdle(mVk.queues.graphicsQueue);
        }

        // Get index of the next image in the swap chain.
        uint32_t imageIndex{0};
        auto result = mVk.swapChain.swapChain.nextImage(mVk.sync.imageAvailable,
                                                        VK_NULL_HANDLE, imageIndex);

        if (result == VK_ERROR_OUT_OF_DATE_KHR)
        { // Unusable, we need to recreate swap chain.
            //recreateSwapChain();
            // Wait for the window to send the resize event.
            return;
        }
        else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
        { // Function call failed.
            throw std::runtime_error("Unable to acquire next image!");
        }

        // Setup height-map draw command.
        auto hmWait = {
                mVk.sync.imageAvailable
        };
        auto hmSignal = {
                mVk.sync.heightMapAvailable
        };
        auto hmWaitStage = {
                VkPipelineStageFlags(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
        };
        VkSubmitInfo hmSI{};
        hmSI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        hmSI.waitSemaphoreCount = static_cast<uint32_t>(hmWait.size());
        hmSI.pWaitSemaphores = hmWait.begin();
        hmSI.pWaitDstStageMask = hmWaitStage.begin();
        hmSI.commandBufferCount = 1;
        hmSI.pCommandBuffers = &mVk.hmRendPass.command;
        hmSI.signalSemaphoreCount = static_cast<uint32_t>(hmSignal.size());
        hmSI.pSignalSemaphores = hmSignal.begin();
        // Submit the draw command.
        if (vkQueueSubmit(mVk.queues.graphicsQueue, 1, &hmSI, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to sumbit draw command!");
        }

        // Setup terrain draw command.
        auto terrainWait = {
                mVk.sync.heightMapAvailable
        };
        auto terrainSignal = {
                mVk.sync.renderFinished
        };
        auto terrainWaitStage = {
                VkPipelineStageFlags(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
        };
        VkSubmitInfo terrainSI{};
        terrainSI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        terrainSI.waitSemaphoreCount = static_cast<uint32_t>(terrainWait.size());
        terrainSI.pWaitSemaphores = terrainWait.begin();
        terrainSI.pWaitDstStageMask = terrainWaitStage.begin();
        terrainSI.commandBufferCount = 1;
        terrainSI.pCommandBuffers = &(wireframe ? mVk.mRendPass.commandsWf :
                                      mVk.mRendPass.commands)[imageIndex];
        terrainSI.signalSemaphoreCount = static_cast<uint32_t>(terrainSignal.size());
        terrainSI.pSignalSemaphores = terrainSignal.begin();
        // Submit the draw command.
        if (vkQueueSubmit(mVk.queues.graphicsQueue, 1, &terrainSI, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to sumbit draw command!");
        }

        // Setup presentation for the rendered image.
        auto swapChains = {
                mVk.swapChain.swapChain.swapchain()
        };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        // Wait for the image to be ready.
        presentInfo.waitSemaphoreCount = static_cast<uint32_t>(terrainSignal.size());
        presentInfo.pWaitSemaphores = terrainSignal.begin();
        presentInfo.swapchainCount = static_cast<uint32_t>(swapChains.size());
        presentInfo.pSwapchains = swapChains.begin();
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr;

        result = vkQueuePresentKHR(mVk.queues.graphicsQueue, &presentInfo);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        { // We should recreate the swap chain.
            //recreateSwapChain();
            // Wait for the window to send the resize event.
            return;
        }
        else if (result != VK_SUCCESS)
        { // Function call failed.
            throw std::runtime_error("Unable to present image!");
        }
    }

    void VulkanEngine::deviceWaitIdle()
    {
        vkDeviceWaitIdle(mVk.base.device);
    }

    void VulkanEngine::cleanupSwapChain()
    {
        mVk.mRendPass.framebuffer.destroy();

        mVk.swapChain.msaa.color.destroy();
        mVk.swapChain.msaa.depth.destroy();
        mVk.swapChain.depthResolve.destroy();

        // TODO - Use vkResetCommandBuffer(...) instead?
        vkFreeCommandBuffers(mVk.base.device, mVk.base.cmdPool,
                             static_cast<uint32_t>(mVk.mRendPass.commandsWf.size()),
                             mVk.mRendPass.commandsWf.data());
        vkFreeCommandBuffers(mVk.base.device, mVk.base.cmdPool,
                             static_cast<uint32_t>(mVk.mRendPass.commands.size()),
                             mVk.mRendPass.commands.data());

        vkDestroyPipeline(mVk.base.device, mVk.pipelines.skySphere, nullptr);
        vkDestroyPipeline(mVk.base.device, mVk.pipelines.terrainWf, nullptr);
        vkDestroyPipeline(mVk.base.device, mVk.pipelines.terrain, nullptr);
        vkDestroyPipelineLayout(mVk.base.device, mVk.pipelines.terrainLayout, nullptr);

        vkDestroyRenderPass(mVk.base.device, mVk.mRendPass.renderPass, nullptr);

        mVk.swapChain.swapChain.destroy(false);
    }

    void VulkanEngine::createInstance(const std::vector<const char*> &extensions)
    {
        if (!mVkU::checkValidationLayers(mVkEngineConf::VALIDATION_LAYERS))
        {
            throw std::runtime_error("Validation layers are enabled but not available!");
        }

        // Basic application information.
        VkApplicationInfo appInfo{};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "Vulkan Engine";
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = "No Engine";
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        auto requiredExtensions = getRequiredExtensions(extensions);

        VkInstanceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;
        // Extensions which are required to run the application.
        createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
        createInfo.ppEnabledExtensionNames = requiredExtensions.data();
        if (mVkEngineConf::VALIDATION_LAYERS_ENABLE)
        {
            createInfo.enabledLayerCount = static_cast<uint32_t>(mVkEngineConf::VALIDATION_LAYERS.size());
            createInfo.ppEnabledLayerNames = mVkEngineConf::VALIDATION_LAYERS.data();
        }
        else
        {
            createInfo.enabledLayerCount = 0;
            createInfo.ppEnabledLayerNames = nullptr;
        }

        if (vkCreateInstance(&createInfo, nullptr, &mVk.base.instance) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to create Vulkan instance.");
        }
    }

    void VulkanEngine::setupDebugCallback()
    {
        if (!mVkEngineConf::VALIDATION_LAYERS_ENABLE)
        {
            return;
        }

        // Callback for warning and error messages.
        VkDebugReportCallbackCreateInfoEXT createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        createInfo.pfnCallback = mVkU::uvkDebugCallback;

        if (mVkU::createDebugReportCallbackEXT(mVk.base.instance, &createInfo,
                                               nullptr, &mVk.base.debugCallback) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to set debug report callback!");
        }
    }

    void VulkanEngine::pickPhysicalDevice()
    {
        // Get number of devices.
        uint32_t deviceCount{0};
        vkEnumeratePhysicalDevices(mVk.base.instance, &deviceCount, nullptr);
        if (deviceCount == 0)
        {
            throw std::runtime_error("No suitable devices found!");
        }

        // Get the actual devices.
        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(mVk.base.instance, &deviceCount, devices.data());

        // Choose the preferred device.
        for (const auto &device : devices)
        {
            if (isDeviceSuitable(device))
            {
                mVk.base.physicalDevice = device;
                break;
            }
        }

        if (mVk.base.physicalDevice == VK_NULL_HANDLE)
        {
            throw std::runtime_error("No suitable devices found!");
        }
    }

    void VulkanEngine::createLogicalDevice()
    {
        mVk.queues.queueFamilies.fill(mVk.base.physicalDevice, mVk.base.surface);
        auto &indices = mVk.queues.queueFamilies;

        // Vector of queue creation information.
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        // Unique-ify the indices.
        auto indVec = indices.indexList();
        std::set<uint32_t> queueFamilyIndices(indVec.begin(), indVec.end());

        float queuePriority{1.0f};
        for (auto familyIndex : queueFamilyIndices)
        { // For each required queue family.
            VkDeviceQueueCreateInfo qCreateInfo{};
            qCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            qCreateInfo.queueFamilyIndex = familyIndex;
            qCreateInfo.queueCount = 1;
            qCreateInfo.pQueuePriorities = &queuePriority;
            queueCreateInfos.emplace_back(qCreateInfo);
        }

        // Specify required queues.
        VkDeviceCreateInfo dCreateInfo{};
        dCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        dCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
        dCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());

        auto deviceFeatures = mVkEngineConf::reqDeviceFeatures();

        dCreateInfo.pEnabledFeatures = &deviceFeatures;

        dCreateInfo.enabledExtensionCount = static_cast<uint32_t>(mVkEngineConf::DEVICE_EXTENSIONS.size());
        dCreateInfo.ppEnabledExtensionNames = mVkEngineConf::DEVICE_EXTENSIONS.data();

        if (mVkEngineConf::VALIDATION_LAYERS_ENABLE)
        {
            dCreateInfo.enabledLayerCount = static_cast<uint32_t>(mVkEngineConf::VALIDATION_LAYERS.size());
            dCreateInfo.ppEnabledLayerNames = mVkEngineConf::VALIDATION_LAYERS.data();
        }
        else
        {
            dCreateInfo.enabledLayerCount = 0;
        }

        if (vkCreateDevice(mVk.base.physicalDevice, &dCreateInfo, nullptr, &mVk.base.device) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create logical device!");
        }
    }

    void VulkanEngine::getDeviceQueues()
    {
        // Get the required queues.
        vkGetDeviceQueue(mVk.base.device,
                         static_cast<uint32_t>(mVk.queues.queueFamilies.graphicsFamily),
                         0, &mVk.queues.graphicsQueue);
        vkGetDeviceQueue(mVk.base.device,
                         static_cast<uint32_t>(mVk.queues.queueFamilies.presentFamily),
                         0, &mVk.queues.presentQueue);
    }

    void VulkanEngine::createSwapChain(uint32_t width, uint32_t height)
    {
        mVk.swapChain.swapChain.create(
                mVk.base.device, mVk.base.physicalDevice, mVk.base.surface,
                width, height,
                mVkEngineConf::USE_TRIPLE_BUFFERING,
                mVkEngineConf::USE_VSYNC);
    }

    void VulkanEngine::createDepthBuffer()
    {
        uint32_t width{mVk.swapChain.swapChain.extent().width};
        uint32_t height{mVk.swapChain.swapChain.extent().height};
        VkFormat depthFormat{mVkU::findDepthFormat(mVk.base.physicalDevice)};
        mVk.swapChain.depthResolve.create(
                mVk.base.device,  mVk.base.physicalDevice,
                width, height, depthFormat,
                VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.swapChain.depthResolve.createView(depthFormat,
                                              mVkU::imageAspects(depthFormat));
    }

    void VulkanEngine::createMultisampleBuffers()
    {
        uint32_t width{mVk.swapChain.swapChain.extent().width};
        uint32_t height{mVk.swapChain.swapChain.extent().height};
        VkFormat depthFormat{mVkU::findDepthFormat(mVk.base.physicalDevice)};

        mVk.swapChain.msaa.depth.create(
                mVk.base.device,  mVk.base.physicalDevice,
                width, height, depthFormat,
                mVkEngineConf::MULTISAMPLING_SAMPLES, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.swapChain.msaa.depth.createView(depthFormat,
                                            mVkU::imageAspects(depthFormat));

        mVk.swapChain.msaa.color.create(
                mVk.base.device,  mVk.base.physicalDevice,
                width, height, mVk.swapChain.swapChain.format(),
                mVkEngineConf::MULTISAMPLING_SAMPLES, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.swapChain.msaa.color.createView(mVk.swapChain.swapChain.format(),
                                            VK_IMAGE_ASPECT_COLOR_BIT);
    }

    void VulkanEngine::createAppRenderPass()
    {
        // Multisampled render target.
        VkAttachmentDescription multisampleColorAtt =
                mVk.swapChain.msaa.color.attachmentDescription(
                        VK_ATTACHMENT_LOAD_OP_CLEAR,
                        VK_ATTACHMENT_STORE_OP_DONT_CARE,
                        VK_IMAGE_LAYOUT_UNDEFINED,
                        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        // Resolved image for presentation.
        VkAttachmentDescription colorAtt{};
        colorAtt.format = mVk.swapChain.swapChain.format();
        colorAtt.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAtt.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAtt.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAtt.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAtt.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAtt.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Multisampled depth render target.
        VkAttachmentDescription multisampleDepthAtt =
                mVk.swapChain.msaa.depth.attachmentDescription(
                        VK_ATTACHMENT_LOAD_OP_CLEAR,
                        VK_ATTACHMENT_STORE_OP_DONT_CARE,
                        VK_IMAGE_LAYOUT_UNDEFINED,
                        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        // Rsolved depth.
        VkAttachmentDescription depthAtt =
                mVk.swapChain.depthResolve.attachmentDescription(
                        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                        VK_ATTACHMENT_STORE_OP_STORE,
                        VK_IMAGE_LAYOUT_UNDEFINED,
                        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

        auto attachments = {
                multisampleColorAtt,
                colorAtt,
                multisampleDepthAtt,
                depthAtt
        };

        /**
         * For now render pass will contain only one subpass.
         */

        // Dependency, which waits for the destination image to be ready.
        VkSubpassDependency imageReadyDep{};
        // Dependency is at the top.
        imageReadyDep.srcSubpass = VK_SUBPASS_EXTERNAL;
        // The first subpass is the next in line.
        imageReadyDep.dstSubpass = 0;
        /*
         * Wait for the color attachment to be ready.
         * Dependency should wait for all commands in the color attachment
         * stage to finish, before continuing.
         */
        imageReadyDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        imageReadyDep.srcAccessMask = 0;
        /*
         * Operations in the next stage should wait for this dependency, if
         * they are in the color attachment stage, which read or write from/to the
         * color attachment.
         */
        imageReadyDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        imageReadyDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                      VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        imageReadyDep.dependencyFlags = 0;

        // ### First subpass, rendering into colormap. ###

        // MSAA target color buffer.
        VkAttachmentReference colorAttMSAARef{};
        colorAttMSAARef.attachment = 0;
        colorAttMSAARef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        // Resolve color buffer.
        VkAttachmentReference colorAttRef{};
        colorAttRef.attachment = 1;
        colorAttRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        // Multisampled depth buffer.
        VkAttachmentReference msaaDepthAttRef{};
        msaaDepthAttRef.attachment = 2;
        msaaDepthAttRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription drawSubpassDesc{};
        // We are using graphics shaders, not compute.
        drawSubpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        // No input attachments.
        drawSubpassDesc.inputAttachmentCount = 0;
        drawSubpassDesc.pInputAttachments = nullptr;
        // Use 1 color attachment.
        drawSubpassDesc.colorAttachmentCount = 1;
        drawSubpassDesc.pColorAttachments = &colorAttMSAARef;
        drawSubpassDesc.pResolveAttachments = &colorAttRef;
        drawSubpassDesc.pDepthStencilAttachment = &msaaDepthAttRef;
        // Rest is unused.
        drawSubpassDesc.preserveAttachmentCount = 0;
        drawSubpassDesc.pPreserveAttachments = nullptr;

        // ### End of first subpass. ###

        VkRenderPassCreateInfo renderPassCI{};
        renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        // Bind the main color attachment, for ease of use.
        renderPassCI.attachmentCount = attachments.size();
        renderPassCI.pAttachments = attachments.begin();
        // Use the draw subpass.
        renderPassCI.subpassCount = 1;
        renderPassCI.pSubpasses = &drawSubpassDesc;
        // Add the dependency waiting for image to be ready.
        renderPassCI.dependencyCount = 1;
        renderPassCI.pDependencies = &imageReadyDep;

        if (vkCreateRenderPass(mVk.base.device, &renderPassCI, nullptr, &mVk.mRendPass.renderPass) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create render pass!");
        }
    }

    void VulkanEngine::createAppDescriptorSetLayout()
    {
        // Layout for the uniform buffer object.
        mVk.descriptors.terrainDesc.addBinding(
                // Binding location.
                0,
                // Simple uniform.
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                // One instance.
                1,
                // Accessed in tessellation shaders and fragment shader.
                VK_SHADER_STAGE_FRAGMENT_BIT |
                        VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT |
                        VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
                nullptr
        );

        // Layout for the texture sampler.
        mVk.descriptors.terrainDesc.addBinding(
                // Binding location.
                1,
                // Image sampler.
                VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                // One instance.
                1,
                // Accessed in fragment shader and tess-eval shader.
                VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,
                nullptr
        );

        // Combine bindings into a set layout.
        mVk.descriptors.terrainDesc.createSetLayout(mVk.base.device);
    }

    void VulkanEngine::createAppGraphicsPipeline()
    {
        // Vertex puller settings.
        VkPipelineVertexInputStateCreateInfo vertInputCI{};
        vertInputCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

        // Primitive Assembly settings.
        VkPipelineInputAssemblyStateCreateInfo assemblyCI{};
        assemblyCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        //assemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        assemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
        assemblyCI.primitiveRestartEnable = VK_FALSE;

        // Tessellation settings.
        VkPipelineTessellationStateCreateInfo tessCI{};
        tessCI.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
        // Tessellation primitive is a quad.
        tessCI.patchControlPoints = 4;

        // Viewport scissoring.
        VkRect2D vpScissor{};
        vpScissor.offset = {0, 0};
        // Combine viewport and scissor into viewport state.
        VkPipelineViewportStateCreateInfo vpStateCI{};
        vpStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;

        // Rasterization stage.
        VkPipelineRasterizationStateCreateInfo rasterizationCI{};
        rasterizationCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        // Do not clamp to far/near plane. Useful for shadow mapping.
        rasterizationCI.depthClampEnable = VK_FALSE;
        // Enable rasterization stage.
        rasterizationCI.rasterizerDiscardEnable = VK_FALSE;
        // Fill polygons, VK_POLYGON_MODE_LINE for wireframe.
        rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
        // Cull the back faces of polygons.
        rasterizationCI.cullMode = VK_CULL_MODE_BACK_BIT;
        // Vertices for front should be in clockwise order.
        rasterizationCI.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        // Don't do depth biasing. Useful for shadow mapping.
        rasterizationCI.depthBiasEnable = VK_FALSE;
        rasterizationCI.depthBiasConstantFactor = 0.0f;
        rasterizationCI.depthBiasClamp = 0.0f;
        rasterizationCI.depthBiasSlopeFactor = 0.0f;
        // Normal line width. Wider lines require wideLines GPU feature.
        rasterizationCI.lineWidth = 1.0f;

        // Multisampling settings.
        VkPipelineMultisampleStateCreateInfo multisampleCI{};
        multisampleCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleCI.rasterizationSamples = VK_SAMPLE_COUNT_4_BIT;
        multisampleCI.sampleShadingEnable = VK_FALSE;
        multisampleCI.minSampleShading = 1.0f;
        multisampleCI.pSampleMask = nullptr;
        multisampleCI.alphaToCoverageEnable = VK_FALSE;
        multisampleCI.alphaToOneEnable = VK_FALSE;

        // Settings for stencil/depth buffer.
        VkPipelineDepthStencilStateCreateInfo depthStencilCI{};
        depthStencilCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        // Check < depth.
        depthStencilCI.depthCompareOp = VK_COMPARE_OP_LESS;
        // No bounds checking.
        depthStencilCI.depthBoundsTestEnable = VK_FALSE;
        // No stencil buffer.
        depthStencilCI.stencilTestEnable = VK_FALSE;
        depthStencilCI.front = {};
        depthStencilCI.back = {};
        // Depth from 0.0 (closest) to 1.0 (furthest).
        depthStencilCI.minDepthBounds = 0.0f;
        depthStencilCI.maxDepthBounds = 1.0f;

        // Color blending settings for the main framebuffer.
        VkPipelineColorBlendAttachmentState colorBlendAtt{};
        // No color blending.
        colorBlendAtt.blendEnable = VK_FALSE;
        colorBlendAtt.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAtt.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAtt.colorBlendOp = VK_BLEND_OP_ADD;
        colorBlendAtt.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAtt.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAtt.alphaBlendOp = VK_BLEND_OP_ADD;
        colorBlendAtt.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                       VK_COLOR_COMPONENT_G_BIT |
                                       VK_COLOR_COMPONENT_B_BIT |
                                       VK_COLOR_COMPONENT_A_BIT;

        // Color blending global settings.
        VkPipelineColorBlendStateCreateInfo colorBlendCI{};
        colorBlendCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        // No logical operations required.
        colorBlendCI.logicOpEnable = VK_FALSE;
        colorBlendCI.logicOp = VK_LOGIC_OP_COPY;
        // Only one color attachment.
        colorBlendCI.attachmentCount = 1;
        colorBlendCI.pAttachments = &colorBlendAtt;
        // No blending.
        colorBlendCI.blendConstants[0] = 0.0f;
        colorBlendCI.blendConstants[1] = 0.0f;
        colorBlendCI.blendConstants[2] = 0.0f;
        colorBlendCI.blendConstants[3] = 0.0f;

        // Select attributes which can be changed dynamically.
        VkPipelineDynamicStateCreateInfo dynamicStateCI{};
        dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        static constexpr uint32_t NUM_DYNAMIC_STATES{2};
        VkDynamicState dynamicStates[NUM_DYNAMIC_STATES] = {
                // Changing resolution of the viewport.
                VK_DYNAMIC_STATE_VIEWPORT,
                // Changing line width.
                VK_DYNAMIC_STATE_LINE_WIDTH};
        dynamicStateCI.dynamicStateCount = NUM_DYNAMIC_STATES;
        dynamicStateCI.pDynamicStates = dynamicStates;

        // Pipeline layout and uniform mapping.
        VkPipelineLayoutCreateInfo pipelineLayoutCI{};
        pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCI.pushConstantRangeCount = 0;
        pipelineLayoutCI.pPushConstantRanges = nullptr;

        // Use the main descriptor set.
        auto layouts = mVk.descriptors.terrainDesc.layouts();
        pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(layouts.size());
        pipelineLayoutCI.pSetLayouts = layouts.data();
        if (vkCreatePipelineLayout(mVk.base.device, &pipelineLayoutCI,
                                   nullptr, &mVk.pipelines.terrainLayout) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create main pipeline layout!");
        }

        // Load the terrain shaders.
        std::string terrainShaderBase{std::string(mVkEngineConf::SHADER_FOLDER) +
                                              mVkEngineConf::HHVkPipelines::TERRAIN_SHADER_NAME};
        mVkH::HVkShader tVs(terrainShaderBase, VK_SHADER_STAGE_VERTEX_BIT, mVk.base.device);
        mVkH::HVkShader tTcs(terrainShaderBase, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, mVk.base.device);
        mVkH::HVkShader tTes(terrainShaderBase, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, mVk.base.device);
        mVkH::HVkShader tFs(terrainShaderBase, VK_SHADER_STAGE_FRAGMENT_BIT, mVk.base.device);

        auto terrainShaders = {
                tVs.pipelineShaderStageCreateInfo(),
                tTcs.pipelineShaderStageCreateInfo(),
                tTes.pipelineShaderStageCreateInfo(),
                tFs.pipelineShaderStageCreateInfo()
        };

        // Load the sky sphere shaders.
        std::string skySphereShaderBase{std::string(mVkEngineConf::SHADER_FOLDER) +
                                                mVkEngineConf::HHVkPipelines::SKY_SPHERE_SHADER_NAME};
        mVkH::HVkShader sVs(skySphereShaderBase, VK_SHADER_STAGE_VERTEX_BIT, mVk.base.device);
        mVkH::HVkShader sTcs(skySphereShaderBase, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, mVk.base.device);
        mVkH::HVkShader sTes(skySphereShaderBase, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, mVk.base.device);
        mVkH::HVkShader sFs(skySphereShaderBase, VK_SHADER_STAGE_FRAGMENT_BIT, mVk.base.device);

        auto skySphereShaders = {
                sVs.pipelineShaderStageCreateInfo(),
                sTcs.pipelineShaderStageCreateInfo(),
                sTes.pipelineShaderStageCreateInfo(),
                sFs.pipelineShaderStageCreateInfo()
        };

        // Create the final graphics pipeline.
        VkGraphicsPipelineCreateInfo graphicsPipelineCI{};
        graphicsPipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        // Set information provided by already created structures
        graphicsPipelineCI.pVertexInputState = &vertInputCI;
        graphicsPipelineCI.pInputAssemblyState = &assemblyCI;
        graphicsPipelineCI.pTessellationState = &tessCI;
        graphicsPipelineCI.pViewportState = &vpStateCI;
        graphicsPipelineCI.pRasterizationState = &rasterizationCI;
        graphicsPipelineCI.pMultisampleState = &multisampleCI;
        graphicsPipelineCI.pDepthStencilState = &depthStencilCI;
        graphicsPipelineCI.pColorBlendState = &colorBlendCI;
        graphicsPipelineCI.pDynamicState = &dynamicStateCI;
        // Use the first subpass = draw subpass.
        graphicsPipelineCI.subpass = 0;
        // This is the first graphics pipeline -> no derivative.
        graphicsPipelineCI.basePipelineHandle = VK_NULL_HANDLE;
        graphicsPipelineCI.basePipelineIndex = -1;

        // Get information about tile vertices.
        auto tBd = mVkI::Vertex3D::getBindingDescription();
        auto tAd = mVkI::Vertex3D::getAttributeDescriptions();

        // Use terrain shader stages.
        graphicsPipelineCI.stageCount = static_cast<uint32_t>(terrainShaders.size());
        graphicsPipelineCI.pStages = terrainShaders.begin();
        // Use tile vertices.
        vertInputCI.vertexBindingDescriptionCount = 1;
        vertInputCI.pVertexBindingDescriptions = &tBd;
        vertInputCI.vertexAttributeDescriptionCount = static_cast<uint32_t>(tAd.size());
        vertInputCI.pVertexAttributeDescriptions = tAd.data();
        // Set already created Vulkan objects.
        graphicsPipelineCI.layout = mVk.pipelines.terrainLayout;
        graphicsPipelineCI.renderPass = mVk.mRendPass.renderPass;
        // Setup viewport.
        mVk.pipelines.terrainViewport.x = 0;
        mVk.pipelines.terrainViewport.y = 0;
        mVk.pipelines.terrainViewport.width = mVk.swapChain.swapChain.extent().width;
        mVk.pipelines.terrainViewport.height = mVk.swapChain.swapChain.extent().height;
        mVk.pipelines.terrainViewport.minDepth = 0.0f;
        mVk.pipelines.terrainViewport.maxDepth = 1.0f;
        // Viewport scissoring.
        vpScissor.extent = mVk.swapChain.swapChain.extent();
        // Combine viewport and scissor into viewport state.
        vpStateCI.viewportCount = 1;
        vpStateCI.pViewports = &mVk.pipelines.terrainViewport;
        vpStateCI.scissorCount = 1;
        vpStateCI.pScissors = &vpScissor;
        // Do depth testing.
        depthStencilCI.depthTestEnable = VK_TRUE;
        // Enable write access to the depth buffer.
        depthStencilCI.depthWriteEnable = VK_TRUE;
        if (vkCreateGraphicsPipelines(mVk.base.device, VK_NULL_HANDLE, 1,
                                      &graphicsPipelineCI, nullptr, &mVk.pipelines.terrain) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create main graphics pipeline!");
        }
        // Create wireframe alternative.
        rasterizationCI.polygonMode = VK_POLYGON_MODE_LINE;
        if (vkCreateGraphicsPipelines(mVk.base.device, VK_NULL_HANDLE, 1,
                                      &graphicsPipelineCI, nullptr, &mVk.pipelines.terrainWf) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create main graphics pipeline!");
        }
        // Use sky sphere shader stages.
        graphicsPipelineCI.stageCount = static_cast<uint32_t>(skySphereShaders.size());
        graphicsPipelineCI.pStages = skySphereShaders.begin();
        // Fill polygons again.
        rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
        // We should never see the sky sphere from the other side.
        rasterizationCI.cullMode = VK_CULL_MODE_NONE;
        // Tesselate triangles.
        tessCI.patchControlPoints = 3;
        if (vkCreateGraphicsPipelines(mVk.base.device, VK_NULL_HANDLE, 1,
                                      &graphicsPipelineCI, nullptr, &mVk.pipelines.skySphere) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create main graphics pipeline!");
        }

        // Implicit shader destruction.
    }

    void VulkanEngine::hmCreatePipeline()
    {
        // Get information about simple vertices.
        auto hmBd = mVkI::SimpleVertex::getBindingDescription();
        auto hmAd = mVkI::SimpleVertex::getAttributeDescriptions();
        // Vertex puller settings.
        VkPipelineVertexInputStateCreateInfo vertInputCI{};
        vertInputCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        // Use simple vertices.
        vertInputCI.vertexBindingDescriptionCount = 1;
        vertInputCI.pVertexBindingDescriptions = &hmBd;
        vertInputCI.vertexAttributeDescriptionCount = static_cast<uint32_t>(hmAd.size());
        vertInputCI.pVertexAttributeDescriptions = hmAd.data();

        // Primitive Assembly settings.
        VkPipelineInputAssemblyStateCreateInfo assemblyCI{};
        assemblyCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        // Render triangles.
        assemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        assemblyCI.primitiveRestartEnable = VK_FALSE;

        // Tessellation settings.
        VkPipelineTessellationStateCreateInfo tessCI{};
        tessCI.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
        // Tessellation primitive is a quad.
        tessCI.patchControlPoints = 4;

        // Setup viewport.
        mVk.pipelines.heightMapViewport.x = 0;
        mVk.pipelines.heightMapViewport.y = 0;
        mVk.pipelines.heightMapViewport.width = mVk.hmRendPass.ext.width;
        mVk.pipelines.heightMapViewport.height = mVk.hmRendPass.ext.height;
        mVk.pipelines.heightMapViewport.minDepth = 0.0f;
        mVk.pipelines.heightMapViewport.maxDepth = 1.0f;
        // Viewport scissoring.
        VkRect2D vpScissor{};
        vpScissor.offset = {0, 0};
        vpScissor.extent = mVk.hmRendPass.ext;
        // Combine viewport and scissor into viewport state.
        VkPipelineViewportStateCreateInfo vpStateCI{};
        vpStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        // Combine viewport and scissor into viewport state.
        vpStateCI.viewportCount = 1;
        vpStateCI.pViewports = &mVk.pipelines.heightMapViewport;
        vpStateCI.scissorCount = 1;
        vpStateCI.pScissors = &vpScissor;

        // Rasterization stage.
        VkPipelineRasterizationStateCreateInfo rasterizationCI{};
        rasterizationCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        // Do not clamp to far/near plane. Useful for shadow mapping.
        rasterizationCI.depthClampEnable = VK_FALSE;
        // Enable rasterization stage.
        rasterizationCI.rasterizerDiscardEnable = VK_FALSE;
        // Fill polygons, VK_POLYGON_MODE_LINE for wireframe.
        rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
        // Cull the back faces of polygons.
        rasterizationCI.cullMode = VK_CULL_MODE_BACK_BIT;
        // Vertices for front should be in clockwise order.
        rasterizationCI.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        // Don't do depth biasing. Useful for shadow mapping.
        rasterizationCI.depthBiasEnable = VK_FALSE;
        rasterizationCI.depthBiasConstantFactor = 0.0f;
        rasterizationCI.depthBiasClamp = 0.0f;
        rasterizationCI.depthBiasSlopeFactor = 0.0f;
        // Normal line width. Wider lines require wideLines GPU feature.
        rasterizationCI.lineWidth = 1.0f;

        // Multisampling settings.
        VkPipelineMultisampleStateCreateInfo multisampleCI{};
        multisampleCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleCI.rasterizationSamples = VK_SAMPLE_COUNT_4_BIT;
        multisampleCI.sampleShadingEnable = VK_FALSE;
        multisampleCI.minSampleShading = 1.0f;
        multisampleCI.pSampleMask = nullptr;
        multisampleCI.alphaToCoverageEnable = VK_FALSE;
        multisampleCI.alphaToOneEnable = VK_FALSE;
        // No multisampling when rendering height-map.
        multisampleCI.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        // Settings for stencil/depth buffer.
        VkPipelineDepthStencilStateCreateInfo depthStencilCI{};
        depthStencilCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        // Check < depth.
        depthStencilCI.depthCompareOp = VK_COMPARE_OP_LESS;
        // No bounds checking.
        depthStencilCI.depthBoundsTestEnable = VK_FALSE;
        // No stencil buffer.
        depthStencilCI.stencilTestEnable = VK_FALSE;
        depthStencilCI.front = {};
        depthStencilCI.back = {};
        // Depth from 0.0 (closest) to 1.0 (furthest).
        depthStencilCI.minDepthBounds = 0.0f;
        depthStencilCI.maxDepthBounds = 1.0f;
        // Don't do depth testing.
        depthStencilCI.depthTestEnable = VK_FALSE;
        // Disable write access to the depth buffer.
        depthStencilCI.depthWriteEnable = VK_FALSE;

        // Color blending settings for the main framebuffer.
        VkPipelineColorBlendAttachmentState colorBlendAtt{};
        // No color blending.
        colorBlendAtt.blendEnable = VK_FALSE;
        colorBlendAtt.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAtt.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAtt.colorBlendOp = VK_BLEND_OP_ADD;
        colorBlendAtt.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        colorBlendAtt.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        colorBlendAtt.alphaBlendOp = VK_BLEND_OP_ADD;
        colorBlendAtt.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                       VK_COLOR_COMPONENT_G_BIT |
                                       VK_COLOR_COMPONENT_B_BIT |
                                       VK_COLOR_COMPONENT_A_BIT;

        // Color blending global settings.
        VkPipelineColorBlendStateCreateInfo colorBlendCI{};
        colorBlendCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        // No logical operations required.
        colorBlendCI.logicOpEnable = VK_FALSE;
        colorBlendCI.logicOp = VK_LOGIC_OP_COPY;
        // Only one color attachment.
        colorBlendCI.attachmentCount = 1;
        colorBlendCI.pAttachments = &colorBlendAtt;
        // No blending.
        colorBlendCI.blendConstants[0] = 0.0f;
        colorBlendCI.blendConstants[1] = 0.0f;
        colorBlendCI.blendConstants[2] = 0.0f;
        colorBlendCI.blendConstants[3] = 0.0f;

        // Select attributes which can be changed dynamically.
        VkPipelineDynamicStateCreateInfo dynamicStateCI{};
        dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        static constexpr uint32_t NUM_DYNAMIC_STATES{2};
        VkDynamicState dynamicStates[NUM_DYNAMIC_STATES] = {
                // Changing resolution of the viewport.
                VK_DYNAMIC_STATE_VIEWPORT,
                // Changing line width.
                VK_DYNAMIC_STATE_LINE_WIDTH};
        dynamicStateCI.dynamicStateCount = NUM_DYNAMIC_STATES;
        dynamicStateCI.pDynamicStates = dynamicStates;

        // Pipeline layout and uniform mapping.
        auto layouts = mVk.descriptors.heightMapDesc.layouts();
        VkPipelineLayoutCreateInfo pipelineLayoutCI{};
        pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCI.pushConstantRangeCount = 0;
        pipelineLayoutCI.pPushConstantRanges = nullptr;
        // Using the height-map descriptors.
        pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(layouts.size());
        pipelineLayoutCI.pSetLayouts = layouts.data();
        if (vkCreatePipelineLayout(mVk.base.device, &pipelineLayoutCI,
                                   nullptr, &mVk.pipelines.heightMapLayout) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create height-map pipeline layout!");
        }

        // Load the height-map shaders.
        std::string heightMapShaderBase{std::string(mVkEngineConf::SHADER_FOLDER) + mVkEngineConf::HHVkPipelines::HEIGHT_MAP_SHADER_NAME};
        mVkH::HVkShader hmVs(heightMapShaderBase, VK_SHADER_STAGE_VERTEX_BIT, mVk.base.device);
        mVkH::HVkShader hmFs(heightMapShaderBase, VK_SHADER_STAGE_FRAGMENT_BIT, mVk.base.device);

        auto heightMapShaders = {
                hmVs.pipelineShaderStageCreateInfo(),
                hmFs.pipelineShaderStageCreateInfo()
        };

        // Create the final graphics pipeline.
        VkGraphicsPipelineCreateInfo graphicsPipelineCI{};
        graphicsPipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        // Set information provided by already created structures
        graphicsPipelineCI.pVertexInputState = &vertInputCI;
        graphicsPipelineCI.pInputAssemblyState = &assemblyCI;
        graphicsPipelineCI.pTessellationState = &tessCI;
        graphicsPipelineCI.pViewportState = &vpStateCI;
        graphicsPipelineCI.pRasterizationState = &rasterizationCI;
        graphicsPipelineCI.pMultisampleState = &multisampleCI;
        graphicsPipelineCI.pDepthStencilState = &depthStencilCI;
        graphicsPipelineCI.pColorBlendState = &colorBlendCI;
        graphicsPipelineCI.pDynamicState = &dynamicStateCI;
        // Use the first subpass = draw subpass.
        graphicsPipelineCI.subpass = 0;
        // This is the first graphics pipeline -> no derivative.
        graphicsPipelineCI.basePipelineHandle = VK_NULL_HANDLE;
        graphicsPipelineCI.basePipelineIndex = -1;
        // Use height-map shader stages.
        graphicsPipelineCI.stageCount = static_cast<uint32_t>(heightMapShaders.size());
        graphicsPipelineCI.pStages = heightMapShaders.begin();
        // Set already created Vulkan objects.
        graphicsPipelineCI.layout = mVk.pipelines.heightMapLayout;
        graphicsPipelineCI.renderPass = mVk.hmRendPass.renderPass;

        if (vkCreateGraphicsPipelines(mVk.base.device, VK_NULL_HANDLE, 1,
                                      &graphicsPipelineCI, nullptr, &mVk.pipelines.heightMap) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create height-map graphics pipeline!");
        }

        // Implicit shader destruction.
    }

    void VulkanEngine::createAppFramebuffers()
    {
        mVk.mRendPass.framebuffer.prepare(mVk.base.device);

        auto &views = mVk.swapChain.swapChain.views();
        for (uint32_t iii = 0; iii < views.size(); ++iii)
        {
            mVk.mRendPass.framebuffer.addFramebuffer(
                    mVk.mRendPass.renderPass,
                    mVk.swapChain.swapChain.extent(),
                    {
                            // Multisampled color buffer.
                            mVk.swapChain.msaa.color.view(),
                            // Resolved color buffer for presentation.
                            views[iii].view(),
                            // Multisampled depth buffer.
                            mVk.swapChain.msaa.depth.view(),
                            // Resolved depth buffer.
                            mVk.swapChain.depthResolve.view(),
                    }
            );
        }
    }

    void VulkanEngine::createCommandPool()
    {
        // Create command pool for the graphics queue family.
        VkCommandPoolCreateInfo commandPoolCI{};
        commandPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCI.flags = 0;
        commandPoolCI.queueFamilyIndex = static_cast<uint32_t>(mVk.queues.queueFamilies.graphicsFamily);

        if (vkCreateCommandPool(mVk.base.device, &commandPoolCI, nullptr, &mVk.base.cmdPool) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create command pool!");
        }
    }

    void VulkanEngine::createDescriptorPool()
    {
        mVk.descriptors.mainPool.requestSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2);
        mVk.descriptors.mainPool.requestSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1);

        // One for terrain and one for height-map.
        mVk.descriptors.mainPool.create(mVk.base.device, 2);
    }

    void VulkanEngine::createAppBuffers()
    {
        mVk.buffers.uniform.create(
                mVk.base.device, mVk.base.physicalDevice,
                sizeof(mVkEngineConf::HHVkDescriptors::UniformBufferObject),
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

        // Create empty buffers as a placeholder.
        mVk.buffers.vertex.create(
                mVk.base.device, mVk.base.physicalDevice, 1,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.buffers.index.create(
                mVk.base.device, mVk.base.physicalDevice, 1,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    }

    void VulkanEngine::createAppSamplers()
    {
        /*
        // Create empty image as a placeholder.
        mVk.samplers.terrainImage.create(
                mVk.base.device, mVk.base.physicalDevice, 1, 1,
                VK_FORMAT_R8G8B8A8_UNORM, VK_SAMPLE_COUNT_1_BIT,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.samplers.terrainImage.createView(
                VK_FORMAT_R8G8B8A8_UNORM,
                VK_IMAGE_ASPECT_COLOR_BIT);

        mVk.samplers.terrain.create(
                mVk.base.device,
                VK_FILTER_LINEAR, VK_FILTER_LINEAR,
                VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
                VK_TRUE,
                VK_BORDER_COLOR_INT_OPAQUE_BLACK);
                */
    }

    void VulkanEngine::createAppSync()
    {
        VkSemaphoreCreateInfo ci{};
        ci.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        if (vkCreateSemaphore(mVk.base.device, &ci, nullptr, &mVk.sync.imageAvailable) != VK_SUCCESS ||
            vkCreateSemaphore(mVk.base.device, &ci, nullptr, &mVk.sync.renderFinished) != VK_SUCCESS ||
            vkCreateSemaphore(mVk.base.device, &ci, nullptr, &mVk.sync.heightMapAvailable) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create semaphores!");
        }
    }

    void VulkanEngine::createAppDescriptorSet()
    {
        mVk.descriptors.terrainDesc.createDescriptorSet(mVk.descriptors.mainPool.pool());

        mVk.descriptors.terrainDesc.writeBufferInfo(
                mVk.buffers.uniform.buffer(),
                0, sizeof(mVkEngineConf::HHVkDescriptors::UniformBufferObject),
                0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);

        mVk.descriptors.terrainDesc.writeSamplerInfo(
                mVk.samplers.terrain.sampler(),
                mVk.samplers.terrainImage.view(),
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
    }

    void VulkanEngine::prepareHeightMap()
    {
        hmCreateRenderPass();
        hmCreateFramebuffer();
        hmCreateBuffers();
        hmCreateSampler();
        hmCreateDescriptorSet();
        hmCreatePipeline();
    }

    void VulkanEngine::hmCreateRenderPass()
    {
        // Create the render target first.
        mVk.samplers.terrainImage.create(
                mVk.base.device, mVk.base.physicalDevice,
                mVk.hmRendPass.resolution, mVk.hmRendPass.resolution,
                //VK_FORMAT_R8G8B8A8_UNORM,
                VK_FORMAT_R32G32B32A32_SFLOAT,
                VK_SAMPLE_COUNT_1_BIT,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        mVk.samplers.terrainImage.createView(VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_ASPECT_COLOR_BIT);

        // Height-map image attachment.
        VkAttachmentDescription hmImageDesc =
                mVk.samplers.terrainImage.attachmentDescription(
                        VK_ATTACHMENT_LOAD_OP_CLEAR,
                        VK_ATTACHMENT_STORE_OP_STORE,
                        VK_IMAGE_LAYOUT_UNDEFINED,
                        // After the render pass, it will be used in a sampler.
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        VkAttachmentReference hmImageRef{};
        hmImageRef.attachment = 0;
        hmImageRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        auto descriptions = {
                hmImageDesc
        };
        auto references = {
                hmImageRef
        };

        // Subpass rendering into the height-map.
        VkSubpassDescription drawSD{};
        drawSD.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        drawSD.inputAttachmentCount = 0;
        drawSD.pInputAttachments = nullptr;
        drawSD.colorAttachmentCount = static_cast<uint32_t>(references.size());
        drawSD.pColorAttachments = references.begin();
        drawSD.pResolveAttachments = nullptr;
        drawSD.pDepthStencilAttachment = nullptr;
        drawSD.preserveAttachmentCount = 0;
        drawSD.pPreserveAttachments = nullptr;

        auto subpasses = {
                drawSD
        };

        // Dependency for layout transition of the height-map image.
        VkSubpassDependency preDep{};
        // Before the first subpass.
        preDep.srcSubpass = VK_SUBPASS_EXTERNAL;
        preDep.dstSubpass = 0;
        // Wait until reading ends.
        preDep.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        preDep.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        // Other operations should wait before accessing color buffer.
        preDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        preDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        preDep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        VkSubpassDependency postDep{};
        // After the first (and last) subpass.
        postDep.srcSubpass = subpasses.size() - 1;
        postDep.dstSubpass = VK_SUBPASS_EXTERNAL;
        // Wait for color framebuffer to be filled.
        postDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        postDep.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        // Other operations should wait before accessing color buffer.
        postDep.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        postDep.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        postDep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        auto deps = {
                preDep,
                postDep
        };

        VkRenderPassCreateInfo renderPassCI{};
        renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassCI.attachmentCount = static_cast<uint32_t>(descriptions.size());
        renderPassCI.pAttachments = descriptions.begin();
        renderPassCI.subpassCount = static_cast<uint32_t>(subpasses.size());
        renderPassCI.pSubpasses = subpasses.begin();
        renderPassCI.dependencyCount = static_cast<uint32_t>(deps.size());
        renderPassCI.pDependencies = deps.begin();

        if (vkCreateRenderPass(mVk.base.device, &renderPassCI, nullptr,
                               &mVk.hmRendPass.renderPass) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create height-map render pass!");
        }
    }

    void VulkanEngine::hmCreateFramebuffer()
    {
        mVk.hmRendPass.framebuffer.prepare(mVk.base.device);
        mVk.hmRendPass.framebuffer.addFramebuffer(
                mVk.hmRendPass.renderPass,
                mVk.hmRendPass.ext,
                {
                        mVk.samplers.terrainImage.view()
                });
    }

    void VulkanEngine::hmCreateBuffers()
    {
        mVk.buffers.hmUniform.create(
                mVk.base.device, mVk.base.physicalDevice,
                sizeof(mVkEngineConf::HHVkDescriptors::HeightMapUBO),
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

        // Copy the data into local structures.
        mVk.buffers.hmVertexData = mVkI::ExampleData::vertices;

        VkDeviceSize vertSize = mVk.buffers.hmVertexData.size() * sizeof(mVkI::SimpleVertex);

        // Create a staging buffer and fill it with data.
        mVkH::HVkBuffer staging(mVk.base.device, mVk.base.physicalDevice,
                                vertSize,
                                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        {
            void *data{nullptr};
            mVkH::ScopeMemoryMapper mapper(mVk.base.device, staging.memory(),
                                           0, vertSize, 0, &data);

            memcpy(data, mVk.buffers.hmVertexData.data(), vertSize);

            // Implicit memory unmap.
        }

        mVk.buffers.hmVertex.create(
                mVk.base.device, mVk.base.physicalDevice, vertSize,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        // Create command pool for the copy command.
        mVkH::HVkCommandBuffer cmdBuffer(mVk.base.device, mVk.base.cmdPool);
        auto cmd = cmdBuffer.beginCommands();
        {
            // Copy data into the newly created buffers.
            mVk.buffers.hmVertex.copyFromBuffer(cmd, staging.buffer(), 0, 0, vertSize);
        }
        // Submit the commands.
        cmdBuffer.endCommands(mVk.queues.graphicsQueue);
    }

    void VulkanEngine::hmCreateSampler()
    {
        mVk.samplers.terrain.create(
                mVk.base.device,
                VK_FILTER_LINEAR, VK_FILTER_LINEAR,
                VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                VK_TRUE,
                VK_BORDER_COLOR_INT_OPAQUE_BLACK);
    }

    void VulkanEngine::hmCreateDescriptorSet()
    {
        mVk.descriptors.heightMapDesc.addBinding(
                0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1, VK_SHADER_STAGE_FRAGMENT_BIT,
                nullptr);
        mVk.descriptors.heightMapDesc.createSetLayout(mVk.base.device);
        mVk.descriptors.heightMapDesc.createDescriptorSet(mVk.descriptors.mainPool.pool());
        mVk.descriptors.heightMapDesc.writeBufferInfo(
                mVk.buffers.hmUniform.buffer(),
                0, sizeof(mVkEngineConf::HHVkDescriptors::HeightMapUBO),
                0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
    }

    void VulkanEngine::hmCreateCommand()
    {
        // Create the main command buffer.
        VkCommandBufferAllocateInfo commandBufferAI{};
        commandBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAI.commandPool = mVk.base.cmdPool;
        commandBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAI.commandBufferCount = 1;

        if (vkAllocateCommandBuffers(mVk.base.device, &commandBufferAI,
                                     &mVk.hmRendPass.command) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate command buffers!");
        }

        // Record the commands.
        VkCommandBufferBeginInfo commandBufferBI{};
        commandBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        commandBufferBI.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        commandBufferBI.pInheritanceInfo = nullptr;

        vkBeginCommandBuffer(mVk.hmRendPass.command, &commandBufferBI);
        {
            // Start the render pass.
            std::array<VkClearValue, 1> clearColors{};
            // Clear color for the height-map
            clearColors[0].color = {0.0f, 0.0f, 0.0f};

            VkRenderPassBeginInfo renderPassBI{};
            renderPassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBI.renderPass = mVk.hmRendPass.renderPass;
            renderPassBI.framebuffer = mVk.hmRendPass.framebuffer.framebuffers()[0];
            // Use whole frame space.
            renderPassBI.renderArea.extent = mVk.hmRendPass.ext;
            renderPassBI.renderArea.offset = {0, 0};
            // Select the clear color.
            renderPassBI.clearValueCount = static_cast<uint32_t>(clearColors.size());
            renderPassBI.pClearValues = clearColors.begin();

            // Start rendering.
            vkCmdBeginRenderPass(mVk.hmRendPass.command, &renderPassBI, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(mVk.hmRendPass.command, VK_PIPELINE_BIND_POINT_GRAPHICS, mVk.pipelines.heightMap);

            vkCmdBindDescriptorSets(mVk.hmRendPass.command, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    mVk.pipelines.heightMapLayout, 0, 1,
                                    &mVk.descriptors.heightMapDesc.descriptorSet(), 0, nullptr);

            auto buffers = {
                    mVk.buffers.hmVertex.buffer()
            };
            auto offsets = {
                    VkDeviceSize(0)
            };

            vkCmdBindVertexBuffers(mVk.hmRendPass.command, 0,
                                   static_cast<uint32_t>(buffers.size()),
                                   buffers.begin(), offsets.begin());

            vkCmdSetViewport(mVk.hmRendPass.command, 0, 1, &mVk.pipelines.heightMapViewport);

            // Draw simple vertices, since there won't be many of them.
            vkCmdDraw(mVk.hmRendPass.command, static_cast<uint32_t>(mVk.buffers.hmVertexData.size()), 1,  0, 0);

            vkCmdEndRenderPass(mVk.hmRendPass.command);
        }
        if (vkEndCommandBuffer(mVk.hmRendPass.command) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create command buffer!");
        }
    }

    void VulkanEngine::createAppCommandBuffers(VkPipeline pipeline, std::vector<VkCommandBuffer> &commands)
    {
        commands.resize(mVk.swapChain.swapChain.views().size());

        // Create the main command buffer.
        VkCommandBufferAllocateInfo commandBufferAI{};
        commandBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAI.commandPool = mVk.base.cmdPool;
        // Primary command buffer.
        commandBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        // One buffer for each framebuffer.
        commandBufferAI.commandBufferCount = static_cast<uint32_t>(mVk.mRendPass.commands.size());

        if (vkAllocateCommandBuffers(mVk.base.device, &commandBufferAI,
                                     commands.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to allocate command buffers!");
        }

        for (std::size_t iii = 0; iii < commands.size(); ++iii)
        {
            VkCommandBufferBeginInfo commandBufferBI{};
            commandBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            // Can be resubmitted to a queue when in pending state.
            commandBufferBI.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            commandBufferBI.pInheritanceInfo = nullptr;

            vkBeginCommandBuffer(commands[iii], &commandBufferBI);
            {
                // Start the render pass.
                std::array<VkClearValue, 4> clearColors{};
                // MSAA color buffer.
                clearColors[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
                // Resolved color buffer.
                clearColors[1].color = {0.0f, 0.0f, 0.0f, 1.0f};
                // MSAA depth buffer.
                clearColors[2].depthStencil = {1.0f, 0};
                // Resolved depth buffer.
                clearColors[3].depthStencil = {1.0f, 0};
                VkRenderPassBeginInfo renderPassBI{};
                renderPassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                // Use the main render pass.
                renderPassBI.renderPass = mVk.mRendPass.renderPass;
                // Choose the framebuffer corresponding to this command buffer.
                renderPassBI.framebuffer = mVk.mRendPass.framebuffer.framebuffers()[iii];
                // Use whole frame space.
                renderPassBI.renderArea.extent = mVk.swapChain.swapChain.extent();
                renderPassBI.renderArea.offset = {0, 0};
                // Select the clear color.
                renderPassBI.clearValueCount = clearColors.size();
                renderPassBI.pClearValues = clearColors.begin();

                // Begin main render pass.
                vkCmdBeginRenderPass(commands[iii], &renderPassBI, VK_SUBPASS_CONTENTS_INLINE);
                // Use the main viewport.
                vkCmdSetViewport(commands[iii], 0, 1, &mVk.pipelines.terrainViewport);

                auto buffers = {
                        mVk.buffers.vertex.buffer()
                };
                auto offsets = {
                        VkDeviceSize(0)
                };

                // Draw terrain:
                // Bind the terrain pipeline, used in graphics mode.
                vkCmdBindPipeline(commands[iii], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
                // Bind descriptor set containing uniform data.
                vkCmdBindDescriptorSets(commands[iii], VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        mVk.pipelines.terrainLayout, 0, 1,
                                        &mVk.descriptors.terrainDesc.descriptorSet(), 0, nullptr);
                // Bind the buffers required for drawing.
                vkCmdBindVertexBuffers(commands[iii], 0,
                                       static_cast<uint32_t>(buffers.size()),
                                       buffers.begin(), offsets.begin());
                // Bind the buffer containing vertex indices.
                vkCmdBindIndexBuffer(commands[iii], mVk.buffers.index.buffer(), 0, VK_INDEX_TYPE_UINT32);
                // Draw indexed vertices.
                vkCmdDrawIndexed(commands[iii], mVk.buffers.numTerrainIndices, 1, 0, 0, 0);

                // Draw sky sphere:
                // Calculate how many indices are used for sky sphere rendering.
                uint32_t numSkySphereIndices{static_cast<uint32_t>(mVk.buffers.indexData.size()) -
                                                     mVk.buffers.numTerrainIndices};
                // Bind the sky sphere pipeline, used in graphics mode.
                vkCmdBindPipeline(commands[iii], VK_PIPELINE_BIND_POINT_GRAPHICS, mVk.pipelines.skySphere);
                // Bind descriptor set containing uniform data.
                vkCmdBindDescriptorSets(commands[iii], VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        mVk.pipelines.terrainLayout, 0, 1,
                                        &mVk.descriptors.terrainDesc.descriptorSet(), 0, nullptr);
                // Bind the buffers required for drawing.
                vkCmdBindVertexBuffers(commands[iii], 0,
                                       static_cast<uint32_t>(buffers.size()),
                                       buffers.begin(), offsets.begin());
                // Bind the buffer containing vertex indices.
                vkCmdBindIndexBuffer(commands[iii], mVk.buffers.index.buffer(), 0, VK_INDEX_TYPE_UINT32);
                // Draw indexed vertices starting at the first sky sphere index.
                vkCmdDrawIndexed(commands[iii], numSkySphereIndices, 1,
                                 mVk.buffers.numTerrainIndices, 0, 0);

                // End of the render pass.
                vkCmdEndRenderPass(commands[iii]);
            }
            if (vkEndCommandBuffer(commands[iii]) != VK_SUCCESS)
            {
                throw std::runtime_error("Unable to create command buffer!");
            }
        }
    }

    void VulkanEngine::cleanupHeightMap()
    {
        vkDestroyPipeline(mVk.base.device, mVk.pipelines.heightMap, nullptr);
        vkDestroyPipelineLayout(mVk.base.device, mVk.pipelines.heightMapLayout, nullptr);
        vkFreeCommandBuffers(mVk.base.device, mVk.base.cmdPool, 1, &mVk.hmRendPass.command);
        mVk.descriptors.heightMapDesc.destroy();
        mVk.samplers.terrain.destroy();
        mVk.buffers.hmVertex.destroy();
        mVk.buffers.hmVertexData.clear();
        mVk.buffers.hmUniform.destroy();
        mVk.hmRendPass.framebuffer.destroy();
        vkDestroyRenderPass(mVk.base.device, mVk.hmRendPass.renderPass, nullptr);
        mVk.samplers.terrainImage.destroy();
    }

    void VulkanEngine::fillVertIndBuffers(const std::vector<mVkI::Vertex3D> &vertices,
                                          const std::vector<uint32_t> &indices)
    {
        // Copy the data into local structures.
        mVk.buffers.vertexData = vertices;
        mVk.buffers.indexData = indices;

        VkDeviceSize vertSize = mVk.buffers.vertexData.size() * sizeof(mVkI::Vertex3D);
        VkDeviceSize indSize = mVk.buffers.indexData.size() * sizeof(uint32_t);

        // Create a staging buffer and fill it with data.
        mVkH::HVkBuffer staging(mVk.base.device, mVk.base.physicalDevice,
                                vertSize + indSize,
                                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        {
            void *data{nullptr};
            mVkH::ScopeMemoryMapper mapper(mVk.base.device, staging.memory(),
                                           0, vertSize + indSize, 0, &data);

            memcpy(data, mVk.buffers.vertexData.data(), vertSize);
            memcpy(static_cast<uint8_t*>(data) + vertSize, mVk.buffers.indexData.data(), indSize);

            // Implicit memory unmap.
        }

        // Re-create the vertex buffer.
        mVk.buffers.vertex.destroy();
        mVk.buffers.vertex.create(
                mVk.base.device, mVk.base.physicalDevice, vertSize,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        // Re-create the index buffer.
        mVk.buffers.index.destroy();
        mVk.buffers.index.create(
                mVk.base.device, mVk.base.physicalDevice, indSize,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        // Create command pool for the copy command.
        mVkH::HVkCommandBuffer cmdBuffer(mVk.base.device, mVk.base.cmdPool);
        auto cmd = cmdBuffer.beginCommands();
        {
            // Copy data into the newly created buffers.
            mVk.buffers.vertex.copyFromBuffer(cmd, staging.buffer(), 0, 0, vertSize);
            mVk.buffers.index.copyFromBuffer(cmd, staging.buffer(), vertSize, 0, indSize);
        }
        // Submit the commands.
        cmdBuffer.endCommands(mVk.queues.graphicsQueue);
    }

    bool VulkanEngine::isDeviceSuitable(VkPhysicalDevice device)
    {
        mVkH::QueueFamilyIndices indices(device, mVk.base.surface);
        bool extensionSupported{
                mVkU::checkDeviceExtensionSupport(device, mVkEngineConf::DEVICE_EXTENSIONS)};

        bool swapChainSupported{false};
        if (extensionSupported)
        {
            mVkH::SwapChainSupportDetails scSupport(device, mVk.base.surface);
            swapChainSupported = !scSupport.formats.empty() && !scSupport.presentModes.empty();
        }

        VkPhysicalDeviceFeatures supportedFeatures{};
        vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

        return indices.isComplete() &&
               extensionSupported &&
               swapChainSupported &&
               // MSAA
               supportedFeatures.samplerAnisotropy &&
               // Tesselation shader used for LOD.
               supportedFeatures.tessellationShader &&
               // Wireframe drawing
               supportedFeatures.fillModeNonSolid;
    }

    std::vector<const char*> VulkanEngine::getRequiredExtensions(const std::vector<const char*> &extensions)
    {
        std::vector<const char*> result;

        std::copy(extensions.begin(), extensions.end(), std::back_inserter(result));

        if (mVkEngineConf::VALIDATION_LAYERS_ENABLE)
        {
            // Add the validation layer extension.
            result.emplace_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }

        return result;
    }
}

