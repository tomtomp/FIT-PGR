/**
 * @file Engine/Util.h
 * @author Tomas Polasek
 * @brief Miscellaneous utilities.
 */

#include "Engine/Util.h"

namespace Util
{
    float clamp(float val, float lo, float hi)
    {
        return (val < lo) ? lo : ((val > hi) ? hi : val);
    }

    std::vector<char> loadFileB(const std::string &filename)
    {
        // Start at the end of the file and open as binary.
        std::ifstream file(filename, std::ios::ate | std::ios::binary);

        if (!file.is_open())
        {
            throw std::runtime_error(std::string("Unable to open file: ") + filename);
        }

        std::size_t fileSize{static_cast<std::size_t>(file.tellg())};
        std::vector<char> fileContent(fileSize);

        file.seekg(0);
        file.read(fileContent.data(), fileContent.size());

        file.close();

        return fileContent;
    }
} // namespace Util
