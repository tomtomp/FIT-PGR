/**
 * @file Engine/VkHelpers.cpp
 * @author Tomas Polasek
 * @brief Helper classes for communication with Vulkan API.
 */

#include "Engine/mVkHelpers.h"

namespace mVkH
{
    QueueFamilyIndices::QueueFamilyIndices(VkPhysicalDevice device, VkSurfaceKHR surface)
    {
        fill(device, surface);
    }

    void QueueFamilyIndices::fill(VkPhysicalDevice device, VkSurfaceKHR surface)
    {
        // Get the number of queue families.
        uint32_t familiesCount{0};
        vkGetPhysicalDeviceQueueFamilyProperties(device, &familiesCount, nullptr);
        if (familiesCount == 0)
        {
            return;
        }

        // Get the actual families.
        std::vector<VkQueueFamilyProperties> families(familiesCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &familiesCount, families.data());

        // Check all the required families and remember their indices.
        for (uint32_t iii = 0; iii < families.size(); ++iii)
        {
            const auto &family = families[iii];
            if (family.queueCount > 0 && (family.queueFlags & VK_QUEUE_GRAPHICS_BIT))
            {
                graphicsFamily = iii;
            }
            VkBool32 presentSupported{VK_FALSE};
            vkGetPhysicalDeviceSurfaceSupportKHR(device, iii, surface, &presentSupported);
            if (family.queueCount > 0 && presentSupported)
            {
                presentFamily = iii;
            }
        }
    }

    SwapChainSupportDetails::SwapChainSupportDetails(VkPhysicalDevice device, VkSurfaceKHR surface)
    {
        fill(device, surface);
    }

    void SwapChainSupportDetails::fill(VkPhysicalDevice device, VkSurfaceKHR surface)
    {
        // Get surface capabilities.
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &capabilities);

        // Get number of surface formats.
        uint32_t formatCount{0};
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

        if (formatCount != 0)
        {
            // Get the actual formats.
            formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, formats.data());
        }

        // Get the number of presentation modes.
        uint32_t presentModeCount{0};
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

        if (formatCount != 0)
        {
            // Get the actual presentation modes.
            presentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, presentModes.data());
        }
    }

    ScopeMemoryMapper::ScopeMemoryMapper(VkDevice device, VkDeviceMemory memory,
                                         VkDeviceSize offset, VkDeviceSize size,
                                         VkMemoryMapFlags flags, void **data) :
        mDevice{device}, mMemory{memory}
    {
        vkMapMemory(mDevice, mMemory, offset, size, flags, data);
    }

    ScopeMemoryMapper::~ScopeMemoryMapper()
    {
        vkUnmapMemory(mDevice, mMemory);
    }
}

