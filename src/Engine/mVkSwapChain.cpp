/**
 * @file Engine/mVkSwapChain.h
 * @author Tomas Polasek
 * @brief Vulkan swap chain abstraction.
 */

#include "Engine/mVkSwapChain.h"

namespace mVkH
{
    void HVkSwapChain::create(VkDevice device, VkPhysicalDevice physDevice,
                              VkSurfaceKHR surface, uint32_t width, uint32_t height,
                              bool tripleBuffering, bool vSync)
    {
        mDevice = device;
        configure(device, physDevice, surface, width, height, tripleBuffering, vSync);
        createSwapChain(physDevice, surface);
        getImages();
        createViews();
    }

    VkResult HVkSwapChain::nextImage(VkSemaphore semaphore, VkFence fence, uint32_t &index)
    {
        auto result = vkAcquireNextImageKHR(mDevice,
                                            mSwapChain,
                                            // Disable timeout.
                                            std::numeric_limits<uint64_t>::max(),
                                            semaphore,
                                            fence,
                                            &index);
        return result;
    }

    void HVkSwapChain::destroy(bool inclSwapChain)
    {
        // Calls image view destructors.
        mSwapChainImageViews.clear();

        // No need to destroy images, they will be destroyed with the swap chain.
        mSwapChainImages.clear();

        if (inclSwapChain && mSwapChain != VK_NULL_HANDLE)
        {
            vkDestroySwapchainKHR(mDevice, mSwapChain, nullptr);

            mSwapChain = VK_NULL_HANDLE;
        }
    }

    HVkSwapChain::~HVkSwapChain()
    {
        destroy(true);
    }

    void HVkSwapChain::configure(VkDevice device, VkPhysicalDevice physDevice,
                                 VkSurfaceKHR surface,
                                 uint32_t width, uint32_t height,
                                 bool tripleBuffering, bool vSync)
    {
        mSupportDetails.fill(physDevice, surface);

        mSurfaceFormat = mVkU::chooseSwapSurfaceFormat(mSupportDetails.formats);
        mPresentMode = mVkU::chooseSwapPresentMode(mSupportDetails.presentModes, vSync);
        mExtent = mVkU::chooseSwapExtent(mSupportDetails.capabilities, width, height);

        // Choose a number of images in the swap chain. +1 for triple buffering.
        mImageCount = mSupportDetails.capabilities.minImageCount
                      + (tripleBuffering ? 1 : 0);
        //  If there is a limit.
        if (mSupportDetails.capabilities.maxImageCount > 0)
        { // Then be sure, we are within the limit.
            mImageCount = std::min(mImageCount, mSupportDetails.capabilities.maxImageCount);
        }
    }

    void HVkSwapChain::createSwapChain(VkPhysicalDevice physDevice, VkSurfaceKHR surface)
    {
        // Creation of the swap chain begins.
        VkSwapchainCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface;

        // Setup images in the swap chain.
        createInfo.minImageCount = mImageCount;
        createInfo.imageFormat = mSurfaceFormat.format;
        createInfo.imageColorSpace = mSurfaceFormat.colorSpace;
        createInfo.imageExtent = mExtent;
        // Not a stereoscopic-3D application.
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        createInfo.oldSwapchain = mSwapChain;
        // Choose image sharing method for multiple queues.
        QueueFamilyIndices indices(physDevice, surface);
        auto indexList = indices.indexList();

        if (indices.graphicsFamily != indices.presentFamily)
        { // We will get 2 different queues, which means, we draw in parallel.
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            // Which queues will share this swap chain.
            createInfo.queueFamilyIndexCount = static_cast<uint32_t>(indexList.size());
            createInfo.pQueueFamilyIndices = indexList.data();
        }
        else
        { // We will get only one queue.
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            // Since no sharing will be realized, no queues are specified.
            createInfo.queueFamilyIndexCount = 0u;
            createInfo.pQueueFamilyIndices = nullptr;
        }

        createInfo.preTransform = mSupportDetails.capabilities.currentTransform;
        // Ignore alpha channel.
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = mPresentMode;
        // Enable discarding non-visible operations.
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = mSwapChain;

        if (vkCreateSwapchainKHR(mDevice, &createInfo, nullptr, &mSwapChain) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create swap chain!");
        }

        // If we recreated swap chain, delete the old one.
        if (createInfo.oldSwapchain != VK_NULL_HANDLE)
        {
            vkDestroySwapchainKHR(mDevice, createInfo.oldSwapchain, nullptr);
        }
    }

    void HVkSwapChain::getImages()
    {
        /**
         * Get the images contained inside swap chain.
         * It is important to check the actual number of images, because the
         * number can be greater than the number of requested images.
         */
        vkGetSwapchainImagesKHR(mDevice, mSwapChain, &mImageCount, nullptr);
        mSwapChainImages.resize(mImageCount);
        vkGetSwapchainImagesKHR(mDevice, mSwapChain, &mImageCount, mSwapChainImages.data());
    }

    void HVkSwapChain::createViews()
    {
        // Make space for the image views.
        mSwapChainImageViews.resize(mSwapChainImages.size());

        for (uint32_t iii = 0; iii < mSwapChainImageViews.size(); ++iii)
        {
            mSwapChainImageViews[iii].create(mDevice, mSwapChainImages[iii],
                                             format(), VK_IMAGE_ASPECT_COLOR_BIT);
            /*
            mVkU::createImageView(mDevice, mSwapChainImages[iii], format(),
                                  VK_IMAGE_ASPECT_COLOR_BIT, mSwapChainImageViews[iii]);
                                  */
        }
    }
}

