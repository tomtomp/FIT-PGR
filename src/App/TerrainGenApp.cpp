/**
 * @file TerrainGenApp.cpp
 * @author Tomas Polasek
 * @brief Class representing the main application.
 */

#include <set>
#include "App/TerrainGenApp.h"

void TerrainGenApp::run()
{
    initWindow();
    initVulkan();

    initApp();

    mainLoop();

    cleanup();
}

void TerrainGenApp::windowResizeCb(GLFWwindow *window, int width, int height)
{
    if (width == 0 || height == 0)
    {
        return;
    }

    auto app = reinterpret_cast<TerrainGenApp*>(glfwGetWindowUserPointer(window));
    app->resizeWindow(width, height);
}

void TerrainGenApp::surfaceCreateCb(void *cData, VkInstance instance,
                                    VkAllocationCallbacks *cb, VkSurfaceKHR *surface)
{
    auto cThis = reinterpret_cast<TerrainGenApp*>(cData);
    glfwCreateWindowSurface(instance, cThis->mWindow, cb, surface);
}

void TerrainGenApp::initWindow()
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    mWindow = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, nullptr, nullptr);

    glfwSetWindowUserPointer(mWindow, this);
    glfwSetWindowSizeCallback(mWindow, windowResizeCb);
}

void TerrainGenApp::initVulkan()
{
    int width{0};
    int height{0};
    glfwGetWindowSize(mWindow, &width, &height);
    mWidth = static_cast<uint32_t>(width);
    mHeight = static_cast<uint32_t>(height);

    mEngine.initialize(TerrainGenApp::surfaceCreateCb, this,
                       mWidth, mHeight, getRequiredGlfwExtensions());

    mTerrainTiles.generate(TILE_LEVELS);
    mSkySphere.generate(mTerrainTiles.vertices().size());

    auto vertices = mTerrainTiles.vertices();
    std::copy(mSkySphere.vertices().begin(),
              mSkySphere.vertices().end(),
              std::back_inserter(vertices));
    auto indices = mTerrainTiles.indices();
    std::copy(mSkySphere.indices().begin(),
              mSkySphere.indices().end(),
              std::back_inserter(indices));

    mState.hmUniforms.size = mTerrainTiles.mapSize();

    mEngine.prepareTerrain(vertices,
                           indices,
                           mState.hmUniforms.size * HEIGHT_MAP_RES_FACTOR,
                           mTerrainTiles.indices().size());

    mEngine.finalize();
}

void TerrainGenApp::initApp()
{
    mCamera.setProjection(60.0f, static_cast<float>(mWidth) / mHeight, 0.1f, 100.0f);
    mCamera.setPos({0.0f, 0.0f, 0.0f});
    mCamera.setRot({0.0f, M_PI, 0.0f});
    mCamera.recalculate();

    // Select the control elements.
    mGamepad.select();
    mGamepad.setCallback(mWindow);
    mGamepad.setJoystickDeadzone(0.0f);
    mKeyboard.select();
    mKeyboard.setCallback(mWindow);
    mMouse.select();
    mMouse.setCallback(mWindow);

    mState.uniforms.mvp = glm::mat4(1.0f);
    mState.uniforms.tessFactor = 32.0f;
    mState.uniforms.heightFactor = HEIGHT_MAP_HEIGHT_FACTOR_BASE;
    mState.uniforms.clearColor = SKY_COLOR;
    mState.uniforms.fogDist = mTerrainTiles.mapSize() / 2.0f;
    mState.hmUniforms.position = glm::vec2(0.0f, 0.0f);
    mState.hmUniforms.time = 0.0f;
    std::srand(std::chrono::system_clock::now().time_since_epoch().count());
    mState.hmUniforms.seed = std::rand();
    mState.hmUniforms.frequency = HEIGHT_MAP_FREQUENCY_BASE;
    mState.hmUniforms.heightFactor = HEIGHT_MAP_HEIGHT_FACTOR_BASE;

    mState.cameraMousePos.x = 1.0f;
    mState.worldPos = glm::vec3{0.0f, 10.0f, 0.0f};
}

void TerrainGenApp::mainLoop()
{
    // Mouse camera control.
    mMouse.setMousePosAction([&] (double posX, double posY) {
        if (mState.cameraEnabled)
        {
            mState.cameraMousePos.x -= (mState.mousePosX - posX) / mWidth;
            mState.cameraMousePos.y -= (mState.mousePosY - posY) / mHeight;
        }
        mState.mousePosX = posX;
        mState.mousePosY = posY;
    });

    // Ctrl + Q to end the appplication.
    bool shouldEnd{false};
    mKeyboard.setAction(GLFW_KEY_Q, GLFW_MOD_CONTROL, GLFW_PRESS, [&] () {
        shouldEnd = true;
    });

    // P to toggle wireframe display.
    mKeyboard.setAction(GLFW_KEY_P, 0, GLFW_PRESS, [&] () {
        mState.displayWf = !mState.displayWf;
    });

    mKeyboard.setAction(GLFW_KEY_W, 0, GLFW_PRESS, [&] () {
        mState.posDelta.z = -1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_W, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.z == -1.0f)
        {
            mState.posDelta.z = 0.0f;
        }
    });
    mKeyboard.setAction(GLFW_KEY_S, 0, GLFW_PRESS, [&] () {
        mState.posDelta.z = 1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_S, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.z == 1.0f)
        {
            mState.posDelta.z = 0.0f;
        }
    });
    mKeyboard.setAction(GLFW_KEY_A, 0, GLFW_PRESS, [&] () {
        mState.posDelta.x = -1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_A, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.x == -1.0f)
        {
            mState.posDelta.x = 0.0f;
        }
    });
    mKeyboard.setAction(GLFW_KEY_D, 0, GLFW_PRESS, [&] () {
        mState.posDelta.x = 1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_D, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.x == 1.0f)
        {
            mState.posDelta.x = 0.0f;
        }
    });
    mKeyboard.setAction(GLFW_KEY_R, 0, GLFW_PRESS, [&] () {
        mState.posDelta.y = 1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_R, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.y == 1.0f)
        {
            mState.posDelta.y = 0.0f;
        }
    });
    mKeyboard.setAction(GLFW_KEY_F, 0, GLFW_PRESS, [&] () {
        mState.posDelta.y = -1.0f;
    });
    mKeyboard.setAction(GLFW_KEY_F, 0, GLFW_RELEASE, [&] () {
        if (mState.posDelta.y == -1.0f)
        {
            mState.posDelta.y = 0.0f;
        }
    });

    // Escape to release mouse.
    mKeyboard.setAction(GLFW_KEY_ESCAPE, 0, GLFW_PRESS, [&] () {
        mState.cameraEnabled = false;
        glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    });

    // Click to recapture again.
    mMouse.setAction(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_PRESS, [&] (double x, double y) {
        mState.cameraEnabled = true;
        glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    });

    // Scroll wheel counter.
    mMouse.setScrollAction([&] (double, double scrollDelta, double, double) {
        mState.scrollDelta += scrollDelta;
    });

    // Gamepad control scheme.
    mGamepad.setJoystickAction(DS4Mapping::Joystick::LX, [&] (uint16_t, double px) {
        if (std::abs(px) > JOYSTICK_DEADZONE)
        {
            mState.cameraMousePos.x += px * JOYSTICK_SENSITIVITY_FACTOR;
        }
    });
    mGamepad.setJoystickAction(DS4Mapping::Joystick::LY, [&] (uint16_t, double py) {
        if (std::abs(py) > JOYSTICK_DEADZONE)
        {
            mState.cameraMousePos.y += py * JOYSTICK_SENSITIVITY_FACTOR;
        }
    });
    mGamepad.setJoystickAction(DS4Mapping::Joystick::RX, [&] (uint16_t, double px) {
        if (std::abs(px) > JOYSTICK_DEADZONE)
        {
            mState.posDelta.x = px;
        }
        else if (std::abs(mState.posDelta.x) < 1.0f)
        {
            mState.posDelta.x = 0.0f;
        }
    });
    mGamepad.setJoystickAction(DS4Mapping::Joystick::RY, [&] (uint16_t, double py) {
        if (std::abs(py) > JOYSTICK_DEADZONE)
        {
            mState.posDelta.z = py;
        }
        else if (std::abs(mState.posDelta.z) < 1.0f)
        {
            mState.posDelta.z = 0.0f;
        }
    });
    mGamepad.setAction(DS4Mapping::Button::LB, GLFW_PRESS, [&] (uint16_t) {
        mState.posDelta.y = 1.0f;
    });
    mGamepad.setAction(DS4Mapping::Button::LB, GLFW_RELEASE, [&] (uint16_t) {
        if (mState.posDelta.y == 1.0f)
        {
            mState.posDelta.y = 0.0f;
        }
    });
    mGamepad.setAction(DS4Mapping::Button::RB, GLFW_PRESS, [&] (uint16_t) {
        mState.posDelta.y = -1.0f;
    });
    mGamepad.setAction(DS4Mapping::Button::RB, GLFW_RELEASE, [&] (uint16_t) {
        if (mState.posDelta.y == -1.0f)
        {
            mState.posDelta.y = 0.0f;
        }
    });
    mGamepad.setAction(DS4Mapping::Button::TRIANGLE, GLFW_PRESS, [&] (uint16_t) {
        mState.displayWf = !mState.displayWf;
    });

    // By how much is the state of the lagging behind, in milliseconds.
    double updateLag{0.0};
    double drawLag{0.0};
    // Counters used for debug printing.
    uint64_t frameCounter{0};
    uint64_t updateCounter{0};

    // Timer used for state updating and rendering.
    Timer updateTimer;
    // Timer used for debug printing.
    Timer printTimer;

    while (!glfwWindowShouldClose(mWindow) && !shouldEnd)
    {
        // Calculate by how much the lag should increase.
        if (updateTimer.elapsed<Timer::milliseconds>() > 1.0)
        {
            double lag = updateTimer.elapsedReset<Timer::microseconds>() / Timer::US_IN_MS;
            updateLag += lag;
            drawLag += lag;
        }

        // Test if we should print information.
        uint64_t sincePrint{printTimer.elapsed<Timer::milliseconds>()};
        if (sincePrint > MS_PER_INFO)
        {
            std::cout << "FrameTime[ms] : "
                      << static_cast<double>(sincePrint) / frameCounter
                      << "\nFrames : " << frameCounter
                      << "; Updates : " << updateCounter
                      << std::endl;
            if (drawLag > 2 * MS_PER_FRAME)
            {
                std::cout << "Frame overload: " << drawLag / MS_PER_FRAME << std::endl;
            }

            frameCounter = 0;
            updateCounter = 0;

            printTimer.reset();
        }

        // TODO - Move to updateState()?
        glfwPollEvents();

        while (updateLag >= MS_PER_UPDATE)
        {
            updateState();

            updateCounter++;
            updateLag -= MS_PER_UPDATE;
        }

        if (drawLag >= MS_PER_FRAME)
        {

            mEngine.submitDrawCommands(mState.displayWf);

            frameCounter++;
            drawLag -= MS_PER_FRAME;
        }
    }

    std::cout << "Waiting until device finishes all requested operations, before quitting..." << std::endl;
    // Wait until all of the operations are finished before ending.
    mEngine.deviceWaitIdle();
}

void TerrainGenApp::cleanup()
{
    mEngine.cleanup();

    glfwDestroyWindow(mWindow);
    glfwTerminate();
}

void TerrainGenApp::resizeWindow(uint32_t width, uint32_t height)
{
    mWidth = width;
    mHeight = height;
    mCamera.setProjection(60.0f, static_cast<float>(mWidth) / mHeight, 0.1f, 100.0f);
    mCamera.recalculate();
    mEngine.resize(width, height);
}

void TerrainGenApp::updateState()
{
    mGamepad.pollEvents();

    glm::mat4 model{1.0f};
    model = glm::scale(model, {0.5f, 0.5f, 0.5f});

    mState.cameraMousePos.y = Util::clamp(mState.cameraMousePos.y, -0.5f, 0.5f);

    float pitch = mState.cameraMousePos.y * M_PI;
    float yaw = mState.cameraMousePos.x * M_PI;
    yaw = std::fmod(yaw, 2.0f * M_PI);
    mCamera.setYawPitchRoll(yaw, pitch, 0.0f);
    /*
    model = glm::mat4_cast(
            glm::quat({-mState.mousePosY / mHeight * M_PI,
                       mState.mousePosX / mWidth * M_PI,
                       0.0f}));
                       */

    glm::vec2 movDelta{mState.posDelta.x, mState.posDelta.z};
    glm::vec2 dirDelta = glm::rotate(movDelta, yaw);
    mState.worldPos.x += dirDelta.x;
    mState.worldPos.y += mState.posDelta.y;
    mState.worldPos.z += dirDelta.y;
    mCamera.setPos({0.0f, -mState.worldPos.y, 0.0f});

    double time = glfwGetTime();

    mCamera.recalculate();
    mState.uniforms.mvp = mCamera.viewProjectionMatrix() * model;
    mState.uniforms.m = model;
    mState.uniforms.v = mCamera.viewMatrix();
    mState.uniforms.p = mCamera.projectionMatrix();
    mState.uniforms.time = static_cast<float>(time);
    mState.uniforms.cameraPos = glm::vec3(0.0f, -mState.worldPos.y, 0.0f);
    mState.uniforms.worldPos = glm::vec2{mState.worldPos.x, mState.worldPos.z};
    /*
    mState.uniforms.tessFactor = std::max(
            static_cast<float>(mState.scrollDelta),
            1.0f);
            */

    //mState.hmUniforms.seed = static_cast<float>(mState.scrollDelta);
    mState.hmUniforms.time = static_cast<float>(time);
    mState.hmUniforms.position = mState.uniforms.worldPos;

    mEngine.updateUBO(mState.uniforms, mState.hmUniforms);
}

std::vector<const char*> TerrainGenApp::getRequiredGlfwExtensions()
{
    std::vector<const char*> result;

    // Get the extensions required by GLFW.
    uint32_t glfwExtensionCount{0};
    const char **glfwExtensions{nullptr};
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    // Copy GLFW extensions to the list.
    std::copy(glfwExtensions, glfwExtensions + glfwExtensionCount, std::back_inserter(result));

    return result;
}
