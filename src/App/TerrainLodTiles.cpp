/**
 * @file TerrainLodTiles.cpp
 * @author Tomas Polasek
 * @brief Generator for terrain tiles.
 */

#include "App/TerrainLodTiles.h"

TerrainLodTiles::TerrainLodTiles(uint32_t levels)
{
    generate(levels);
}

void TerrainLodTiles::generate(uint32_t levels)
{
    if (levels < 1)
    {
        throw std::runtime_error("Unable to generate terrain with 0 levels!");
    }
    generateTiles(levels);
}

void TerrainLodTiles::generateTiles(uint32_t levels)
{
    // Map is rectangular with size 2^(levels + 1).
    mMapWidth = 1u << (levels + 1u);

    generateBaseGrid();
    for (uint32_t iii = 0; iii < levels; ++iii)
    {
        // Disable tesselation factor for first tier of tiles.
        generateRing(1u << iii, iii != 0);
    }
}

void TerrainLodTiles::generateBaseGrid()
{
    mVkI::Vertex3D vert{};

    // First
    vert.pos = { 0.0f, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { 0.0f, TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { TILE_START_SIZE, TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { TILE_START_SIZE, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    // Second
    vert.pos = { 0.0f, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { -TILE_START_SIZE, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { -TILE_START_SIZE, TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { 0.0f, TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    // Third
    vert.pos = { 0.0f, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { 0.0f, -TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { -TILE_START_SIZE, -TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { -TILE_START_SIZE, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    // Fourth
    vert.pos = { 0.0f, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { TILE_START_SIZE, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { TILE_START_SIZE, -TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);

    vert.pos = { 0.0f, -TILE_START_SIZE, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = BASE_TESS_FACTOR;
    addVertex(vert);
}

void TerrainLodTiles::generateRing(float radius, bool adjustTessFactor)
{
    mVkI::Vertex3D vert{};
    const float edgeTessFactor{ adjustTessFactor ? EDGE_TESS_FACTOR : BASE_TESS_FACTOR};
    const float baseTessFactor{BASE_TESS_FACTOR};

    // Right side.
    vert.pos = { radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    vert.pos = { radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Left side.
    vert.pos = { -radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    vert.pos = { -radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, 0.0f, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Top side.
    vert.pos = { 0.0f, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { -radius, 2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, 2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    vert.pos = { radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, 2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { radius, 2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Bottom side.
    vert.pos = { -radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, -2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -radius, -2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    vert.pos = { 0.0f, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = edgeTessFactor;
    addVertex(vert);
    vert.pos = { radius, -2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 0.0f, -2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Corners
    // Bottom left
    vert.pos = { -radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, 2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -radius, 2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Bottom right
    vert.pos = { radius, radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { radius, 2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, 2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Top right
    vert.pos = { radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { 2 * radius, -2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { radius, -2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);

    // Top left
    vert.pos = { -radius, -radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -radius, -2 * radius, 0.0f}; 
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, -2 * radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
    vert.pos = { -2 * radius, -radius, 0.0f};
    vert.uv = calcTexCoord(vert.pos);
    vert.tesFactor = baseTessFactor;
    addVertex(vert);
}

glm::vec2 TerrainLodTiles::calcTexCoord(const glm::vec2 &pos)
{
    return {
            pos.x / mMapWidth + 0.5,
            pos.y / mMapWidth + 0.5,
   };
}

void TerrainLodTiles::addVertex(const mVkI::Vertex3D &vert)
{
    auto findIt = mIndexMap.find(vert);
    if (findIt != mIndexMap.end())
    { // Vertex is already registered.
        mIndices.push_back(findIt->second);
    }
    else
    { // Vertex is not yet added.
        auto newIndex = static_cast<uint16_t>(mVertices.size());
        mIndexMap[vert] = newIndex;
        mIndices.push_back(newIndex);
        mVertices.push_back(vert);
    }
}
