/**
 * @file App/SkySphere.h
 * @author Tomas Polasek
 * @brief Generator for the sky sphere.
 */

#include "App/SkySphere.h"

void SkySphere::generate(uint32_t indexStart)
{
    // Generation coordinates found on http://prideout.net/blog/?p=48 .
    static constexpr float A = 0.276f;
    static constexpr float B = 0.447f;
    static constexpr float C = 0.526f;
    static constexpr float D = 0.724f;
    static constexpr float E = 0.851f;
    static constexpr float N = 0.0f;
    static constexpr float O = 1.0f;

    mVertices = {
            mVkI::Vertex3D({ N,  N,  O}),
            mVkI::Vertex3D({ E,  N,  B}),
            mVkI::Vertex3D({ A,  E,  B}),
            mVkI::Vertex3D({-D,  C,  B}),
            mVkI::Vertex3D({-D, -C,  B}),
            mVkI::Vertex3D({ A, -E,  B}),
            mVkI::Vertex3D({ D,  C, -B}),
            mVkI::Vertex3D({-A,  E, -B}),
            mVkI::Vertex3D({-D,  N, -B}),
            mVkI::Vertex3D({-A, -E, -B}),
            mVkI::Vertex3D({ D, -C, -B}),
            mVkI::Vertex3D({ N,  N, -O})
    };

    mIndices = {
            2, 1, 0,
            3, 2, 0,
            4, 3, 0,
            5, 4, 0,
            1, 5, 0,
            11, 6, 7,
            11, 7, 8,
            11, 8, 9,
            11, 9, 10,
            11, 10, 6,
            1, 2, 6,
            2, 3, 7,
            3, 4, 8,
            4, 5, 9,
            5, 1, 10,
            2, 7, 6,
            3, 8, 7,
            4, 9, 8,
            5, 10, 9,
            1, 6, 10
    };

    for (auto &ind : mIndices)
    {
        ind += indexStart;
    }
}
