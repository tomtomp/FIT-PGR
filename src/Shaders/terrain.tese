#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Tesselation evaluation shader for rendering procedural terrain.
 */

// Options: equal_spacing, fractional_even_spacing and fractional_odd_spacing
layout(quads, equal_spacing, cw) in;

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

/// Sampler used for terrain mesh deformation.
layout(binding = 1) uniform sampler2D terrainSampler;

/// Texture coordinates passed from higher stages.
layout(location = 0) in vec2 inUV[];
/// Used only for debugging, passed through to the FS.
layout(location = 1) in float inTessFactor[];

out gl_PerVertex
{
    vec4 gl_Position;
};

/// Normal of the deformed terrain mesh.
layout(location = 0) out vec3 fragNormal;
/// Vector from the camera to given vertex.
layout(location = 1) out vec3 fragCamDist;
/// Debugging.
layout(location = 2) out vec2 fragTexCoords;
/// Debugging - Passed through.
layout(location = 3) out float fragTessFactor;
/// Position of the vertex in virtual world.
layout(location = 4) out vec2 fragWorldPos;

/**
 * Bilinear interpolation of attributes with tesselation factor.
 * Points are oriented in following way:
 * p3---p2
 * |    |
 * p0---p1
 */
float tessBlerp(float p0, float p1, float p2, float p3)
{
    return mix(mix(p0, p1, gl_TessCoord.x),
               mix(p3, p2, gl_TessCoord.x), gl_TessCoord.y);
}

/**
 * Bilinear interpolation of attributes with tesselation factor.
 * Points are oriented in following way:
 * p3---p2
 * |    |
 * p0---p1
 */
vec2 tessBlerp(vec2 p0, vec2 p1, vec2 p2, vec2 p3)
{
    return mix(mix(p0, p1, gl_TessCoord.x),
               mix(p3, p2, gl_TessCoord.x), gl_TessCoord.y);
}

/**
 * Bilinear interpolation of attributes with tesselation factor.
 * Points are oriented in following way:
 * p3---p2
 * |    |
 * p0---p1
 */
vec4 tessBlerp(vec4 p0, vec4 p1, vec4 p2, vec4 p3)
{
    return mix(mix(p0, p1, gl_TessCoord.x),
               mix(p3, p2, gl_TessCoord.x), gl_TessCoord.y);
}

void main()
{
    // Calculate uv coordinates using interpolation through tessellated triangles.
    vec2 hmUV = tessBlerp(inUV[0], inUV[1], inUV[2], inUV[3]);
    // Get sample from the height-map.
    vec4 hmSample = texture(terrainSampler, hmUV);
    // Calculate position of current vertex on the mesh.
    vec4 vMeshPos = tessBlerp(gl_in[0].gl_Position, gl_in[1].gl_Position,
                              gl_in[2].gl_Position, gl_in[3].gl_Position);

    vMeshPos.y += hmSample.a;

    // Transform the deformed vertex.
    gl_Position = ubo.mvp * vMeshPos;

    // Normal is contained in the height-map.
    fragNormal = hmSample.rgb;

    // Calculate the world position.
    fragWorldPos = hmUV + ubo.worldPos / 100.0f;
    // Calculate distance to the camera.
    fragCamDist = vMeshPos.xyz + ubo.cameraPos;

    // Pass to fragment shader for debugging purposes.
    fragTexCoords = hmUV;
    fragTessFactor = tessBlerp(inTessFactor[0], inTessFactor[1],
                               inTessFactor[3], inTessFactor[2]);
}
