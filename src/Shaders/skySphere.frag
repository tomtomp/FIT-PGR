#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Fragment shader for rendering the sky sphere.
 */

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec3 inNormal;

layout(location = 0) out vec4 outColor;

/*
 * Notice: All of the information about noise have been
 * found on https://www.scratchapixel.com .
 */

/// Magical random generator.
float random(in vec2 st)
{
    return fract(sin(dot(st.xy, vec2(17.17237, 113.194723))) * 192838.1317);
}

/// Simple value-noise.
float noise(in vec2 st)
{
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Corners of the interpolation square.
    float lt = random(i);
    float rt = random(i + vec2(1.0f, 0.0f));
    float lb = random(i + vec2(0.0f, 1.0f));
    float rb = random(i + vec2(1.0f, 1.0f));

    // Smoothstep the interpolation factor.
    vec2 u = f * f * (3.0 - 2.0 * f);

    // Bilinear interpolation.
    float result = mix(
        mix(lt, rt, u.x),
        mix(lb, rb, u.x),
        u.y
    );

    return result;
}

/// Number of noise layers.
#define NOISE_LAYERS 7

/// Fractional brownian motion.
float fbm(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += noise(st) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Turbulence noise.
float turb(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += abs(2.0f * noise(st) - 1.0f) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Ridge noise.
float ridge(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        float n = amplitude - abs(2.0f * noise(st) - 1.0f) * amplitude;
        n = n * n;
        result += n;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

void main()
{
    vec3 sunDir = -vec3(0.0f, cos(ubo.time / 7.3f), sin(ubo.time / 7.3f));
    vec2 pos = inUV + ubo.time / 17.7f;
    float sunTurb = turb(pos);
    vec3 sunColor = mix(vec3(0.7f, 0.7f, 0.1f), vec3(0.9f, 0.7f, 0.1f), sin(sunTurb));

    //float c = fbm((inTexCoord + vec2(sin(ubo.time), cos(ubo.time))) * 13.0f);
    //float c = fbm(inTexCoord * 13.0f) + turb(inTexCoord * 7.0f) + ridge(inTexCoord * 3.0f);
    //pos = inUV * pow(length(inUV), sunTurb * 13.0f) + ubo.time / 73.0f;
    vec2 wrapPos1 = vec2(fbm(pos), fbm(pos + 1.0f));
    vec2 wrapPos2 = vec2(fbm(pos + wrapPos1 * ubo.time / 13.3f), fbm(pos + wrapPos1 * ubo.time / 17.7f));
    //float c = fbm(pos + fbm(pos + fbm(pos)));
    float c = clamp(fbm(pos) - fbm(wrapPos2) * 0.37f, 0.0f, 1.0f);
    //float c = clamp(fbm(pos) - fbm(wrapPos1) * 0.5f + fbm(wrapPos2) * 0.25f, 0.0f, 1.0f);
    // Colors taken from image of a sky.
    //vec3 col = mix(vec3(0.82f, 0.87f, 0.90f), ubo.clearColor, smoothstep(0.0f, 0.9f, c));
    vec3 col = mix(vec3(0.82f, 0.87f, 0.90f), ubo.clearColor, c);
    // The Sun.
    //col = mix(col, sunColor, smoothstep(0.5f, 1.0f, smoothstep(0.95f, 1.0f, clamp(dot(sunDir, inNormal), 0.0f, 1.0f))));
    col += 0.7f * sunColor * pow(clamp(dot(sunDir, inNormal), 0.0f, 1.0f), 139.0f);
    col += 0.7f * sunColor * pow(clamp(dot(sunDir, inNormal), 0.0f, 1.0f), 173.0f);
    // Horizon clear color.
    //col = mix(col, ubo.clearColor, smoothstep(0.0f, 0.3f, clamp(dot(inNormal, vec3(0.0f, 1.0f, 0.0f)) + 0.2f, 0.0f, 1.1f)));
    col = mix(col, ubo.clearColor, clamp(smoothstep(0.0f, 11.0f, length(inUV)), 0.0f, 1.0f));

    outColor = vec4(col, 1.0f);
}

