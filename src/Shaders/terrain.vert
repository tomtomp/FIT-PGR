#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Vertex shader for rendering procedural terrain.
 */

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 3) in float inTessFactor;

layout(location = 0) out vec2 tcTexCoord;
layout(location = 1) out float tcTessFactor;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    gl_Position = vec4(inPosition.x, 0.0, inPosition.y, 1.0);
    tcTexCoord = inTexCoord;
    tcTessFactor = inTessFactor;
}

