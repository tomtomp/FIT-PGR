#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Tessellation control shader for rendering procedural terrain.
 */

// Generate "quad" patches.
layout(vertices = 4) out;

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

layout(location = 0) in vec2 inTexCoords[];
layout(location = 1) in float inTessFactor[];

layout(location = 0) out vec2 teTexCoords[4];
layout(location = 1) out float teTessFactor[4];

void main()
{
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    teTexCoords[gl_InvocationID] = inTexCoords[gl_InvocationID];
    teTessFactor[gl_InvocationID] = inTessFactor[gl_InvocationID];

    if (gl_InvocationID == 0)
    { // Once per patch.
        // Set the tessellation factors for quad patch.
        // ID 1 and 2
        float o0 = min(inTessFactor[1], inTessFactor[2]) * ubo.tessFactor;
        gl_TessLevelOuter[0] = o0;
        // ID 0 and 1
        float o1 = min(inTessFactor[0], inTessFactor[1]) * ubo.tessFactor;
        gl_TessLevelOuter[1] = o1;
        // ID 3 and 0
        float o2 = min(inTessFactor[3], inTessFactor[0]) * ubo.tessFactor;
        gl_TessLevelOuter[2] = o2;
        // ID 2 and 3
        float o3 = min(inTessFactor[2], inTessFactor[3]) * ubo.tessFactor;
        gl_TessLevelOuter[3] = o3;
        // Outer[1] and Outer[2]
        //gl_TessLevelInner[0] = ubo.tessFactor;
        gl_TessLevelInner[0] = o1 + o2;
        // Outer[0] and Outer[3]
        //gl_TessLevelInner[1] = ubo.tessFactor;
        gl_TessLevelInner[1] = o0 + o3;
    }
}