#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Tesselation evaluation shader for rendering the sky sphere.
 */

// Options: equal_spacing, fractional_even_spacing and fractional_odd_spacing
layout(triangles, equal_spacing, cw) in;

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

out gl_PerVertex
{
    vec4 gl_Position;
};

/// Texture coordinates for fragment shader.
layout(location = 0) out vec2 fragTexCoords;
/// Normal for the fragment shader.
noperspective layout(location = 1) out vec3 fragNormal;

void main()
{
    // Barycentric linear interpolation.
    vec3 vPos = gl_TessCoord.x * gl_in[0].gl_Position.xyz +
                gl_TessCoord.y * gl_in[1].gl_Position.xyz +
                gl_TessCoord.z * gl_in[2].gl_Position.xyz;

    // Sphere-ify the icosahedron.
    vPos = normalize(vPos);

    // Transform the vertex.
    vec4 vert = ubo.v * vec4(vPos * 97.0f, 0.0f);
    gl_Position = ubo.p * vec4(vert.xyz, 1.0f);

    // Spherical coordinates: https://en.wikipedia.org/wiki/Spherical_coordinate_system
    //fragTexCoords = vec2(acos(vPos.x), atan(vPos.z, max(0.01f, vPos.y)));
    // Prevent division by zero.
    float divY = max(0.001f, vPos.y);
    // Calculate sphere projection on a plane.
    fragTexCoords = vec2(vPos.x / divY, vPos.z / divY);
    // Normal is pointing inside the sphere.
    fragNormal = -vPos;
}
