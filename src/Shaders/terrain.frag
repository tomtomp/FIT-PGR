#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Fragment shader for rendering procedural terrain.
 */

layout(binding = 1) uniform sampler2D terrainSampler;

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

layout(location = 0) in vec3 inNormal;
layout(location = 1) in vec3 inCamDist;
layout(location = 2) in vec2 inUV;
layout(location = 3) in float inTessFactor;
layout(location = 4) in vec2 inWorldPos;

layout(location = 0) out vec4 outColor;

/// Magical random generator.
float random(in vec2 st)
{
    return fract(sin(dot(st.xy, vec2(37.17237, 313.194723))) * 192838.1317);
}

/// Simple value-noise.
float noise(in vec2 st)
{
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Corners of the interpolation square.
    float lt = random(i);
    float rt = random(i + vec2(1.0f, 0.0f));
    float lb = random(i + vec2(0.0f, 1.0f));
    float rb = random(i + vec2(1.0f, 1.0f));

    // Smoothstep the interpolation factor.
    vec2 u = f * f * (3.0 - 2.0 * f);

    // Bilinear interpolation.
    float result = mix(
        mix(lt, rt, u.x),
        mix(lb, rb, u.x),
        u.y
    );

    return result;
}

/// Number of noise layers.
#define NOISE_LAYERS 7

/// Fractional brownian motion.
float fbm(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += noise(st) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Turbulence noise.
float turb(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += abs(2.0f * noise(st) - 1.0f) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Ridge noise.
float ridge(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        float n = amplitude - abs(2.0f * noise(st) - 1.0f) * amplitude;
        n = n * n;
        result += n;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

void main()
{
    vec4 heightMap = texture(terrainSampler, inUV);
    vec3 c = vec3(abs(heightMap.rgb));
    if (heightMap.a < 0.0f)
    {
        // Bottom layer is sand.
        c = vec3(0.851, 0.753, 0.596) + turb(inWorldPos * 117.0f) / 23.7f;
        float underWater = -heightMap.a * length(inCamDist) / abs(inCamDist.y) / ubo.heightFactor;
        c = mix(c, vec3(0.433, 0.655, 0.945), clamp(underWater * 11.0f, 0.0f, 1.0f));
        /*
        vec2 waterSurface = vec2(
            inWorldPos.x + (inCamDist.x * -heightMap.a / abs(inCamDist.y)),
            inWorldPos.y + (inCamDist.z * -heightMap.a / abs(inCamDist.y))
        );
        //float wave = (cos(inWorldPos.x + (inCamDist.x * -heightMap.a / abs(inCamDist.y)) + ubo.time) + 1.0f) / 7.0f;
        float wave = (cos(waterSurface.x + ubo.time) + sin(waterSurface.y + ubo.time)) / 7.0f;
        c = mix(c, vec3(0.118, 0.208, 0.239), wave);
        */
    }
    else
    {
        // Height.
        float h = heightMap.a / ubo.heightFactor;
        // Bottom layer is sand.
        c = vec3(0.851, 0.753, 0.596) + turb(inWorldPos * 117.0f) / 23.7f;
        // Next is grass.
        c = mix(c, vec3(0.439, 0.588, 0.2) - ridge(inWorldPos * 113.0f) / 57.7f - turb(inWorldPos * 67.0f) / 17.0f, smoothstep(0.05f, 0.65f, h));
        // Next is dirt.
        c = mix(c, vec3(0.353, 0.157, 0.129) + fbm(inWorldPos * 67.0f) / 73.0f, smoothstep(0.70f, 0.80f, h));
        // Next is rock.
        c = mix(c, vec3(0.62, 0.612, 0.616) + sin(length(inWorldPos) + ridge(inWorldPos * 163.7f)) / 7.0f, smoothstep(0.80f, 0.83f, h));
        // Finally snow.
        c = mix(c, mix(vec3(0.953, 0.933, 0.949), vec3(0.102, 0.341, 0.761), ridge(inWorldPos * 13.7f) / 13.7f), smoothstep(0.85f, 1.0f, h));
    }
    float fogFactor = clamp(fbm(inUV + ubo.time / 13.0f + fbm(inUV)), 0.55f, 1.0f);
    c = mix(c, vec3(0.24f, 0.44f, 0.73f), smoothstep(0.7f * fogFactor, 1.0f, length(inCamDist) / ubo.fogDist));
    c = mix(c, ubo.clearColor, smoothstep(0.8f, 1.0f, length(inCamDist) / ubo.fogDist));
    //c = mix(c, ubo.clearColor, clamp(smoothstep(0.7f * fogFactor, 1.0f, length(inWorldPos) / ubo.fogDist), 0.0f, 1.0f));
    //float t = length(inWorldPos) / ubo.fogDist;
    //c = mix(c, ubo.clearColor, 1.0f - exp(-0.000001 * t * t));

    outColor = vec4(c, 1.0f);
}

