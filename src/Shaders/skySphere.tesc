#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Tessellation control shader for rendering the sky sphere.
 */

// Generate triangle patches.
layout(vertices = 3) out;

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    mat4 m;
    mat4 v;
    mat4 p;
    vec3 clearColor;
    vec3 cameraPos;
    float tessFactor;
    float time;
    float heightFactor;
    float fogDist;
    vec2 worldPos;
} ubo;

void main()
{
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    if (gl_InvocationID == 0)
    { // Once per patch.
        // Set the tessellation factors for triangle patch.
        // ID 2 and 0
        float o0 = 1.0f * ubo.tessFactor;
        gl_TessLevelOuter[0] = o0;
        // ID 0 and 1
        float o1 = 1.0f * ubo.tessFactor;
        gl_TessLevelOuter[1] = o1;
        // ID 1 and 2
        float o2 = 1.0f * ubo.tessFactor;
        gl_TessLevelOuter[2] = o2;
        // Outer[0], Outer[1], Outer[2]
        gl_TessLevelInner[0] = o0 + o1 + o2;
    }
}