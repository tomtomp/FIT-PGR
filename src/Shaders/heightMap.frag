#version 450
#extension GL_ARB_separate_shader_objects : enable
// ^ Required by Vulkan.

/**
 * Vertex shader for height-map rendering.
 */

layout(binding = 0) uniform UniformBufferObject {
    vec2 position;
    float seed;
    float time;
    float size;
    float frequency;
    float heightFactor;
} ubo;

layout(location = 0) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;

/*
 * Notice: All of the information about noise have been
 * found on https://www.scratchapixel.com .
 */

/// Magical random generator.
float random(in vec2 st)
{
    return fract(sin(dot(st.xy, vec2(17.17237, 113.194723)) * ubo.seed) * 192838.1317);
}

/// Simple value-noise.
float noise(in vec2 st)
{
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Corners of the interpolation square.
    float lt = random(i);
    float rt = random(i + vec2(1.0f, 0.0f));
    float lb = random(i + vec2(0.0f, 1.0f));
    float rb = random(i + vec2(1.0f, 1.0f));

    // Smoothstep the interpolation factor.
    vec2 u = f * f * (3.0 - 2.0 * f);

    // Bilinear interpolation.
    float result = mix(
        mix(lt, rt, u.x),
        mix(lb, rb, u.x),
        u.y
    );

    return result;
}

/// Number of noise layers.
#define NOISE_LAYERS 7

/// Fractional brownian motion.
float fbm(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += noise(st) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Turbulence noise.
float turb(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.3f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        result += abs(2.0f * noise(st) - 1.0f) * amplitude;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

/// Ridge noise.
float ridge(in vec2 st)
{
    float result = 0.0f;

    float amplitude = 1.0f;
    float lunacrity = 2.0f;
    float gain = 0.5f;

    for (int iii = 0; iii < NOISE_LAYERS; ++iii)
    {
        float n = amplitude - abs(2.0f * noise(st) - 1.0f) * amplitude;
        n = n * n;
        result += n;
        st *= lunacrity;
        amplitude *= gain;
    }

    return result;
}

void main()
{
    //vec2 pos = inTexCoord /* * ubo.size*/ * ubo.frequency + ubo.position / 100.0f /*/ ubo.size*/;
    vec2 pos = inTexCoord  + ubo.position / 100.0f;
    //float c = (fbm(pos) * 2.0f - 1.0f) * ubo.heightFactor;
    /*
    float c = fbm(pos * 0.67f) * ubo.heightFactor +
                -fbm(pos * 13.3f) * ubo.heightFactor / 16.0f +
                -ridge(pos * 3.17f) * ubo.heightFactor +
                -turb(pos * 7.0f) * ubo.heightFactor / 4.0f +
                -ridge(pos * 3.0f) * ubo.heightFactor / 8.0f;
                */
    //vec2 pos = inTexCoord + ubo.time;
    //c = fbm(pos * 3.0f + fbm(pos * 7.0f + fbm(pos * 13.0f)));
    //c = ridge(pos * 7.3f) * ubo.heightFactor;
    //c += turb(pos * 7.3f) * ubo.heightFactor;
    //c -= ridge(pos * 7.3f) * ubo.heightFactor / 16.0f;
    float c = turb(pos * 3.7) * ubo.heightFactor / 2.0f +
               -ridge(pos * 3.9f) * ubo.heightFactor / 2.0f +
                fbm(pos * 7.3f) * ubo.heightFactor / 4.0f;
    //c = clamp(c, -ubo.heightFactor, ubo.heightFactor);
    outColor = vec4(1.0f, 0.0f, 1.0f, c);
}

