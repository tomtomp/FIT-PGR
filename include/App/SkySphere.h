/**
 * @file App/SkySphere.h
 * @author Tomas Polasek
 * @brief Generator for the sky sphere.
 */

#ifndef VULKANTERRAINGEN_SKYSPHERE_H
#define VULKANTERRAINGEN_SKYSPHERE_H

#include "Engine/VulkanEngine.h"

class SkySphere
{
public:
    SkySphere() = default;
    /**
     * TODO - Add parameters.
     * Generate vertex and index data.
     * @param indexStart Starting index number.
     */
    void generate(uint32_t indexStart);

    /// Get the vertex data.
    auto &vertices() const
    { return mVertices; }

    /// Get the index data.
    auto &indices() const
    { return mIndices; }
private:
    /// List of vertices creating the sphere.
    std::vector<mVkI::Vertex3D> mVertices;
    /// List of indices into the vertex data.
    std::vector<uint32_t> mIndices;
protected:
};

#endif //VULKANTERRAINGEN_SKYSPHERE_H
