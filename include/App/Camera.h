/**
 * @file App/Camera.h
 * @author Tomas Polasek
 * @brief Camera implementation.
 */

#ifndef VULKANTERRAINGEN_CAMERA_H
#define VULKANTERRAINGEN_CAMERA_H

#include "Types.h"

/// OpenGL camera class.
class Camera
{
public:
    /**
     * Construct camera at origin, looking long the negative z axis.
     * @param fov Field of view in degrees.
     * @param aspectRatio Aspect ratio, usually width / height.
     * @param near Near clipping plane.
     * @param far Far clipping plane.
     */
    Camera(float fov, float aspectRatio, float near, float far)
    { calculatePerspectiveProjection(glm::radians(fov), aspectRatio, near, far); }

    /// Lazy constructor.
    Camera() = default;

    /**
     * Calculate new perspective projection.
     * @param fov Field of view in degrees.
     * @param aspectRatio Aspect ratio, usually width / height.
     * @param near Near clipping plane.
     * @param far Far clipping plane.
     */
    void setProjection(float fov, float aspectRatio, float near, float far)
    { calculatePerspectiveProjection(glm::radians(fov), aspectRatio, near, far); }

    /**
     * Move the camera to given position.
     * @param pos Position in the world-space.
     */
    void setPos(const glm::vec3 &pos)
    { calculateTranslation(pos); }

    /**
     * Rotate the camera using given vector of radian angles.
     * @param rot Vector containing angles in radians.
     */
    void setRot(const glm::vec3 &rot)
    { calculateRotation(rot); }

    /**
     * Rotate the camera using given yaw, pitch and roll angles.
     * @param yaw Yaw angle.
     * @param pitch Pitch angle.
     * @param roll Roll angle.
     */
    void setYawPitchRoll(float yaw, float pitch, float roll)
    { calculateYawPitchRoll(yaw, pitch, roll); }

    /**
     * Set the rotation of the camera, so that it points
     * in given direction.
     * @param dir Direction vector.
     */
    void setDirection(const glm::vec3 &dir)
    { calculateRotationDir(dir); }

    /**
     * Get ViewProjection matrix.
     * @return Returns the ViewProjection matrix.
     */
    const glm::mat4 &viewProjectionMatrix()
    { return getVP(); }

    /**
     * Get view transformation matrix.
     * @return Returns the view matrix.
     */
    const glm::mat4 &viewMatrix()
    { return mView; }

    /**
     * Get projection transformation matrix.
     * @return Returns the view matrix.
     */
    const glm::mat4 &projectionMatrix()
    { return mProjection; }

    /**
     * Get address of the ViewProjection data.
     * @return Returns ptr to the first value in the ViewProjection matrix.
     */
    const glm::mat4::value_type *viewProjection()
    { return &getVP()[0][0]; }

    /**
     * Recalculate the ViewProjection matrix.
     * Must be manually called, if the changes to
     * other matrices should take effect.
     */
    void recalculate()
    {
        calculateView();
        calculateViewProjection();
    }
private:
    /**
     * Calculate Rotation matrix using quaternions. Input vector
     * has to be in radians.
     * @param rot Rotation (x, y, z) in radians.
     */
    void calculateRotation(const glm::vec3 &rot)
    {
        mRotationQuat = glm::quat(rot);
        mRotation = glm::mat4_cast(mRotationQuat);
    }

    /**
     * Calculate rotation matrix using yaw pitch and roll.
     * @param yaw Yaw angle.
     * @param pitch Pitch angle.
     * @param roll Roll angle.
     */
    void calculateYawPitchRoll(float yaw, float pitch, float roll)
    {
        //mRotation = glm::yawPitchRoll(yaw, pitch, roll);
        mRotation = glm::mat4(1.0f);
        mRotation = glm::rotate(mRotation, pitch, glm::vec3{1.0f, 0.0f, 0.0f});
        mRotation = glm::rotate(mRotation, yaw, glm::vec3{0.0f, 1.0f, 0.0f});
        mRotation = glm::rotate(mRotation, roll, glm::vec3{0.0f, 0.0f, 1.0f});
    }

    /**
     * Set the rotation of the camera, so that it points
     * in given direction.
     * @param dir Direction vector.
     */
    void calculateRotationDir(const glm::vec3 &dir)
    {
    }

    /**
     * Calculate the Translation matrix using given position.
     * @param pos Position in world space.
     */
    void calculateTranslation(const glm::vec3 &pos)
    {
        mTranslation = glm::translate(glm::mat4(1.0f), pos);
    }

    /// Calculate the View matrix.
    void calculateView()
    {
        mView = mRotation * mTranslation;
    }

    /**
     * Calculate the Projection matrix using perspective projection.
     * @param fov Field of view in radians.
     * @param aspectRatio Aspect ratio, usually width / height.
     * @param near Near clipping plane.
     * @param far Far clipping plane.
     */
    void calculatePerspectiveProjection(float fov, float aspectRatio, float near, float far)
    {
        mProjection = glm::perspective(fov, aspectRatio, near, far);
        // Flip the y-axis, since it points down in Vulkan coordinate system.
        mProjection[1][1] *= -1.0f;
    }

    /// Calculate the ViewProjection matrix.
    void calculateViewProjection()
    { mViewProjection = mProjection * mView; }

    /// Get the View matrix, if the dirty flag is set, it will be recalculated first.
    const glm::mat4 &getView()
    {
        return mView;
    }

    /// Get the Projection matrix.
    const glm::mat4 &getProjection()
    {
        return mProjection;
    }

    /**
     * Get the ViewProjection matrix. If the dirty flag
     * is set, it will be recalculated.
     * @return Returns the ViewProjection matrix.
     */
    const glm::mat4 &getVP()
    {
        return mViewProjection;
    }

    /// Translation matrix.
    glm::mat4 mTranslation;
    /// Rotation quaternion.
    glm::quat mRotationQuat;
    /// Rotation matrix.
    glm::mat4 mRotation;
    /// View matrix = mRotation * mTranslation.
    glm::mat4 mView;
    /// Projection matrix.
    glm::mat4 mProjection;
    /// ViewProjection matrix = mProjection * mView.
    glm::mat4 mViewProjection;
protected:
}; // class Camera

#endif //VULKANTERRAINGEN_CAMERA_H
