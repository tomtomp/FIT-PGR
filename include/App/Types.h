/**
 * @file App/Types.h
 * @author Tomas Polasek
 * @brief Types used in the application.
 */

#ifndef VULKANTERRAINGEN_TYPES_H
#define VULKANTERRAINGEN_TYPES_H

#include "Engine/Types.h"

#endif //VULKANTERRAINGEN_TYPES_H
