/**
 * @file TerrainLodTiles.cpp
 * @author Tomas Polasek
 * @brief Generator for terrain tiles.
 */

#ifndef VULKANTERRAINGEN_TERRAINLODTILES_H
#define VULKANTERRAINGEN_TERRAINLODTILES_H

#include "Engine/Vertex3D.h"
#include "Engine/Types.h"
#include "Engine/Util.h"
#include "Timer.h"

class TerrainLodTiles : Util::Noncopyable
{
public:
    /// Lazy constructor
    TerrainLodTiles() = default;

    /**
     * Create tiles for given number of levels.
     * @param levels Number of tile levels, should be larger
     *   or equal to one.
     */
    explicit TerrainLodTiles(uint32_t levels);

    /**
     * Create tiles for given number of levels.
     * @param levels Number of tile levels, should be larger
     *   or equal to one.
     */
    void generate(uint32_t levels);

    /// Get the size of the height-map texture.
    auto mapSize() const
    { return mMapWidth; }

    /// Get the list of vertices.
    const auto &vertices() const
    { return mVertices; }

    /// Get the size required for vertex buffer.
    const VkDeviceSize vertexSize() const
    { return mVertices.size() * sizeof(mVkI::Vertex3D); }

    /// Get the list of indices.
    const auto &indices() const
    { return mIndices; }

    /// Get the size required for index buffer.
    const VkDeviceSize indexSize() const
    { return mIndices.size() * sizeof(uint32_t); }
private:
    /// Size of the first tile.
    static constexpr float TILE_START_SIZE{1.0f};
    /// Tesselation factor for normal vertices.
    static constexpr float BASE_TESS_FACTOR{1.0f};
    /// Tesselation factor for edge.
    static constexpr float EDGE_TESS_FACTOR{2.0f};

    /// Generate vertices and indices for the terrain tiles.
    void generateTiles(uint32_t levels);

    /// Generate the first 4 tiles in each direction.
    void generateBaseGrid();

    /**
     * Generate ring of tiles of given radius.
     * @param radius Radius of the ring and the size of the tiles.
     * @param adjustTessFactor Shold the tesselation factor be adjusted
     *   for the line neighbouring smaller tiles??
     */
    void generateRing(float radius, bool adjustTessFactor);

    /**
     * Calculate texture coordinates for given position.
     * @param pos Position of the vertex.
     * @return Returns texture coordinates based on the
     *   size of the heigh-map.
     */
    glm::vec2 calcTexCoord(const glm::vec2 &pos);

    /**
     * Add given vertex and register its index.
     * @param vert Vertex to add.
     */
    void addVertex(const mVkI::Vertex3D &vert);

    /// List of vertices for the terrain tiles.
    std::vector<mVkI::Vertex3D> mVertices;
    /// List of indices representing the tiles.
    std::vector<uint32_t> mIndices;
    /// Map for remembering indices for unique vertices.
    std::unordered_map<mVkI::Vertex3D, uint32_t> mIndexMap;
    /// Width of the height-map image.
    uint32_t mMapWidth{0};
protected:
};

#endif //VULKANTERRAINGEN_TERRAINLODTILES_H
