/**
 * @file Engine/TerrainGenApp.cpp
 * @author Tomas Polasek
 * @brief Class representing the main application.
 */

#ifndef VULKANTERRAINGE_TUTORIALAPP_H
#define VULKANTERRAINGE_TUTORIALAPP_H

#include "Engine/VulkanEngine.h"
#include "Timer.h"
#include "TerrainLodTiles.h"
#include "SkySphere.h"
#include "Camera.h"
#include "Gamepad.h"
#include "Keyboard.h"
#include "Mouse.h"

/// Main application class.
class TerrainGenApp : Util::Noncopyable
{
public:
    TerrainGenApp() = default;

    /// Main application loop.
    void run();
private:
    /// Width of the main window in pixels.
    static constexpr uint32_t WINDOW_WIDTH{800};
    /// Height of the main window in pixels.
    static constexpr uint32_t WINDOW_HEIGHT{600};
    /// Title of the main window.
    static constexpr char const *WINDOW_TITLE{"TerrainGen"};
    /// Print information every one second.
    static constexpr uint64_t MS_PER_INFO{1000};
    /// Target number of updates per second.
    static constexpr double TARGET_UPS{60.0};
    /// Once in how many milliseconds should state update.
    static constexpr double MS_PER_UPDATE{Timer::MS_IN_S / TARGET_UPS};
    /// Target number of frames per second.
    static constexpr double TARGET_FPS{60.0};
    /// Once in how many milliseconds should a frame be presented.
    static constexpr double MS_PER_FRAME{Timer::MS_IN_S / TARGET_FPS};
    /// Number of terrain tiers.
    static constexpr uint32_t TILE_LEVELS{7u};
    /// Multiplication factor for the heigh-map resolution.
    static constexpr uint32_t HEIGHT_MAP_RES_FACTOR{8};
    /// Base frequency for height-map generation.
    static constexpr float HEIGHT_MAP_FREQUENCY_BASE{1.0f};
    /// Base height factor for height-map generation.
    static constexpr float HEIGHT_MAP_HEIGHT_FACTOR_BASE{32.0f};
    /// Clear color for the sky, taken from image of a sky.
    //static constexpr glm::vec3 SKY_COLOR{0.82f, 0.87f, 0.90f};
    static constexpr glm::vec3 SKY_COLOR{0.24f, 0.44f, 0.73f};
    /// Deadzone for gamepad joysticks.
    static constexpr float JOYSTICK_DEADZONE{0.1f};
    /// Joystick sensitivity.
    static constexpr float JOYSTICK_SENSITIVITY_DIV{1.0e1f};
    /// Joystick sensitivity.
    static constexpr float JOYSTICK_SENSITIVITY{0.7f};
    /// Joystick sensitivity factor.
    static constexpr float JOYSTICK_SENSITIVITY_FACTOR{JOYSTICK_SENSITIVITY/JOYSTICK_SENSITIVITY_DIV};

    /// Callback for resizing of the main window.
    static void windowResizeCb(GLFWwindow *window, int width, int height);

    /// Callback for surface creation.
    static void surfaceCreateCb(void *cData, VkInstance instance,
                                VkAllocationCallbacks *cb, VkSurfaceKHR *surface);

    /// Initialize the window using GLFW.
    void initWindow();
    /// Initialize Vulkan API.
    void initVulkan();
    /// Initialize application data.
    void initApp();
    /// Main loop of the application.
    void mainLoop();
    /// Cleanup resources.
    void cleanup();

    /// Resize the window to given size.
    void resizeWindow(uint32_t width, uint32_t height);

    /// Run the application logic.
    void updateState();

    /**
     * Get vector of all required extensions by name.
     * @return Vector containing all required extensions by name.
     */
    std::vector<const char*> getRequiredGlfwExtensions();

    /// Main application window.
    GLFWwindow *mWindow{nullptr};
    uint32_t mWidth{0};
    uint32_t mHeight{0};

    /// Vulkan engine.
    mVkE::VulkanEngine mEngine;

    /// Terrain tiles generator.
    TerrainLodTiles mTerrainTiles;
    /// Sky sphere generator.
    SkySphere mSkySphere;
    /// Main camera controller.
    Camera mCamera;
    /// Gamepad abstraction.
    Gamepad mGamepad;
    /// Keyboard abstraction.
    Keyboard mKeyboard;
    /// Mouse abstraction.
    Mouse mMouse;

    /// Structure containing state of the application.
    struct AppState
    {
        /// Mouse position.
        double mousePosX{0.0};
        double mousePosY{0.0};
        /// Position for the camera.
        glm::vec2 cameraMousePos{0.0f, 0.0f};

        /// Mouse scroll delta.
        double scrollDelta{0.0};

        /// Is the camera control enabled?
        bool cameraEnabled{false};

        /// Uniforms used by the terrain renderer.
        mVkEngineConf::HHVkDescriptors::UniformBufferObject uniforms;
        /// Uniforms used by the height-map generator.
        mVkEngineConf::HHVkDescriptors::HeightMapUBO hmUniforms;

        /// Wireframe toggle
        bool displayWf{false};

        /// Movement delta.
        glm::vec3 posDelta{0.0f, 0.0f, 0.0f};
        /// Position in the world.
        glm::vec3 worldPos{0.0f, 0.0f, 0.0f};
    } mState;
};

#endif //VULKANTERRAINGE_TUTORIALAPP_H
