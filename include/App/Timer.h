/**
 * @file Timer.h
 * @author Tomas Polasek
 * @brief Simple timer.
 */

#ifndef TERRAIN_GEN_TIMER_H
#define TERRAIN_GEN_TIMER_H

#include "Engine/Types.h"

/// Simple timer.
class Timer
{
private:
    using clock = std::chrono::high_resolution_clock;
    using time_point = clock::time_point;
public:
    using seconds = std::chrono::seconds;
    using milliseconds = std::chrono::milliseconds;
    using microseconds = std::chrono::microseconds;
    using nanoseconds = std::chrono::nanoseconds;

    // Number of milliseconds in one second.
    static constexpr double MS_IN_S{1000.0};
    /// How many microseconds are in one millisecond.
    static constexpr double US_IN_MS{1000.0};

    /// Initialize the timer and start it.
    Timer()
    { reset(); }

    /// Get the current time.
    time_point now() const
    { return clock::now(); }

    /// Start the timer.
    void reset()
    { mStart = now(); }

    /**
     * Get the elapsed time and reset the timer.
     * @tparam UnitT Unit, in which the time will be returned (e.g. Timer::seconds).
     * @return Elapsed time from the last reset.
     */
    template <typename UnitT>
    uint64_t elapsedReset()
    {
        uint64_t elapsedTime{elapsed<UnitT>()};
        reset();
        return elapsedTime;
    }

    /**
     * Get how many time elapsed from the start time.
     * @tparam UnitT Unit, in which the time will be returned (eg. Timer::seconds).
     * @return Returns elapsed time in requested units.
     */
    template <typename UnitT>
    uint64_t elapsed() const
    { return std::chrono::duration_cast<UnitT>(now() - mStart).count(); }
private:
    /// Time, when the timer started.
    time_point mStart;
protected:
}; // class Timer


#endif //TERRAIN_GEN_TIMER_H
