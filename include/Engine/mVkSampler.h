/**
 * @file Engine/mVkSampler.h
 * @author Tomas Polasek
 * @brief Vulkan sampler abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKSAMPLER_H
#define VULKANTERRAINGEN_MVKSAMPLER_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for the Vulkan sampler.
    class HVkSampler
    {
    public:
        /// Lazy constructor
        HVkSampler() = default;

        /**
         * Create the sampler with given parameters.
         * @param device Device to create the sampler on.
         * @param mag Magnification filter.
         * @param min Minification filter.
         * @param mode Addressing mode for all texture coordinates.
         * @param anisotropy Anisotropic filtering enable.
         * @param borderColor Border color.
         */
        void create(VkDevice device, VkFilter mag, VkFilter min,
                    VkSamplerAddressMode mode, VkBool32 anisotropy,
                    VkBorderColor borderColor);

        /// Get the inner sampler handle.
        VkSampler sampler() const
        { return mSampler; }

        /// Destroy created objects.
        void destroy();

        /// Destroy created objects.
        ~HVkSampler();
    private:
        /// Device used in creation of this sampler.
        VkDevice mDevice;
        /// Inner sampler handle.
        VkSampler mSampler{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKSAMPLER_H
