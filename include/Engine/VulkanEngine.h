/**
 * @file Engine/VulkanEngine.h
 * @author Tomas Polasek
 * @brief Connection between application and Vulkan API.
 */

#ifndef VULKANTERRAINGEN_VULKANENGINE_H
#define VULKANTERRAINGEN_VULKANENGINE_H

#include "mVkUtil.h"
#include "mVkHelpers.h"
#include "mVkSwapChain.h"
#include "mVkDescriptorSet.h"
#include "mVkFramebuffer.h"
#include "mVkShader.h"
#include "mVkBuffer.h"
#include "mVkSampler.h"

#include "SimpleVertex.h"
#include "Vertex3D.h"

/// Vulkan engine configuration namespace
namespace mVkEngineConf
{
    /// Folder containing compiled shaders..
    static constexpr char const *SHADER_FOLDER{"shaders/"};
#ifdef NDEBUG
    /// Are the validations layers enabled?
    static constexpr bool VALIDATION_LAYERS_ENABLE{false};
    /// List of validation layers.
    static const std::vector<const char*> VALIDATION_LAYERS{};
#else
    /// Are the validations layers enabled?
    static constexpr bool VALIDATION_LAYERS_ENABLE{true};
    /// List of validation layers.
    static const std::vector<const char*> VALIDATION_LAYERS{
            "VK_LAYER_LUNARG_standard_validation"
    };
#endif

    /// List of required device extensions.
    static const std::vector<const char*> DEVICE_EXTENSIONS{
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    /// Should the swap chain be triple-buffered?
    static constexpr bool USE_TRIPLE_BUFFERING{true};
    /// Should frame swaps be synchronized to the vblank?
    static constexpr bool USE_VSYNC{false};

    /// Number of samples used in multisampling, should be larger than one.
    static constexpr VkSampleCountFlagBits MULTISAMPLING_SAMPLES{VK_SAMPLE_COUNT_4_BIT};

    /**
     * Handle holder for base Vulkan objects, required
     * by all applications.
     */
    struct HHVkBase
    {
        /// Main Vulkan instance.
        VkInstance instance{VK_NULL_HANDLE};
        /// Debug callback used with validation layers.
        VkDebugReportCallbackEXT debugCallback{VK_NULL_HANDLE};
        /// Surface of the main window.
        VkSurfaceKHR surface{VK_NULL_HANDLE};
        /// Physical device used by the application.
        VkPhysicalDevice physicalDevice{VK_NULL_HANDLE};
        /// Logical device used by the application.
        VkDevice device{VK_NULL_HANDLE};
        /// Command pool used for all command allocations.
        VkCommandPool cmdPool{VK_NULL_HANDLE};
    };

    /**
     * Queues used by the application.
     */
    struct HHVkQueues
    {
        /// Information about queue families.
        mVkH::QueueFamilyIndices queueFamilies;
        /// Graphics queue using the main logical device.
        VkQueue graphicsQueue{VK_NULL_HANDLE};
        /// Queue used for window presentation.
        VkQueue presentQueue{VK_NULL_HANDLE};
    };

    /**
     * Vulkan objects used by the swap chain.
     */
    struct HHVkSwapChain
    {
        /// Swap chain used for presentation to the main window.
        mVkH::HVkSwapChain swapChain;
        /// Resolve image for depth multisampling.
        mVkH::HVkImage depthResolve;

        /// Objects specific to multisampling.
        struct HHVkMultiSampling
        {
            /// Multisampled color image.
            mVkH::HVkImage color;
            /// Multisampled depth image.
            mVkH::HVkImage depth;
        } msaa;
    };

    /**
     * Vulkan objects used in the main render pass, resulting
     * in the presented images.
     */
    struct HHVkMainRenderPass
    {
        /// Handle to the main render pass.
        VkRenderPass renderPass;
        /// Framebuffers for the render pass.
        mVkH::HVkFramebuffer framebuffer;
        /// Render command buffers for each framebuffer.
        std::vector<VkCommandBuffer> commands;
        /// Wireframe rendering commands.
        std::vector<VkCommandBuffer> commandsWf;
    };

    /**
     * Descriptors used in the applications and their
     * layouts.
     */
    struct HHVkDescriptors
    {
        /// Pool used for descriptor allocation.
        mVkH::HVkDescriptorPool mainPool;

        /**
         * Layout of the uniform buffer, passed to the
         * shaders.
         */
        struct UniformBufferObject
        {
            /// Model-View-Projection matrix.
            glm::mat4 mvp;
            /// Model transformation matrix.
            glm::mat4 m;
            /// View transformation matrix.
            glm::mat4 v;
            /// Projection transformation matrix.
            glm::mat4 p;
            /// Clear color of the sky.
            glm::vec3 clearColor;
            /// Position of the camera.
            alignas(16) glm::vec3 cameraPos;
            /// Global tessellation factor.
            float tessFactor;
            /// Current time.
            float time;
            /// Maximum height and negative height.
            float heightFactor;
            /// Distance where the fog is fully in effect.
            float fogDist;
            /// Current position in the world.
            alignas(16) glm::vec2 worldPos;
        };

        /// Descriptor set for terrain rendering.
        mVkH::HVkDescriptorSet terrainDesc;

        /**
         * Uniform buffer used in the height-map
         * rendering shaders.
         */
        struct HeightMapUBO
        {
            /// Position of the center of the map.
            glm::vec2 position;
            /// Seed for the height-map generator.
            float seed;
            /// Current time.
            float time;
            /// Size of the height-map.
            float size;
            /// Frequency of the height-map.
            float frequency;
            /// Maximum height and negative height.
            float heightFactor;
        };

        /// Descriptor set for height-map rendering.
        mVkH::HVkDescriptorSet heightMapDesc;
    };

    /**
     * Container for pipelines used in this application.
     */
    struct HHVkPipelines
    {
        /// Terrain rendering shader prefix.
        static constexpr char const *TERRAIN_SHADER_NAME{"terrain"};
        /// Sky sphere rendering shader prefix.
        static constexpr char const *SKY_SPHERE_SHADER_NAME{"skySphere"};
        /// Viewport used in terrain pipeline.
        VkViewport terrainViewport;
        /// Terrain rendering pipeline layout.
        VkPipelineLayout terrainLayout;
        /// Terrain rendering pipeline.
        VkPipeline terrain;
        /// Wireframe terrain rendering pipeline.
        VkPipeline terrainWf;
        /// Sky sphere rendering pipeline.
        VkPipeline skySphere;

        /// Height-map rendering shader prefix.
        static constexpr char const *HEIGHT_MAP_SHADER_NAME{"heightMap"};
        /// Viewport used in height-map pipeline.
        VkViewport heightMapViewport;
        /// Height-map rendering pipeline layout.
        VkPipelineLayout heightMapLayout;
        /// Height-map rendering pipeline.
        VkPipeline heightMap;
    };

    /**
     * Height-map rendering.
     */
    struct HHVkHeightMapRenderPass
    {
        /// Resolution of the height-map.
        uint32_t resolution{0u};
        /// Extent of the framebuffer.
        VkExtent2D ext;
        /// Render pass for rendering the height-map.
        VkRenderPass renderPass;
        /// Single framebuffer for the result height-map.
        mVkH::HVkFramebuffer framebuffer;
        /// Command for rendering the height-map.
        VkCommandBuffer command;
    };

    /**
     * Vulkan buffers used in the application.
     */
    struct HHVkBuffers
    {
        /// Buffer for storing shader uniforms.
        mVkH::HVkBuffer uniform;

        /// Draw vertex data.
        std::vector<mVkI::Vertex3D> vertexData;
        /// Draw vertex buffer.
        mVkH::HVkBuffer vertex;
        /// Draw index data.
        std::vector<uint32_t> indexData;
        /// Draw index buffer.
        mVkH::HVkBuffer index;
        /// Number of indices used for terrain drawing.
        uint32_t numTerrainIndices;

        /// Uniform buffer for the height-map rendering.
        mVkH::HVkBuffer hmUniform;
        /// Vertex data for the height-map rendering.
        std::vector<mVkI::SimpleVertex> hmVertexData;
        /// Vertex buffer for the height-map rendering.
        mVkH::HVkBuffer hmVertex;
    };

    /**
     * Vulkan samplers and their images/views used in the application.
     */
    struct HHVkSamplers
    {
        /// Terrain height-map image used for deformation.
        mVkH::HVkImage terrainImage;
        /// Terrain height-map sampler used for deformation.
        mVkH::HVkSampler terrain;
    };

    /**
     * Semaphores and fences used in the application.
     */
    struct HHVkSynchronization
    {
        /// Semaphore for waiting for the next swap chain image.
        VkSemaphore imageAvailable;
        /// Semaphore for waiting for the rendering to finish.
        VkSemaphore renderFinished;

        /// Signalization that height-map rendering is finished.
        VkSemaphore heightMapAvailable;
    };

    /// Get structure with required device features.
    VkPhysicalDeviceFeatures reqDeviceFeatures();
}

/// Vulkan engine namespace.
namespace mVkE
{
    class VulkanEngine : Util::Noncopyable
    {
    public:
        /// Lazy constructor.
        VulkanEngine() = default;

        /**
         * Initialize the Vulkan engine.
         * @param surfaceCb Callback to create sufrace of the main window.
         * @param cData Pointer passed to the surface creation callback.
         * @param width Width of the main window.
         * @param height Height of the main window.
         * @param extensions Extensions required by window toolkit.
         */
        void initialize(mVkU::SurfaceCreateCb surfaceCb,
                        void *cData,
                        uint32_t width, uint32_t height,
                        std::vector<const char*> extensions);

        /**
         * Finalize the initialization of the Vulkan engine.
         */
        void finalize();

        /**
         * Recreate the swap chain for given window size.
         * @param width New width of the window.
         * @param height New height of the window.
         */
        void resize(uint32_t width, uint32_t height);

        /// Clean up vulkan objects.
        void cleanup();

        /**
         * Update the uniform buffer with given data.
         * @param uniforms Data to copy over to the gpu UBO.
         * @param hmUniorms Uniforms for the height-map generator.
         */
        void updateUBO(mVkEngineConf::HHVkDescriptors::UniformBufferObject &uniforms,
                       mVkEngineConf::HHVkDescriptors::HeightMapUBO &hmUniorms);

        /**
         * Prepare engine for terrain rendering.
         * @param vertices List of vertices.
         * @param indices List of indices.
         * @param heightMapRes Resolution of the height-map.
         * @param terrainIndices Number of indices for terrain.
         */
        void prepareTerrain(const std::vector<mVkI::Vertex3D> &vertices,
                            const std::vector<uint32_t> &indices,
                            uint32_t heightMapRes, uint32_t terrainIndices);

        /// Submit the draw commands.
        void submitDrawCommands(bool wireframe);

        /// Wait for the main logical device to be idle.
        void deviceWaitIdle();
    private:
        /// Cleanup all of the swap chain Vulkan objects.
        void cleanupSwapChain();

        /// Initialize the main Vulkan instance.
        void createInstance(const std::vector<const char*> &extensions);
        /// Configuration for validation layer callback.
        void setupDebugCallback();
        /// Pick a suitable physical device.
        void pickPhysicalDevice();
        /// Create a logical device with required queues.
        void createLogicalDevice();
        /// Get queues from created logical device.
        void getDeviceQueues();
        /// Create the swap chain for main window.
        void createSwapChain(uint32_t width, uint32_t height);
        /// Create depth buffer for the main swap chain.
        void createDepthBuffer();
        /// Create multisampling buffers for the main swap chain.
        void createMultisampleBuffers();
        /// Create the main graphics queue command pool.
        void createCommandPool();
        /// Create pool for creating descriptors.
        void createDescriptorPool();

        /// Specify attachments used in the rendering process.
        void createAppRenderPass();
        /// Create required framebuffers for current swap chain.
        void createAppFramebuffers();
        /// Create layout information about used descriptor sets.
        void createAppDescriptorSetLayout();
        /// Create buffers used in the application.
        void createAppBuffers();
        /// Create samplers used in the application.
        void createAppSamplers();
        /// Create synchronization primitives used in the application.
        void createAppSync();
        /// Create the descriptor set and configure descriptor within.
        void createAppDescriptorSet();

        /// Prepare objects for height-map rendering.
        void prepareHeightMap();
        /// Create render pass for height-map rendering.
        void hmCreateRenderPass();
        /// Create frame buffer for height-map rendering.
        void hmCreateFramebuffer();
        /// Create vertex and uniform buffers for the height-map rendering
        void hmCreateBuffers();
        /// Create height-map sampler.
        void hmCreateSampler();
        /// Create descriptor set for the height-map rendering.
        void hmCreateDescriptorSet();
        /// Create commands for height-map rendering.
        void hmCreateCommand();
        /// Create the height-map rendering pipeline.
        void hmCreatePipeline();

        /// Create the graphics pipeline used in rendering.
        void createAppGraphicsPipeline();
        /// Create command buffer for each of the framebuffers.
        void createAppCommandBuffers(VkPipeline pipeline,
                                     std::vector<VkCommandBuffer> &commands);

        /// Destroy and then recreate the command buffers.
        //void recreateCommandBuffers();

        /// Cleanup objects used for height-map rendering.
        void cleanupHeightMap();

        /**
         * Fill vertex and index buffers.
         * @param vertices List of vertices.
         * @param indices List of indices.
         */
        void fillVertIndBuffers(const std::vector<mVkI::Vertex3D> &vertices,
                                const std::vector<uint32_t> &indices);

        /**
         * Check given physical device, if it supports all required
         * extensions.
         * @param device Device being investigated.
         * @return Returns true, if the device is suitable.
         */
        bool isDeviceSuitable(VkPhysicalDevice device);

        /**
         * Get vector of all required extensions by name.
         * @param extensions Extensions already required by other parts.
         * @return Vector containing all required extensions by name.
         */
        std::vector<const char*> getRequiredExtensions(const std::vector<const char*> &extensions);

        /// Holder for all of the handle holders.
        struct HHVkApp
        {
            mVkEngineConf::HHVkBase base;
            mVkEngineConf::HHVkQueues queues;
            mVkEngineConf::HHVkSwapChain swapChain;
            mVkEngineConf::HHVkMainRenderPass mRendPass;
            mVkEngineConf::HHVkDescriptors descriptors;
            mVkEngineConf::HHVkPipelines pipelines;
            mVkEngineConf::HHVkHeightMapRenderPass hmRendPass;
            mVkEngineConf::HHVkBuffers buffers;
            mVkEngineConf::HHVkSamplers samplers;
            mVkEngineConf::HHVkSynchronization sync;
        } mVk;
    protected:
    };
}

#endif //VULKANTERRAINGEN_VULKANENGINE_H
