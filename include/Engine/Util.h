/**
 * @file Engine/Util.h
 * @author Tomas Polasek
 * @brief Miscellaneous utilities.
 */

#ifndef TERRAIN_GEN_UTIL_H
#define TERRAIN_GEN_UTIL_H

#include "Types.h"

/// Container for utility functions.
namespace Util
{
    /// Non-copyable prototype class.
    class Noncopyable
    {
    public:
        Noncopyable() = default;
        Noncopyable(const Noncopyable &other) = delete;
        Noncopyable(Noncopyable &&other) = delete;
        Noncopyable &operator=(const Noncopyable &rhs) = delete;
        Noncopyable &operator=(Noncopyable &&rhs) = delete;
    };

    /**
     * Clamp value to given range.
     * @param val Value to clamp
     * @param lo Lower edge.
     * @param hi Higher edge.
     * @return Returns value in range <lo, hi>.
     */
    float clamp(float val, float lo, float hi);

    /**
     * Load given file with binary content.
     * @param filename Path to the file.
     * @return Returns binary data read from the file.
     */
    std::vector<char> loadFileB(const std::string &filename);
} // namespace Util

#endif //TERRAIN_GEN_UTIL_H
