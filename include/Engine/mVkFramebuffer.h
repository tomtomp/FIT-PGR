/**
 * @file Engine/mVkFramebuffer.h
 * @author Tomas Polasek
 * @brief Abstraction for Vulkan framebuffers.
 */

#ifndef VULKANTERRAINGEN_MVKFRAMEBUFFER_H
#define VULKANTERRAINGEN_MVKFRAMEBUFFER_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for Vulkan framebuffer collection.
    class HVkFramebuffer
    {
    public:
        /// Initialize internal structure
        HVkFramebuffer() = default;

        /// Prepare structure for framebuffer creation.
        void prepare(VkDevice device);

        /**
         * Add framebuffer to the list.
         * @param renderPass Which render will this framebuffer be used with.
         * @param swapChainExtent Extent for the current swap chain.
         * @param attachments List of attachments for this frame.
         */
        void addFramebuffer(VkRenderPass renderPass, VkExtent2D swapChainExtent,
                            std::initializer_list<VkImageView> attachments);

        /// List of framebuffers.
        auto framebuffers()
        { return mFramebuffers; }

        /// Destroy all created framebuffers.
        void destroy();

        /// Destroy created framebuffers.
        ~HVkFramebuffer();
    private:
        /// Device used in framebuffer creation.
        VkDevice mDevice{VK_NULL_HANDLE};
        /// List of created framebuffers.
        std::vector<VkFramebuffer> mFramebuffers;
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKFRAMEBUFFER_H
