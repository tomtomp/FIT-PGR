/**
 * @file Engine/Vertex3D.h
 * @author Tomas Polasek
 * @brief 3D vertex used for representing 3d models.
 */

#ifndef VULKANTERRAINGEN_VERTEX3D_H
#define VULKANTERRAINGEN_VERTEX3D_H

#include "mVkUtil.h"

namespace mVkI
{
    struct Vertex3D
    {
        /// Position of the vertex.
        glm::vec3 pos;
        /// Texture coordinates.
        glm::vec2 uv;
        /// Normal.
        glm::vec3 normal;
        /// Tessellation factor for the vertex.
        float tesFactor;

        Vertex3D() = default;

        explicit Vertex3D(glm::vec3 vPos) :
                pos{vPos}
        { }

        Vertex3D(glm::vec3 vPos, glm::vec2 vUv, glm::vec3 vNorm) :
            pos{vPos}, uv{vUv}, normal{vNorm}
        { }

        bool operator==(const Vertex3D &rhs) const
        {
            return pos == rhs.pos &&
                    uv == rhs.uv &&
                    normal == rhs.normal &&
                    tesFactor == rhs.tesFactor;
        }

        static VkVertexInputBindingDescription getBindingDescription()
        {
            VkVertexInputBindingDescription bd{};
            // Buffer binding.
            bd.binding = 0;
            // Number of bytes per vertex input.
            bd.stride = sizeof(Vertex3D);
            // One vertex per vertex index.
            bd.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return bd;
        }

        static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions()
        {
            VkVertexInputAttributeDescription posAD{};
            // Buffer binding.
            posAD.binding = 0;
            // Attribute location.
            posAD.location = 0;
            // Format of the position element, 3 signed floats.
            posAD.format = VK_FORMAT_R32G32B32_SFLOAT;
            // Offset of the position in the vertex data structure
            posAD.offset = offsetof(Vertex3D, pos);

            VkVertexInputAttributeDescription texAD{};
            // Buffer binding.
            texAD.binding = 0;
            // Attribute location.
            texAD.location = 1;
            // Format of the texture coordinate element, 2 signed floats.
            texAD.format = VK_FORMAT_R32G32_SFLOAT;
            // Offset of the texture coordinates in the vertex data structure
            texAD.offset = offsetof(Vertex3D, uv);

            VkVertexInputAttributeDescription normAd{};
            // Buffer binding.
            normAd.binding = 0;
            // Attribute location.
            normAd.location = 2;
            // Normal format - 3 floats.
            normAd.format = VK_FORMAT_R32G32B32_SFLOAT;
            // Offset of the texture coordinates in the vertex data structure
            normAd.offset = offsetof(Vertex3D, normal);

            VkVertexInputAttributeDescription tessAd{};
            // Buffer binding.
            tessAd.binding = 0;
            // Attribute location.
            tessAd.location = 3;
            // Single float.
            tessAd.format = VK_FORMAT_R32_SFLOAT;
            // Offset of the texture coordinates in the vertex data structure
            tessAd.offset = offsetof(Vertex3D, tesFactor);

            return { posAD, texAD, normAd, tessAd };
        }
    };
}

// Recommended by: http://en.cppreference.com/w/cpp/utility/hash
namespace std
{
    template<> struct hash<mVkI::Vertex3D>
    {
        typedef mVkI::Vertex3D argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& s) const noexcept
        {
            result_type const h1 ( std::hash<glm::vec3>{}(s.pos) );
            result_type const h2 ( std::hash<float>{}(s.tesFactor) );
            return h1 ^ (h2 << 1);
        }
    };
}

#endif //VULKANTERRAINGEN_VERTEX3D_H
