/**
 * @file Engine/mVkCommandBuffer.h
 * @author Tomas Polasek
 * @brief Vulkan command buffer abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKCOMMANDBUFFER_H
#define VULKANTERRAINGEN_MVKCOMMANDBUFFER_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for Vulkan command buffer.
    class HVkCommandBuffer
    {
    public:
        /**
         * Create a command buffer on given device, in given command
         * pool.
         * @param device Device being used.
         * @param cmdPool In which pool should the buffer be created.
         */
        HVkCommandBuffer(VkDevice device, VkCommandPool cmdPool);

        /// Free the command buffer.
        ~HVkCommandBuffer();

        /**
         * Begin collecting commands in this command buffer.
         * @return Returns this command buffer for use in vkCmd...
         *   functions.
         */
        VkCommandBuffer beginCommands();

        /**
         * End collecting commands and flush the buffer.
         * @param submitQueue Queue to submit the commands to.
         */
        void endCommands(VkQueue submitQueue);
    private:
        /// Device used to create this buffer.
        VkDevice mDevice;
        /// Pool used to create this buffer.
        VkCommandPool mCmdPool;
        /// Inner Vulkan command buffer.
        VkCommandBuffer mCmdBuffer{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKCOMMANDBUFFER_H
