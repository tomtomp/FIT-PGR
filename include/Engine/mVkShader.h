/**
 * @file Engine/mVkShader.h
 * @author Tomas Polasek
 * @brief Vulkan shader abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKSHADER_H
#define VULKANTERRAINGEN_MVKSHADER_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for Vulkan shader.
    class HVkShader : Util::Noncopyable
    {
    public:
        /**
         * Load and prepare shader located at given location. Creates actual
         * filename from basePath + file suffix based on shader type (.vert, .frag, ...).
         * @param basePath Base path of the SpirV compiled shader..
         * @param shaderType Type of the shader.
         * @param device Vulkan device to use.
         */
        HVkShader(const std::string basePath, VkShaderStageFlagBits shaderType, VkDevice device);

        /// Get the shader module.
        VkShaderModule shaderModule() const
        { return mShaderModule; }

        /// Get filled pipeline shader stage CI structure.
        VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo() const;

        /// Destroy shader module, if created.
        ~HVkShader();
    private:
        /// Extension of compiled Spir-V code.
        static constexpr char const *SHADER_SPIRV_EXT{".spv"};

        /**
         * Create shader module from given Spir-V code.
         * @param shaderSpirV Compiled shader Spir-V code.
         * @param device Device to use.
         * @return Returns shader module for given shader code.
         */
        VkShaderModule createShaderModule(VkDevice device, const std::vector<char> &shaderSpirV);

        /// Device used in shader creation and destruction.
        VkDevice mDevice;
        /// Type of this shader.
        VkShaderStageFlagBits mShaderType{VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM};
        /// Loaded shader module.
        VkShaderModule mShaderModule{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKSHADER_H
