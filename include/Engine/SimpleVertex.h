/**
 * @file Engine/SimpleVertex.h
 * @author Tomas Polasek
 * @brief Simple vertex data structure capable o being transfered to the GPU.
 */

#ifndef TERRAIN_GEN_SIMPLEVERTEX_H
#define TERRAIN_GEN_SIMPLEVERTEX_H

#include "mVkUtil.h"

namespace mVkI
{
    struct SimpleVertex
    {
        /// Position of the vertex.
        glm::vec2 pos;

        static VkVertexInputBindingDescription getBindingDescription()
        {
            VkVertexInputBindingDescription bd{};
            // Buffer binding.
            bd.binding = 0;
            // Number of bytes per vertex input.
            bd.stride = sizeof(SimpleVertex);
            // One vertex per vertex index.
            bd.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return bd;
        }

        static std::array<VkVertexInputAttributeDescription, 1> getAttributeDescriptions()
        {
            VkVertexInputAttributeDescription posAD{};
            // Buffer binding.
            posAD.binding = 0;
            // Attribute location.
            posAD.location = 0;
            // Format of the position element, 2 signed floats.
            posAD.format = VK_FORMAT_R32G32_SFLOAT;
            // Offset of the position in the vertex data structure
            posAD.offset = offsetof(SimpleVertex, pos);

            return { posAD };
        }
    };

    namespace ExampleData
    {
        static const std::vector<SimpleVertex> vertices = {
                {{ -1.0f, -1.0f }},
                {{ -1.0f,  1.0f }},
                {{  1.0f,  1.0f }},

                {{ -1.0f, -1.0f }},
                {{  1.0f,  1.0f }},
                {{  1.0f, -1.0f }}};
    }
}

#endif //TERRAIN_GEN_SIMPLEVERTEX_H
