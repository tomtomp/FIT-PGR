/**
 * @file Engine/mVkHelpers.h
 * @author Tomas Polasek
 * @brief Helper classes for communication with Vulkan API.
 */

#ifndef VULKANTERRAINGEN_VKHELPERS_H
#define VULKANTERRAINGEN_VKHELPERS_H

#include "mVkUtil.h"

/// Vulkan helpers namespace
namespace mVkH
{
    /**
     * Structure used for checking available queue families.
     */
    struct QueueFamilyIndices
    { // TODO - Rework needed!
        /**
         * Find queue families which we are interested in.
         * @param device Device to search in.
         * @param surface Current application surface.
         */
        QueueFamilyIndices(VkPhysicalDevice device, VkSurfaceKHR surface);

        /// Lazy constructor
        QueueFamilyIndices() = default;

        /**
         * Find queue families which we are interested in.
         * @param device Device to search in.
         * @param surface Current application surface.
         */
        void fill(VkPhysicalDevice device, VkSurfaceKHR surface);

        /// Index for families which were not found.
        static constexpr int32_t NOT_FOUND{-1};
        /// Index of the graphics family.
        int32_t graphicsFamily{NOT_FOUND};
        /// Index of the presentation family.
        int32_t presentFamily{NOT_FOUND};

        /// Returns true, if all of the requirements were met.
        bool isComplete()
        {
            return graphicsFamily != NOT_FOUND &&
                   presentFamily != NOT_FOUND;
        }

        auto indexList() const
        {
            ASSERT_FATAL(graphicsFamily != -1);
            ASSERT_FATAL(presentFamily != -1);

            return std::vector<uint32_t>{ static_cast<uint32_t>(graphicsFamily),
                                          static_cast<uint32_t>(presentFamily) };
        }
    };

    /**
     * Structure containing information about swap chain
     * support.
     */
    struct SwapChainSupportDetails
    { // TODO - Rework needed!
        /**
         * Find details about swap chain on given device.
         * @param device Physical device being investigated.
         * @param surface Current application surface.
         */
        SwapChainSupportDetails(VkPhysicalDevice device, VkSurfaceKHR surface);

        /// Lazy constructor
        SwapChainSupportDetails() = default;

        /**
         * Find details about swap chain on given device.
         * @param device Physical device being investigated.
         * @param surface Current application surface.
         */
        void fill(VkPhysicalDevice device, VkSurfaceKHR surface);

        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    /// Helper class for mapping Vulkan memory with RAII.
    class ScopeMemoryMapper : Util::Noncopyable
    {
    public:
        /**
         * Map given memory to the CPU memory.
         * @param device Device where the memory is situated on.
         * @param memory Memory to map.
         * @param offset Offset in the memory.
         * @param size Size of memory to map.
         * @param flags Flags for mapping the memory.
         * @param data Return pointer for the mapped memory.
         */
        ScopeMemoryMapper(VkDevice device, VkDeviceMemory memory,
                          VkDeviceSize offset, VkDeviceSize size,
                          VkMemoryMapFlags flags, void **data);

        /// Unmap the memory.
        ~ScopeMemoryMapper();
    private:
        /// Device for which the memory is mapped for.
        VkDevice mDevice{VK_NULL_HANDLE};
        /// Memory mapped by this object.
        VkDeviceMemory mMemory{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_VKHELPERS_H
