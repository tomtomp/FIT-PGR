/**
 * @file Engine/mVkSwapChain.h
 * @author Tomas Polasek
 * @brief Vulkan swap chain abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKSWAPCHAIN_H
#define VULKANTERRAINGEN_MVKSWAPCHAIN_H

#include "mVkUtil.h"
#include "mVkHelpers.h"
#include "mVkImage.h"

namespace mVkH
{
    /// Helper abstraction for the Vulkan swap chain.
    class HVkSwapChain
    {
    public:
        /// Lazy constructor
        HVkSwapChain() = default;

        /**
         * Create the swap chain.
         * @param device Device to create the swap chain on.
         * @param physDevice Physical device used.
         * @param surface Surface of the window.
         * @param width Width of the window.
         * @param height Height of the window.
         * @param tripleBuffering Use triple buffering?
         * @param vSync Use vertical synchronization?
         */
        void create(VkDevice device, VkPhysicalDevice physDevice,
                    VkSurfaceKHR surface, uint32_t width, uint32_t height,
                    bool tripleBuffering, bool vSync);

        /// Format of images in this swap chain.
        VkFormat format() const
        { return mSurfaceFormat.format; }

        /// Extent of images in this swap chain.
        VkExtent2D extent() const
        { return mExtent; }

        /// Get list of views for the swap chain images.
        const auto &views() const
        { return mSwapChainImageViews; }

        /// Get the inner swap chain object.
        VkSwapchainKHR swapchain() const
        { return mSwapChain; }

        /**
         * Get index of the next image in the swap chain.
         * @param semaphore Semaphore for the next image.
         * @param fence Fence for the next image.
         * @param index Output is returned through this reference.
         * @return Returns code from the Vulkan function.
         */
        VkResult nextImage(VkSemaphore semaphore, VkFence fence, uint32_t &index);

        /**
         * Destroy created objects.
         * @param inclSwapChain Should the swapchain itself be included?
         */
        void destroy(bool inclSwapChain);

        /// Destroy this swap chain.
        ~HVkSwapChain();
    private:
        /**
         * Configure inner properties of this class, before
         * creation of the swap chain.
         * @param device Device used for creation.
         * @param physDevice Physical device used.
         * @param surface Surface of the window.
         * @param width Width of the image.
         * @param width Height of the image.
         * @param tripleBuffering Should the swap chain be
         *   triple buffered?
         * @param vSync Should the swap chain be synchronized
         *   with vblank?
         */
        void configure(VkDevice device, VkPhysicalDevice physDevice,
                       VkSurfaceKHR surface,
                       uint32_t width, uint32_t height,
                       bool tripleBuffering, bool vSync);

        /**
         * Create the actual swap chain.
         * @param physDevice Physical device used in creation.
         * @param surface Surface which this swap chain is used for.
         */
        void createSwapChain(VkPhysicalDevice physDevice, VkSurfaceKHR surface);

        /// Get images from created swap chain.
        void getImages();

        /// Create views for images gained from the swap chain.
        void createViews();

        /// Details about supported features in local swap chain.
        SwapChainSupportDetails mSupportDetails;
        /// Surface format of this swap chain.
        VkSurfaceFormatKHR mSurfaceFormat{VK_FORMAT_UNDEFINED, VK_COLOR_SPACE_MAX_ENUM_KHR};
        /// Presentation mode of this swap chain.
        VkPresentModeKHR mPresentMode{VK_PRESENT_MODE_MAX_ENUM_KHR};
        /// Size of the images in this swap chain.
        VkExtent2D mExtent{0u, 0u};
        /// Number of images in this swap chain.
        uint32_t mImageCount{0};

        /// Device used to create this swap chain.
        VkDevice mDevice{VK_NULL_HANDLE};
        /// Inner Vulkan swap chain handle.
        VkSwapchainKHR mSwapChain{VK_NULL_HANDLE};
        /// List of images in the swap chain.
        std::vector<VkImage> mSwapChainImages;
        /// List of image views for the swap chain images.
        std::vector<mVkH::HVkImageView> mSwapChainImageViews;
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKSWAPCHAIN_H
