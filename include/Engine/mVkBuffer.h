/**
 * @file Engine/mVkBuffer.h
 * @author Tomas Polasek
 * @brief Vulkan buffer abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKBUFFER_H
#define VULKANTERRAINGEN_MVKBUFFER_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for Vulkan buffer.
    class HVkBuffer
    {
    public:
        /**
         * Create buffer with given parameters.
         * @param device Device on which to create the buffer.
         * @param physDevice Physical device to find requested memory.
         * @param size Required size of the buffer.
         * @param usage Presumed usage of the buffer.
         * @param properties Required properties of target memory.
         */
        HVkBuffer(VkDevice device, VkPhysicalDevice physDevice,
                  VkDeviceSize size, VkBufferUsageFlags usage,
                  VkMemoryPropertyFlags properties);

        /// Lazy constructor.
        HVkBuffer();

        /**
         * Create buffer with given parameters.
         * @param device Device on which to create the buffer.
         * @param physDevice Physical device to find requested memory.
         * @param size Required size of the buffer.
         * @param usage Presumed usage of the buffer.
         * @param properties Required properties of target memory.
         */
        void create(VkDevice device, VkPhysicalDevice physDevice,
                    VkDeviceSize size, VkBufferUsageFlags usage,
                    VkMemoryPropertyFlags properties);

        /// Destroy and deallocate.
        void destroy();

        /// Destroy and deallocate.
        ~HVkBuffer();

        /// Get the inner Vulkan buffer handle.
        VkBuffer buffer() const
        { return mBuffer; }

        /// Get the inner buffer memory.
        VkDeviceMemory memory() const
        { return mMemory; }

        /// Get the size of the buffer.
        VkDeviceSize size() const
        { return mSize; }

        /**
         * Copy content from other buffer to this one.
         * @param cmdBuffer Command buffer used for the copy command.
         * @param srcBuffer Source buffer.
         * @param srcOffset Offset in the source buffer from which to copy.
         * @param dstOffset Offset in this buffer to copy to.
         * @param size How much data should be copied.
         */
        void copyFromBuffer(VkCommandBuffer cmdBuffer, VkBuffer srcBuffer,
                            VkDeviceSize srcOffset, VkDeviceSize dstOffset,
                            VkDeviceSize size);
    private:
        /// Device this buffer exists on.
        VkDevice mDevice;
        /// Actual buffer.
        VkBuffer mBuffer{VK_NULL_HANDLE};
        /// Size of the buffer.
        VkDeviceSize mSize{0};
        /// Memory bound to the buffer.
        VkDeviceMemory mMemory{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKBUFFER_H
