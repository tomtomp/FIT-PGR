/**
 * @file Engine/mVkDescriptorSet.h
 * @author Tomas Polasek
 * @brief Abstraction for Vulkan descriptor set.
 */

#ifndef VULKANTERRAINGEN_MVKDESCRIPTORSET_H
#define VULKANTERRAINGEN_MVKDESCRIPTORSET_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Vulkan descriptor set abstraction.
    class HVkDescriptorSet
    {
    public:
        /// Initialize internal structure.
        HVkDescriptorSet() = default;

        /**
         * Add given descriptor layout binding to this descriptor set.
         * @param binding Binding location of this descriptor.
         * @param type Type of the descriptor.
         * @param count Number of descriptors of this type to use.
         * @param stages Shader stages in which this descriptor should
         *   be available.
         * @param immutableSamplers List of immutable samplers, or nullptr.
         */
        void addBinding(uint32_t binding, VkDescriptorType type, uint32_t count,
                        VkShaderStageFlags stages, const VkSampler *immutableSamplers);

        /**
         * Use added bindings to create a set layout.
         * @param device Device to use in creation of the set layout.
         */
        void createSetLayout(VkDevice device);

        /**
         * Allocate descriptor set based on the compiled layouts.
         * @param pool Pool from which should the descriptor set be allocated.
         */
        void createDescriptorSet(VkDescriptorPool pool);

        /**
         * Connect buffer to descriptor on given binding.
         * @param buffer Buffer to bind.
         * @param offset Offset, where the data begins.
         * @param size Size of the data.
         * @param binding Binding location.
         * @param type Type of the descriptor.
         */
        void writeBufferInfo(VkBuffer buffer, VkDeviceSize offset, VkDeviceSize size,
                             uint32_t binding, VkDescriptorType type);

        /**
         * Connect sampler to descriptor on given binding.
         * @param sampler Sampler to bind.
         * @param view Image view for sampled image.
         * @param layout Layout of the image.
         * @param binding Binding location.
         * @param type Type of the descriptor.
         */
        void writeSamplerInfo(VkSampler sampler, VkImageView view, VkImageLayout layout,
                              uint32_t binding, VkDescriptorType type);

        /// Get list of layouts in this descriptor set.
        auto layouts()
        { return mLayouts; }

        /// Get the inner descriptor set.
        auto &descriptorSet()
        { return mDescriptorSet; }

        /// Destroy all created objects.
        void destroy();

        /// Destroy all created objects.
        ~HVkDescriptorSet();
    private:
        /// List of added descriptor binding.
        std::vector<VkDescriptorSetLayoutBinding> mBindings;
        /// List of pool sizes for added descriptor bindings.
        std::vector<VkDescriptorPoolSize> mPoolSizes;

        /// Device used in creation of this descriptor set.
        VkDevice mDevice;
        /// Bindings compiled into the set layouts.
        std::vector<VkDescriptorSetLayout> mLayouts{};
        /// Final descriptor set.
        VkDescriptorSet mDescriptorSet{VK_NULL_HANDLE};
    protected:
    };

    /// Vulkan descriptor pool abstraction.
    class HVkDescriptorPool
    {
    public:
        /// Initialize internal structure.
        HVkDescriptorPool();

        /**
         * Request space for given number of descriptors with
         * given type.
         * @param type Type of the descriptor.
         * @param count Number of descriptors requested.
         */
        void requestSize(VkDescriptorType type, uint32_t count);

        /**
         * Create the descriptor pool with requested space.
         * @param device Device to use.
         * @param maxSets Maximum number of sets which can be allocated.
         */
        void create(VkDevice device, uint32_t maxSets);

        /// Get the inner pool handle.
        VkDescriptorPool pool() const
        { return mPool; }

        /// Destroy the descriptor pool.
        void destroy();

        /// Destroy the descriptor pool.
        ~HVkDescriptorPool();
    private:
        /// Required numbers of allocations for each descriptor type.
        uint32_t mRequirements[VK_DESCRIPTOR_TYPE_RANGE_SIZE]{0, };

        /// Device used to create this pool.
        VkDevice mDevice{VK_NULL_HANDLE};
        /// Inner handle to the Vulkan object.
        VkDescriptorPool mPool{VK_NULL_HANDLE};
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKDESCRIPTORSET_H
