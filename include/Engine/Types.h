/**
 * @file Engine/Types.h
 * @author Tomas Polasek
 * @brief Type definitions and common includes.
 */

#ifndef TERRAIN_GEN_TYPES_H
#define TERRAIN_GEN_TYPES_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
// Use Vulkan 0.0 - 1.0 depth.
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <chrono>
#include <array>
#include <set>
#include <unordered_map>
#include <map>

#include "Assert.h"

#endif //TERRAIN_GEN_TYPES_H
