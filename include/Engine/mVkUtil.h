/**
 * @file Engine/mVkUtil.h
 * @author Tomas Polasek
 * @brief Utilities for the Vulkan engine.
 */

#ifndef VULKANTERRAINGEN_VKUTIL_H
#define VULKANTERRAINGEN_VKUTIL_H

#include "Types.h"
#include "Util.h"

/// Vulkan utility functions.
namespace mVkU
{
    /**
     * Check if all of the validation layers are available for
     * current instance.
     * @param layers Which layers to check.
     * @return Returns true, if all of the validations layers
     *   are present.
     */
    bool checkValidationLayers(const std::vector<const char*> &layers);

    /// Print available Vulkan extensions.
    void uvkPrintExtensions();

    /// Callback function for the Vulkan debug reporting.
    VKAPI_ATTR VkBool32 VKAPI_CALL uvkDebugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objectType,
        uint64_t object,
        size_t location,
        int32_t messageCode,
        const char* pLayerPrefix,
        const char* pMessage,
        void* pUserData);

    /// Callback from Vulkan engine to get sufrace of the window.
    using SurfaceCreateCb = void(*)(void*, VkInstance, VkAllocationCallbacks*, VkSurfaceKHR*);

    /// Create a VkDebugReportCallbackEXT object.
    VkResult createDebugReportCallbackEXT(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT *createInfo,
        const VkAllocationCallbacks *allocator,
        VkDebugReportCallbackEXT *callback);

    /// Destroy a VkDebugReportCallbackEXT object.
    void destroyDebugReportCallbackEXT(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks *allocator);

    /**
     * Check if given device supports all required extensions.
     * @param device Device being investigated.
     * @param reqExtensions Required extensions.
     * @return Returns true, if the device supports all required
     *   extensions.
     */
    bool checkDeviceExtensionSupport(VkPhysicalDevice device,
                                     const std::vector<const char*> &reqExtensions);

    /**
     * Choose preferred surface format.
     * @param availableFormats List of available formats.
     * @return Returns chosen surface format.
     */
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats);

    /**
     * Choose preferred presentation mode.
     * @param presentModes List of available presentation modes.
     * @param vSync Set to true, if vertical synchronization presentation
     *   mode should be chosen.
     * @return Returns chosen presentation mode.
     */
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &presentModes,
                                           bool vSync);

    /**
     * Choose preferred resolution.
     * @param capabilities Capabilities of target surface.
     * @param width Width of the window.
     * @param height Height of the window.
     * @return Returns chosen resolution.
     */
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities,
                                uint32_t width, uint32_t height);

    /**
     * Get file extension for given shader type.
     * @param shaderType Type of the shader.
     * @return Returns extension used for shaders of given type. For unknown
     *   shader types returns empty string.
     */
    const char *shaderExtension(VkShaderStageFlags shaderType);

    /**
     * Select memory, which can be used to store given type of data and support
     * requested operations.
     * @param typeFilter Which type of memory do we need.
     * @param properties Which properties should the memory have.
     * @param physDevice Physical device being used.
     * @return Returns index of the memory.
     */
    uint32_t findMemoryType(VkPhysicalDevice physDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

    /**
     * Find supported format for images, based on the list of candidates.
     * @param physDevice Physical device to search on.
     * @param candidates List of candidates, first satisfactory candidate will
     *   be returned.
     * @param tiling Requested type of tiling.
     * @param features Target usage.
     * @return Returns first supported candidate.
     */
    VkFormat findSupportedFormat(VkPhysicalDevice physDevice,
                                 const std::initializer_list<VkFormat>& candidates,
                                 VkImageTiling tiling, VkFormatFeatureFlags features);

    /**
     * Find image format for depth buffer.
     * @param physDevice Physical device to search on.
     * @return Returns format used for the depth buffer.
     */
    VkFormat findDepthFormat(VkPhysicalDevice physDevice);

    /**
     * Does given image format have a stencil memory?
     * @param format Tested format.
     * @return Returns true, if the format has stencil bits.
     */
    bool hasStencilComponent(VkFormat format);

    /**
     * Calculate aspect mask based on the format.
     * For Depth formats returns VK_IMAGE_ASPECT_DEPTH_BIT.
     * For Depth with Stencil formats, returns
     *   VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT.
     * For other formats returns VK_IMAGE_ASPECT_COLOR_BIT.
     * @param format Format of the image.
     * @return Returns aspect flags contained in image with given format.
     */
    VkImageAspectFlags imageAspects(VkFormat format);

    /**
     * Create image view for given image.
     * @param device Device to create the view on.
     * @param image Image to create the view for.
     * @param format Format of the view.
     * @param aspectFlags Aspect flags of the view.
     * @param imageView Created image view will be passed back
     *   through this reference.
     */
    void createImageView(VkDevice device,
                         VkImage image, VkFormat format,
                         VkImageAspectFlags aspectFlags,
                         VkImageView &imageView);
}

#endif //VULKANTERRAINGEN_VKUTIL_H
