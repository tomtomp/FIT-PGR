/**
 * @file Engine/Assert.h
 * @author Tomas Polasek
 * @brief Assertion system.
 */

#ifndef VULKANTERRAINGEN_ASSERT_H
#define VULKANTERRAINGEN_ASSERT_H

#include <cstdio>
#include <cstdlib>
#include <csignal>

#define UNUSED(VAR) (static_cast<void>(VAR))

#define ASSERT_FATAL(COND_EXP) \
    if ((COND_EXP)) \
    {} \
    else \
    { \
        fprintf(stderr, "Fatal error \"" #COND_EXP "\"" \
                        "%s: %d ( %s ) failed.\n", \
                        __FILE__, __LINE__, __func__); \
        fflush(stderr); \
        std::raise(SIGABRT); \
        exit(-1); \
    }

#ifndef NDEBUG
#   define ASSERT_BASE(COND_EXP, LINE, FILE, FUNC) \
    if ((COND_EXP)) \
    {} \
    else \
    { \
        fprintf(stderr, "Assertion \"" #COND_EXP "\"" \
                        "%s:%d ( %s ) failed.\n", \
                        FILE, LINE, FUNC); \
        fflush(stderr); \
        std::raise(SIGABRT); \
    }
#else
#   define ASSERT_BASE(COND_EXP, LINE, FILE, FUNC)
#endif

#ifndef NDEBUG
/// Choose first, if debug is on, else choose second.
#   define CHOOSE_DEBUG(first, second) first
/// If NDEBUG is not defined, the command in brackets will be present.
#   define DO_IF_DEBUG(cmd) cmd
#else
/// Choose first, if debug is on, else choose second.
#   define CHOOSE_DEBUG(first, second) second
/// If NDEBUG is not defined, the command in brackets will be present.
#   define DO_IF_DEBUG(_)
#endif

// Assert used in places, where the performance hit is not too big.
#ifndef NDEBUG_FAST
#   define ASSERT_FAST(COND_EXP) ENT_ASSERT_BASE((COND_EXP), \
                                 __LINE__, __FILE__, __func__)
#else
#   define ASSERT_FAST(_)
#endif

// Assert used in places, where the performance hit is quite notable.
#ifndef NDEBUG_SLOW
#   define ASSERT_SLOW(COND_EXP) ENT_ASSERT_BASE((COND_EXP), \
                                 __LINE__, __FILE__, __func__)
#else
#   define ASSERT_SLOW(_)
#endif

// Print warning message.
#ifndef NDEBUG
#   define WARNING(MSG) \
    do{ \
        fprintf(stderr, "Warning: \"" MSG "\"" \
                        "%s:%d ( %s ).\n", \
                        __FILE__, __LINE__, __func__); \
        fflush(stderr); \
    } while(false)
#else
#   define WARNING(_)
#endif

#endif //VULKANTERRAINGEN_ASSERT_H
