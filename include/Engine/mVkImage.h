/**
 * @file Engine/mVkImage.h
 * @author Tomas Polasek
 * @brief Vulkan image abstraction.
 */

#ifndef VULKANTERRAINGEN_MVKIMAGE_H
#define VULKANTERRAINGEN_MVKIMAGE_H

#include "mVkUtil.h"

namespace mVkH
{
    /// Helper abstraction for Vulkan image view.
    class HVkImageView
    {
    public:
        /**
         * Create image view for given image.
         * @param device Device to create the view on.
         * @param image Image to create the view for.
         * @param format Format of the image view.
         * @param aspectFlags Aspect flags for the view.
         */
        HVkImageView(VkDevice device, VkImage image, VkFormat format,
                     VkImageAspectFlags aspectFlags);

        /// Lazy constructor.
        HVkImageView();

        /**
         * Create image view for given image.
         * @param device Device to create the view on.
         * @param image Image to create the view for.
         * @param format Format of the image view.
         * @param aspectFlags Aspect flags for the view.
         */
        void create(VkDevice device, VkImage image, VkFormat format,
                    VkImageAspectFlags aspectFlags);

        /// Get the inner Vulkan handle.
        VkImageView view() const
        { return mImageView; }

        /// Destroy this image view.
        void destroy();

        /// Destroy this image view.
        ~HVkImageView();
    private:
        /// Device used in creation of this image.
        VkDevice mDevice;
        /// Inner Vulkan image view handle.
        VkImageView mImageView{VK_NULL_HANDLE};
    protected:
    };

    /// Helper abstraction for Vulkan image.
    class HVkImage
    {
    public:
        /**
         * Create image with given parameters.
         * @param device Device where to create this image.
         * @param physDevice Physical device used for device creation.
         * @param width Width of the image.
         * @param height Height of the image.
         * @param format Format of texels in this image.
         * @param samples Samples per pixel.
         * @param tiling Tiling properties of the image.
         * @param usageFlags Presumed usage.
         * @param memFlags Requested memory properties.
         */
        HVkImage(VkDevice device, VkPhysicalDevice physDevice,
                 uint32_t width, uint32_t height, VkFormat format,
                 VkSampleCountFlagBits samples, VkImageTiling tiling,
                 VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags);

        /// Lazy constructor.
        HVkImage();

        /**
         * Create image with given parameters.
         * @param device Device where to create this image.
         * @param physDevice Physical device used for device creation.
         * @param width Width of the image.
         * @param height Height of the image.
         * @param format Format of texels in this image.
         * @param samples Samples per pixel.
         * @param tiling Tiling properties of the image.
         * @param usageFlags Presumed usage.
         * @param memFlags Requested memory properties.
         */
        void create(VkDevice device, VkPhysicalDevice physDevice,
                    uint32_t width, uint32_t height, VkFormat format,
                    VkSampleCountFlagBits samples, VkImageTiling tiling,
                    VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags);

        /**
         * Create image view for this image and remember it.
         * @param format Format of the view.
         * @param aspectFlags Aspect flags for the view.
         * @return Returns handle to the image view.
         */
        VkImageView createView(VkFormat format, VkImageAspectFlags aspectFlags);

        /// Get already created image view.
        VkImageView view() const
        { return mImageView.view(); }

        /// Get the inner image handle.
        VkImage image() const
        { return mImage; }

        /// Width of this image.
        uint32_t width() const
        { return mWidth; }

        /// Height of this image.
        uint32_t height() const
        { return mHeight; }

        /// Format of this image.
        VkFormat format() const
        { return mFormat;}

        /**
         * Fill the image from given buffer.
         * @param cmdBuffer Buffer to add the copy command to.
         * @param buffer Buffer to copy from.
         */
        void copyFromBuffer(VkCommandBuffer cmdBuffer, VkBuffer buffer);

        /**
         * Transition layout of this image from old layout to the new layout.
         * @param cmdBuffer Transition barrier command will be added to this buffer.
         * @param oldLayout Current layout of this image.
         * @param newLayout Requested layout.
         * @param srcAccess Source access mask.
         * @param srcStage Source stage, in which the access mask is used.
         * @param dstAccess Destination access mask.
         * @param dstStage Destination stage, in which the access mask is used.
         */
        void transitionLayout(VkCommandBuffer cmdBuffer,
                              VkImageLayout oldLayout, VkImageLayout newLayout,
                              VkAccessFlags srcAccess, VkPipelineStageFlags srcStage,
                              VkAccessFlags dstAccess, VkPipelineStageFlags dstStage);

        /**
         * TODO - Add stencil attachment operations.
         * Get attachment description for this image.
         * @param loadOp Operation performed before use.
         * @param storeOp Operation performed after use.
         * @param initial Layout before render pass.
         * @param final Layout after render pass.
         * @return Filled attachment description structure.
         */
        VkAttachmentDescription attachmentDescription(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp,
                                                      VkImageLayout initial, VkImageLayout final);

        /// Destroy this image.
        void destroy();

        /// Destroy the image.
        ~HVkImage();
    private:
        /// Device used in creation of this image.
        VkDevice mDevice;
        /// Width of the image.
        uint32_t mWidth{0};
        /// Height of the image.
        uint32_t mHeight{0};
        /// Format of the image.
        VkFormat mFormat{VK_FORMAT_UNDEFINED};
        /// Number of samples per texel.
        VkSampleCountFlagBits mSamples;
        /// Inner Vulkan image handle.
        VkImage mImage{VK_NULL_HANDLE};
        /// Mamory bound to the image.
        VkDeviceMemory mImageMemory{VK_NULL_HANDLE};
        /// View for this image.
        HVkImageView mImageView;
    protected:
    };
}

#endif //VULKANTERRAINGEN_MVKIMAGE_H
