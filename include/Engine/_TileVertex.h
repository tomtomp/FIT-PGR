/**
 * @file Engine/TileVertex.h
 * @author Tomas Polasek
 * @brief Tile vertex used in terrain rendering..
 */

#ifndef VULKANTERRAINGEN_TERRAINTILE_H
#define VULKANTERRAINGEN_TERRAINTILE_H

#include "mVkUtil.h"

namespace mVkI
{
    struct TileVertex
    {
        /// Position of the vertex.
        glm::vec2 pos;
        /// Texture coordinates.
        glm::vec2 texCoord;
        /// Tessellation factor for the vertex.
        float tesFactor;

        /**
         * Compare vertices, based on their position and
         * tessellation factor. Texture coordinates are
         * not compared, since they should be equal anyway.
         * @param rhs The other vertex.
         * @return Returns true, if position and tessellation factor
         *   are the same.
         */
        bool operator==(const TileVertex &rhs) const
        { return pos == rhs.pos && tesFactor == rhs.tesFactor; }

        static VkVertexInputBindingDescription getBindingDescription()
        {
            VkVertexInputBindingDescription bd{};
            // Buffer binding.
            bd.binding = 0;
            // Number of bytes per vertex input.
            bd.stride = sizeof(TileVertex);
            // One vertex per vertex index.
            bd.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return bd;
        }

        static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions()
        {
            VkVertexInputAttributeDescription posAD{};
            // Buffer binding.
            posAD.binding = 0;
            // Attribute location.
            posAD.location = 0;
            // Format of the position element, 2 signed floats.
            posAD.format = VK_FORMAT_R32G32_SFLOAT;
            // Offset of the position in the vertex data structure
            posAD.offset = offsetof(TileVertex, pos);

            VkVertexInputAttributeDescription texAD{};
            // Buffer binding.
            texAD.binding = 0;
            // Attribute location.
            texAD.location = 1;
            // Format of the color element, 2 signed floats.
            texAD.format = VK_FORMAT_R32G32_SFLOAT;
            // Offset of the texture coordinates in the vertex data structure
            texAD.offset = offsetof(TileVertex, texCoord);

            VkVertexInputAttributeDescription tessAD{};
            // Buffer binding.
            tessAD.binding = 0;
            // Attribute location.
            tessAD.location = 2;
            // Tessellation control is a single float.
            tessAD.format = VK_FORMAT_R32_SFLOAT;
            // Offset of the texture coordinates in the vertex data structure
            tessAD.offset = offsetof(TileVertex, tesFactor);

            return { posAD, texAD, tessAD };
        }
    };
}

// Recommended by: http://en.cppreference.com/w/cpp/utility/hash
namespace std
{
    template<> struct hash<mVkI::TileVertex>
    {
        typedef mVkI::TileVertex argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& s) const noexcept
        {
            result_type const h1 ( std::hash<glm::vec2>{}(s.pos) );
            result_type const h2 ( std::hash<float>{}(s.tesFactor) );
            return h1 ^ (h2 << 1);
        }
    };
}

#endif //VULKANTERRAINGEN_TERRAINTILE_H
