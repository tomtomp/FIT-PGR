# Description
Procedural terrain generator using Vulkan API.

# Compilation
Requirements: 
* C++ compiler with C++14 support
* Vulkan libraries and tools
* GLFW3 library

To compile the project, change directory into the project root and run: 
```
mkdir build && cd build
VULKAN_SDK_HOME=<VULKAN_HOME> cmake ..
make -j8
```

If the <VULKAN_HOME> is not the correct directory, cmake should print out a helpful message.